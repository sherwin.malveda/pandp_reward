import 'dart:async';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/errors/standard_error.dart';
//import 'package:plainsandprintsloyalty/model/request/change_password_request.dart';
import 'package:plainsandprintsloyalty/model/request/forgot_password_password.dart';
import 'package:plainsandprintsloyalty/model/request/login_request.dart';
//import 'package:plainsandprintsloyalty/model/request/verification_request.dart';
import 'package:plainsandprintsloyalty/model/session.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/auth_repository.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';

class MockedAuthRepository extends Mock implements AuthRepository {}

void main() {
  final ScreenState doneState = ScreenState(States.DONE);
  final ScreenState waitingState = ScreenState(States.WAITING);
  final String errorMessage = "Something went wrong";
  final ScreenState errorState =
  ScreenState(States.ERROR, error: StandardError(message: errorMessage));
  final Exception exception = Exception(errorMessage);

  AuthBloc bloc;
  AuthRepository repository = MockedAuthRepository();

  setUp(() {
    bloc = AuthBloc(authRepository: repository);
  });

  group("login", () {
    test("should emit done state when login succeeded", () {
      final LoginRequest request =
          LoginRequest(username: "johndoe", password: "password");
      final Session session = Session();

      when(repository.login(request)).thenAnswer((_) => Future.value(session));

      scheduleMicrotask(() {
        bloc.login(request);
      });
      expect(bloc.loginState, emitsInOrder([waitingState, doneState]));
      expect(bloc.session, emits(session));
    });

    test("should emit error state when login is unsuccessful", () {
      final LoginRequest request =
      LoginRequest(username: "johndoe", password: "password");

      when(repository.login(request))
          .thenAnswer((_) => Future.error(exception));

      scheduleMicrotask(() {
        bloc.login(request);
      });
      expect(bloc.loginState, emitsInOrder([waitingState, errorState]));
      expect(bloc.session, emitsError(exception));
    });
  });

  group("registration", () {
    test("should emit done state when login succeeded", () {
      final User user = User();
      //final VerificationRequest creds = VerificationRequest();
      //when(repository.register(user)).thenAnswer((_) => Future.value(creds));

      scheduleMicrotask(() {
        bloc.register(user);
      });

      expect(bloc.registerState, emitsInOrder([waitingState, doneState]));
    });

    test("should emit error state when registration is unsuccessful", () {
      final User user = User();

      when(repository.register(user))
          .thenAnswer((_) => Future.error(exception));

      scheduleMicrotask(() {
        bloc.register(user);
      });
      expect(bloc.registerState, emitsInOrder([waitingState, errorState]));
    });
  });

  group("forgot password", () {
    test("should emit done state when forgot password succeeded", () {
      final ForgotPasswordRequest request = ForgotPasswordRequest();

      when(repository.forgotPassword(request))
          .thenAnswer((_) => Future.value());

      scheduleMicrotask(() {
        bloc.forgotPassword(request);
      });

      expect(bloc.forgotPasswordState, emitsInOrder([waitingState, doneState]));
    });

    test("should emit error state when forgot password is unsuccessful", () {
      final ForgotPasswordRequest request = ForgotPasswordRequest();

      when(repository.forgotPassword(request))
          .thenAnswer((_) => Future.error(exception));

      scheduleMicrotask(() {
        bloc.forgotPassword(request);
      });

      expect(
          bloc.forgotPasswordState, emitsInOrder([waitingState, errorState]));
    });
  });

  group("change password", () {
    test("should emit done state when change password succeeded", () {
      //final ChangePasswordRequest request = ChangePasswordRequest();

      //when(repository.changePassword(request)).thenAnswer((_) => Future.value());

      scheduleMicrotask(() {
        //bloc.changePassword(request);
      });

      expect(bloc.changePasswordState, emitsInOrder([waitingState, doneState]));
    });

    test("should emit error state when change password is unsuccessful", () {
      //final ChangePasswordRequest request = ChangePasswordRequest();

      //when(repository.changePassword(request)).thenAnswer((_) => Future.error(exception));

      scheduleMicrotask(() {
        //bloc.changePassword(request);
      });

      expect(
          bloc.changePasswordState, emitsInOrder([waitingState, errorState]));
    });
  });
}
