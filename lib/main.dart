import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/api/api.dart';
import 'package:plainsandprintsloyalty/api/security_interceptor.dart';
import 'package:plainsandprintsloyalty/api/session_invalidation_notifier.dart';
import 'package:plainsandprintsloyalty/api/token_inteceptor.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/page_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/dao/mao_dao.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/auth_repository.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/card_repository.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/page_repository.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/store_repository.dart';
import 'package:plainsandprintsloyalty/repositories/implementations/auth_repository_impl.dart';
import 'package:plainsandprintsloyalty/repositories/implementations/card_repository_impl.dart';
import 'package:plainsandprintsloyalty/repositories/implementations/page_repository_impl.dart';
import 'package:plainsandprintsloyalty/repositories/implementations/store_repository_impl.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/about_us_page.dart';
import 'package:plainsandprintsloyalty/screens/account_verification_page.dart';
import 'package:plainsandprintsloyalty/screens/account_verification_page2.dart';
import 'package:plainsandprintsloyalty/screens/branches_page.dart';
import 'package:plainsandprintsloyalty/screens/card_page.dart';
import 'package:plainsandprintsloyalty/screens/change_password_page.dart';
import 'package:plainsandprintsloyalty/screens/contact_us_page.dart';
import 'package:plainsandprintsloyalty/screens/convert_points_page.dart';
import 'package:plainsandprintsloyalty/screens/dashboard_page.dart';
import 'package:plainsandprintsloyalty/screens/downloading.dart';
import 'package:plainsandprintsloyalty/screens/faqs_page.dart';
import 'package:plainsandprintsloyalty/screens/forgot_password_page.dart';
import 'package:plainsandprintsloyalty/screens/forgot_password_reset_page.dart';
import 'package:plainsandprintsloyalty/screens/getting_started_page.dart';
import 'package:plainsandprintsloyalty/screens/landing_page.dart';
import 'package:plainsandprintsloyalty/screens/login_page.dart';
import 'package:plainsandprintsloyalty/screens/new_voucher_page.dart';
import 'package:plainsandprintsloyalty/screens/registration_existingmember_page.dart';
import 'package:plainsandprintsloyalty/screens/registration_page.dart';
import 'package:plainsandprintsloyalty/screens/registration_question_page.dart';
import 'package:plainsandprintsloyalty/screens/splash_screen_page.dart';
import 'package:plainsandprintsloyalty/screens/success_page.dart';
import 'package:plainsandprintsloyalty/screens/terms_and_condition_page.dart';
import 'package:plainsandprintsloyalty/screens/voucher_details_page.dart';
import 'package:plainsandprintsloyalty/screens/vouchers_page.dart';

void main() => runApp(PlainsAndPrintsApp());

final Dio tokenDio = Dio();

final Dio dio = Dio();

final SessionInvalidationNotifier sessionInvalidationNotifier =
SessionInvalidationNotifier();

final TokenInterceptor interceptor =
TokenInterceptor(tokenDio: tokenDio, mainDio: dio, dao: mapDao);

final LogInterceptor logInterceptor = LogInterceptor(
    requestBody: true, responseBody: true);

final SecurityInterceptor securityInterceptor = SecurityInterceptor();

final Api api = Api(
    dio: dio,
    tokenInterceptor: interceptor,
    logInterceptor: logInterceptor,
    securityInterceptor: securityInterceptor,
    mapDao: mapDao,
    sessionInvalidationNotifier: sessionInvalidationNotifier);

final MapDao mapDao = MapDao();

final AuthRepository authRepository =
AuthRepositoryImpl(api: api, mapDao: mapDao);
final CardRepository cardRepository = CardRepositoryImpl(api: api);
final StoreRepository storeRepository = StoreRepositoryImpl(api: api);
final PageRepository pageRepository = PageRepositoryImpl(api: api);

final AuthBloc authBloc = AuthBloc(authRepository: authRepository);
final CardBloc cardBloc = CardBloc(cardRepository: cardRepository);
final StoreBloc storeBloc = StoreBloc(storeRepository: storeRepository);
final PageBloc pageBloc = PageBloc(pageRepository: pageRepository);

class PlainsAndPrintsApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    
    return MasterProvider(
      cardBloc: cardBloc,
      storeBloc: storeBloc,
      authBloc: authBloc,
      pageBloc: pageBloc,
      sessionInvalidationNotifier: sessionInvalidationNotifier,
      child: MaterialApp(
        title: 'Plains And Prints',
        theme: ThemeData(
          fontFamily: "Schyler",
          primarySwatch: Colors.blue,
          textTheme: TextTheme(body1: TextStyle(color: AppColors.gray1)),
          brightness: Brightness.light
        ),
        routes: {
          Routes.DOWNLOADING: (context) => Downloading(),
          Routes.SPLASH_SCREEN: (context) => SplashScreen(),
          Routes.GETTING_STARTED: (context) => GettingStarted(),
          Routes.LANDING: (context) => LandingPage(),
          Routes.REGISTRATION: (context) => RegistrationPage(),
          Routes.LOGIN: (context) => LoginPage(),
          Routes.DASHBOARD: (context) => DashboardPage(),
          Routes.FORGOT_PASSWORD: (context) => ForgotPasswordPage(),
          Routes.FORGOT_RESET_PASSWORD: (context) => ForgotPasswordResetPage(),
          Routes.BRANCHES: (context) => BranchesPage(),
          Routes.CARD: (context) => CardPage(),
          Routes.REGISTRATION_QUESTION: (context) => RegistrationQuestionPage(),
          Routes.EXISTING_MEMBER: (context) => ExistingMemberPage(),
          Routes.CONVERT_POINTS: (context) => ConvertPointsPage(),
          Routes.VOUCHERS: (context) => VouchersPage(),
          Routes.NEW_VOUCHER: (context) => NewVoucherPage(),
          Routes.VOUCHER_DETAILS: (context) => VoucherDetailsPage(),
          Routes.ACCOUNT_VERIFICATION2: (context) => AccountVerificationPage2(),
          Routes.ACCOUNT_VERIFICATION: (context) => AccountVerificationPage(),
          Routes.SUCCESS_PAGE: (context) => SuccessPage(),
          Routes.TERMS_AND_CONDITIONS: (context) => TermsAndConditionPage(),
          Routes.ABOUT_US: (context) => AboutUsPage(),
          Routes.CONTACT_US: (context) => ContactUsPage(),
          Routes.FAQS: (context) => FAQsPage(),
          Routes.CHANGE_PASSWORD: (context) => ChangePasswordPage(),
        },
      ),
    );
  }
}