import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';

class ApiUris {

  static const String BASE_URL = API_ENDPOINT;
  static const String VERSION_URI = "/api/v1";
  static const String REGISTRATION = "/register/account";
  static const String EXISTING = "/register/account/existing";
  static const String LOGIN = "/login/account";
  static const String ACTIVATE = "/activate/account";
  static const String VERIFY = "/verify/account";
  static const String TOKEN = "/login/token";
  static const String ACCOUNT_INFO = "/account/info/{accountId}";
  static const String ACCOUNT_EDIT = "/account/edit/{accountId}";
  static const String BALANCE_INQUIRY = "/card/balance/{accountId}";
  static const String TRANSACTION_HISTORY = "/account/txns/{accountId}";
  static const String PROMOTIONS = "/promotion";
  static const String PROMO_DETAILS = "/promotion/info/{promoId}";
  static const String NEWS_LETTER = "/newsletter";
  static const String NEWS_LETTER_DETAILS = "/newsletter/info/{newsletter_id}";
  static const String BRANCHES = "/branch";
  static const String BRANCH_INFO = "/branch/info/{branchId}";
  static const String FORGOT_PASSWORD = "/account/forgotpassword";
  static const String RESET_PASSWORD = "/account/reset-password";
  static const String CHANGE_PASSWORD = "/account/changepassword/{accountId}";
  static const String VOUCHER_LIST = "/voucher/{accountId}";
  static const String CONVERT_POINTS = "/voucher/redeem/{accountId}";
  static const String CANCEL_VOUCHER = "/voucher/cancel/{accountId}";
  static const String REGISTRATION_BACKGROUND = "/page/{pageId}";
  static const String BANNER_LIST = "/banner";
  static const String TERMS_AND_CONDITION = "/termsandcondition";
  static const String ABOUT_US = "/aboutus";
  static const String CONTACT_US = "/contactus";
  static const String PROVINCE = "/dropdown/provinces";
  static const String CITIES = "/dropdown/cities/{provinceId}";
  static const String NOTIFICATION_LIST = "/pushmessage";

  static String replaceParam(String uri, String param, dynamic value) {
    return uri.replaceFirst("{$param}", "$value");
  }

  static String attachPaginationToUri(String uri, ListRequest request) {
    return "$uri/${request.page}/${request.rows}";
  }

  static String attachPaginationToUri2(String uri, ListRequest request) {
    return "$uri/${request.accountId}/${request.page}/${request.rows}";
  }
}