import 'package:flutter/foundation.dart';

class SessionInvalidationNotifier extends ChangeNotifier {
  @override
  void notifyListeners() {
    print("Session Invalidation Notifier");
    super.notifyListeners();
  }

  @override
  bool get hasListeners => super.hasListeners;
}
