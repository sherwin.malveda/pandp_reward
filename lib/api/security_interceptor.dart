import 'package:dio/dio.dart';
import 'package:plainsandprintsloyalty/api/api.dart';

class SecurityInterceptor extends InterceptorsWrapper {
  SecurityInterceptor()
  : super(onRequest: (RequestOptions options) {
    options.headers['apikey'] = Api.API_KEY;
      return options;
    }
  );
}
