import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:plainsandprintsloyalty/api/api_uris.dart';
import 'package:plainsandprintsloyalty/dao/mao_dao.dart';
import 'package:plainsandprintsloyalty/model/session.dart';

class TokenInterceptor extends InterceptorsWrapper {
  final Dio tokenDio;
  final Dio mainDio;
  final MapDao dao;

  TokenInterceptor({
    @required this.mainDio,
    @required this.tokenDio,
    @required this.dao,
  }) : super(onRequest: (RequestOptions options) async {
    if (!options.headers.containsKey("authorization")) return options;

          final Session currentSession = Session.fromDao(dao);
          if (!_isTokenValid(currentSession)) {
            mainDio.lock();
            final Session newSession =
            await _newSession(tokenDio, currentSession);
            if (newSession != null) newSession.saveMap(dao);
            options.headers["authorization"] =
            "${newSession.tokenType} ${newSession.token}";
            mainDio.unlock();
            return options;
          }
    return options;
        });

  static bool _isTokenValid(Session session) {
    final DateTime expiry = session.expiry.toUtc();
    final DateTime now = DateTime.now().toUtc().add(Duration(seconds: 50));
    return expiry.isAfter(now);
  }

  static Future<Session> _newSession(Dio tokenDio, Session session) async {
    print("Refresh token flow");
    print("Refresh token ${session.refreshToken}");
    print("grant type ${session.grantType}");
    print("Refresh token flow");

    Session newSession;

    final Response response = await tokenDio.post(
        ApiUris.BASE_URL + ApiUris.VERSION_URI + ApiUris.TOKEN,
        data: {
          "refresh_token": session.refreshToken,
          "grant_type": session.grantType
        },
        options: Options(headers: {
          "apikey":
          "b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w=="
        }));
    newSession = Session.fromMap(response.data);

    return newSession;
  }
}
