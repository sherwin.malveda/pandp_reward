import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:plainsandprintsloyalty/api/api_uris.dart';
import 'package:plainsandprintsloyalty/api/security_interceptor.dart';
import 'package:plainsandprintsloyalty/api/session_invalidation_notifier.dart';
import 'package:plainsandprintsloyalty/api/token_inteceptor.dart';
import 'package:plainsandprintsloyalty/dao/mao_dao.dart';
import 'package:plainsandprintsloyalty/model/session.dart';
import 'package:plainsandprintsloyalty/repositories/response_validator.dart';

class Api extends ResponseValidator {
  static const String API_KEY =
    "b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w==";

  final Dio dio;
  final TokenInterceptor tokenInterceptor;
  final LogInterceptor logInterceptor;
  final SecurityInterceptor securityInterceptor;
  final SessionInvalidationNotifier sessionInvalidationNotifier;
  final MapDao mapDao;

  Api({@required this.dio,
    @required this.mapDao,
    @required this.tokenInterceptor,
    @required this.securityInterceptor,
    @required this.sessionInvalidationNotifier,
    this.logInterceptor})
      : super(sessionInvalidationNotifier) {
    dio.options.baseUrl = ApiUris.BASE_URL + ApiUris.VERSION_URI;
    dio.interceptors.add(securityInterceptor);
    dio.interceptors.add(tokenInterceptor);
    dio.interceptors.add(logInterceptor);
  }

  Future<Response> get(String path, bool isAuthenticated) async {
    Response get;
    try {
      if (isAuthenticated) {
        final String authorization = mapDao.getString(Session.KEY_TOKEN);
        final String tokenType = mapDao.getString(Session.KEY_TOKEN_TYPE);
        get = await dio.get(path,
          options: Options(
            headers: {"authorization": "$tokenType $authorization"}
          )
        );
        return get;
      } else {
        get = await dio.get(path);
      }
      checkError(get);
    } catch (error) {
      checkError(error.response);
    }
    return get;
  }

  Future<Response> post(String path, Map<String, dynamic> body,
      bool isAuthenticated) async {
    Response post;
    try {
      if (isAuthenticated) {
        final String authorization = mapDao.getString(Session.KEY_TOKEN);
        final String tokenType = mapDao.getString(Session.KEY_TOKEN_TYPE);
        post = await dio.post(path,
          data: body,
          options: Options(
            headers: {"authorization": "$tokenType $authorization"}
          )
        );
        return post;
      } else {
        post = await dio.post(path, data: body);
      }
      checkError(post);
    } catch (error) {
      checkError(error.response);
    }
    return post;
  }
}