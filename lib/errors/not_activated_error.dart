import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/model/request/verification_request.dart';

class NotActivatedError implements Exception {
  final VerificationRequest request;

  NotActivatedError({@required this.request});

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
    other is NotActivatedError &&
      runtimeType == other.runtimeType &&
      request == other.request;

  @override
  int get hashCode => request.hashCode;
}