import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:simple_animations/simple_animations.dart';

class TypewriterText extends StatelessWidget {
  
  final String text;
  TypewriterText(this.text);

  @override
  Widget build(BuildContext context) {
    return ControlledAnimation(
      duration: Duration(milliseconds: 2000),
      delay: Duration(milliseconds: 800),
      tween: IntTween(begin: 0, end: text.length),
      builder: (context, textLength) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(text.substring(0, textLength), 
              style: TextStyle(
                fontFamily: 'Schyler',
                fontSize: 22,
                color: AppColors.violet1,
                fontWeight: FontWeight.bold
              ),
            ),
            ControlledAnimation(
              playback: Playback.LOOP,
              duration: Duration(milliseconds: 600),
              tween: IntTween(begin: 0, end: 1),
              builder: (context, oneOrZero) {
                return Opacity(
                  opacity: oneOrZero == 1 ? 1.0 : 0.0,
                  child: Text("_", 
                    style: TextStyle(
                      letterSpacing: 5,
                      fontSize: 20,
                      color: AppColors.violet1,
                      fontWeight: FontWeight.bold
                    )
                  )
                );
              },
            )
          ],
        );
      }
    );
  }
}