import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';

class PrimaryButton extends StatelessWidget {

  final Widget child;
  final VoidCallback onPressed;

  const PrimaryButton({Key key, @required this.child, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: AppColors.violet1,
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 24.0),
      onPressed: onPressed,
      child: child,
      elevation: 4.0,
    );
  }
}
