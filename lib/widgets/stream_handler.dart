import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

typedef Widget WithData<T extends EmptyCheckable>(T data);
typedef Widget WithError(Object error);

class StreamHandler<T extends EmptyCheckable> extends StatelessWidget {
  final AsyncSnapshot<T> snapshot;
  final Widget loading;
  final WithData<T> withData;
  final WithError withError;
  final Widget noData;

  StreamHandler({
    @required this.snapshot,
    @required this.loading,
    @required this.withData,
    @required this.withError,
    @required this.noData,
  });

  @override
  Widget build(BuildContext context) {
    if (snapshot.hasData) {
      if (snapshot.data.isEmpty()) return noData;
      return withData(snapshot.data);
    } else if (snapshot.hasError) {
      return withError(snapshot.error);
    } else if (snapshot.connectionState == ConnectionState.waiting) {
      return loading;
    } else {
      return noData;
    }
  }
}