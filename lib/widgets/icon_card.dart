import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';

class IconCard extends StatelessWidget {
  final String assetImage;
  final String title;
  final VoidCallback onPressed;

  const IconCard({
    Key key,
    @required this.assetImage,
    @required this.title,
    @required this.onPressed}
  ) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child:Container(
        decoration: BoxDecoration(
          border: Border.all(color: AppColors.violet1),
          borderRadius: BorderRadius.all(Radius.circular(4.0))
        ),
        child: InkWell(
          onTap: onPressed,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(assetImage, height: 30, width: 30),
              SizedBox(height: 5.0),
              Text("$title",
                style: TextStyle(
                  fontFamily: 'Schyler',
                  fontSize: 7,
                  fontWeight: FontWeight.bold,
                  color: AppColors.violet1
                ),
                maxLines: 3,
                softWrap: true,
                overflow: TextOverflow.fade,
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      )
    );
  }
}