import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/model/faqs.dart';

class FAQsItem extends StatelessWidget {
  const FAQsItem(this.faqs);

  final FAQs faqs;

  Widget _buildTiles(FAQs root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title, 
      style: TextStyle(
        fontFamily: 'Schyler',
        color: AppColors.violet1,
        fontSize: 13.0
        )
      )
    );
    return ExpansionTile(
      key: PageStorageKey<FAQs>(root),
      title: Text(root.title, 
        style: TextStyle(
          fontFamily: 'Schyler',
          color: AppColors.violet1,
          fontSize: 15.0, 
          fontWeight: FontWeight.bold
        )
      ),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(faqs);
  }
}
