import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';

class LargeAppBar extends PreferredSize {
  final Widget child;

  LargeAppBar({Key key, this.child})
      : super(key: key, child: child, preferredSize: Size.fromHeight(100));

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 30),
          child,
        ],
      ),
      color: AppColors.violet1,
    );
  }
}