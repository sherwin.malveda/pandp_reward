import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';

class TransactionRow extends StatelessWidget {

  final String branch;
  final String invoiceNumber;
  final String transactionType;
  final String storeLocation;
  final double endBalance;
  final double earnedPoints;
  final double transactionAmount;
  final double redeemedPoints;
  final int transactionStatus;
  final DateTime transactionDate;

  const TransactionRow({Key key, this.branch, this.invoiceNumber, this.transactionType,
    this.storeLocation, this.endBalance, this.earnedPoints, this.transactionAmount,
    this.redeemedPoints, this.transactionStatus, this.transactionDate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColors.violet3,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              SizedBox(width: 8.0),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("${DateFormat("dd").format(transactionDate.add(new Duration(hours: 8)))}",
                    style: TextStyle(
                      color: AppColors.violet1,
                      fontSize: 39,
                      fontWeight: FontWeight.w500
                    )
                  ),
                  Text("${DateFormat("MMM").format(transactionDate.add(new Duration(hours: 8)))}",
                    style: TextStyle(
                      color: AppColors.violet1,
                      fontSize: 14,
                      fontWeight: FontWeight.w500
                    )
                  ),
                  Text("${DateFormat("yyyy").format(transactionDate.add(new Duration(hours: 8)))}",
                    style: TextStyle(
                      color: AppColors.violet1,
                      fontSize: 12,
                      fontWeight: FontWeight.w500
                    )
                  ),
                ],
              ),
              SizedBox(width: 8.0),
              VerticalDivider(),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Date: ${DateFormat("MM/dd/yy hh:mm a").format(transactionDate.add(new Duration(hours: 8)))}",
                            style: TextStyle(
                              color: AppColors.violet1,
                              fontSize: 10,
                              fontWeight: FontWeight.w500
                            )
                          ),
                          SizedBox(height: 4.0),
                          Text("Store Location: "+branch,
                            style: TextStyle(
                              color: AppColors.violet1,
                              fontSize: 10,
                              fontWeight: FontWeight.w500
                            )
                          ),
                          SizedBox(height: 4.0),
                          Text("Type: "+transactionType,
                              style: TextStyle(
                                  color: AppColors.violet1,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                          SizedBox(height: 4,),
                          Text("Status: " +getStatus(transactionStatus),
                              style: TextStyle(
                                  color: AppColors.violet1,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                        ],
                      )
                    ),
                    SizedBox(width: 5,),
                    Expanded(
                      child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Amount: ${NumberFormat.simpleCurrency(locale: "tl-ph").format(transactionAmount)}",
                            style: TextStyle(
                                color: AppColors.violet1,
                                fontSize: 10,
                                fontWeight: FontWeight.w500
                            )
                        ),
                          SizedBox(height: 4.0),
                          Text("OR No.: "+ invoiceNumber,
                              style: TextStyle(
                                  color: AppColors.violet1,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                          SizedBox(height: 4.0),
                          Text("Points Earned:"+earnedPoints.toString(),
                              style: TextStyle(
                                  color: AppColors.violet1,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                          SizedBox(height: 4.0),
                          Text("Points Redeemed: "+redeemedPoints.toString(),
                              style: TextStyle(
                                  color: AppColors.violet1,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                          SizedBox(height: 4.0),
                          Text("End Balance: "+endBalance.toString(),
                              style: TextStyle(
                                  color: AppColors.violet1,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                        ],
                      )
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String getStatus(int status){
    String _status = "";
    if(status == 1){
      _status = "Success";
    }else if(status == 2){
      _status = "Fail";
    }else if(status == 3){
      _status = "Reversed";
    }else if(status == 4){
      _status = "Voided";
    }else{
      _status = "Processing";
    }
    return _status;
  }
}