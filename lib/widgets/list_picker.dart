import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/model/item.dart';

class ListPicker extends StatelessWidget {

  final List<Item> items;

  const ListPicker({Key key, @required this.items})
    : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: items
      .map((item) => ListTile(
        title: Text(item.value, style: TextStyle(fontFamily: 'Schyler'),),
        onTap: () => Navigator.pop(context, item))
      ).toList()
    );
  }
}
