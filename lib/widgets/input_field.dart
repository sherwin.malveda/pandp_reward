import 'package:flutter/material.dart';

class InputField extends StatelessWidget {
  final String hintText;
  final bool masked;
  final FormFieldValidator<String> validator;
  final bool autoValidate;
  final TextEditingController controller;
  final int maxLength;
  final TextInputType keyboardType;
  final TextCapitalization textCapitalization;
  final FocusNode focusNode;
  final Function submit;

  const InputField({
    Key key,
    this.hintText,
    @required this.controller,
    this.validator,
    this.keyboardType,
    this.textCapitalization = TextCapitalization.none,
    this.autoValidate = false,
    this.masked = false,
    this.maxLength,
    this.focusNode,
    this.submit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: focusNode,
      obscureText: masked,
      validator: validator,
      autovalidate: autoValidate,
      style: TextStyle(fontFamily: 'Schyler'),
      keyboardType: keyboardType,
      textCapitalization: textCapitalization,
      controller: controller,
      decoration: InputDecoration(
        labelText: hintText,
        contentPadding: EdgeInsets.all(5.0),
        border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)
        )
      ),
      onFieldSubmitted: submit,
    );
  }
}
