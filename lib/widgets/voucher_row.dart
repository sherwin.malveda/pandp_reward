import 'dart:ui';
import 'package:barcode_flutter/barcode_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/widgets/dashed_rect.dart';

class VoucherRow extends StatelessWidget {
  final String description, code, status ,expiration;
  final VoidCallback onTap;

  const VoucherRow({Key key, @required this.description, @required this.code,
    @required this.status, @required this.expiration, @required this.onTap})
    : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onTap,
        child: DashedRect(
          color: AppColors.gray1,
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(description,
                  style: TextStyle(fontFamily: 'Schyler',color: AppColors.violet1, fontSize: 20.0),
                ),
                SizedBox(height: 10.0),
                Stack(
                  children: <Widget>[
                    BarCodeImage(
                      data: code,
                      codeType: BarCodeType.Code39,
                      lineWidth: 1.8,
                      barHeight: 60.0,
                      onError: (error) {
                        print('barcode error $error');
                      },
                    ),
                  ],
                ),
                Text(code,
                  style: TextStyle(fontFamily: 'Schyler',fontSize: 15,color: Colors.black, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 30.0),
                Text("valid until " +expiration.substring(0,10),
                  style: TextStyle(fontFamily: 'Schyler',color: Colors.red),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}