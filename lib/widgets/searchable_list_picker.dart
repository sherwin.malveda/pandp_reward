import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/model/item.dart';
import 'package:plainsandprintsloyalty/widgets/input_field.dart';
import 'package:plainsandprintsloyalty/widgets/list_picker.dart';

class SearchableListPicker extends StatefulWidget {
  final String title;
  final List<Item> items;

  SearchableListPicker({Key key, this.title, this.items}) : super(key: key);

  @override
  _SearchableListPickerState createState() => _SearchableListPickerState();
}

class _SearchableListPickerState extends State<SearchableListPicker> {
  final TextEditingController _controller = TextEditingController();

  List<Item> _items = [];

  @override
  void initState() {
    _items = widget.items;
    _controller.addListener(_onSearched);
    super.initState();
  }

  void _onSearched() {
    setState(() {
      _items = List.from(
          widget.items.where((item) =>
            item.value.toLowerCase().contains(
              _controller.text.toLowerCase()
            )
          )
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(widget.title, style: TextStyle(fontFamily: 'Schyler', color: Colors.black)),
        elevation: 0.0,
        brightness: Brightness.light,
        leading: IconButton(icon:Icon(Icons.arrow_back, color: Colors.black,),
        onPressed:() => Navigator.pop(context),)
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InputField(
              hintText: "Search",
              controller: _controller,
            ),
          ),
          Flexible(child: ListPicker(items: _items)),
        ],
      )
    );
  }
}