import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';

class PromoList extends StatelessWidget {
  final String promo;
  final VoidCallback onPressed;

  const PromoList({Key key, @required this.promo, @required this.onPressed}) : super(key: key);

  // Set the banner Image
  _bannerImage(BuildContext context) {
    if (promo.length == 0) {
      return new Container(
        height: ((MediaQuery.of(context).size.width -20) * (9/16)),
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage("assets/images/promodefault.jpg"),
            fit: BoxFit.contain,
          ),
        ),
      );
    } else {
      return CachedNetworkImage(
        imageUrl: promo,
          fit: BoxFit.contain,
          height: ((MediaQuery.of(context).size.width - 20) * (9/16)),
          width: MediaQuery.of(context).size.height * 1.0,
        placeholder: (context, url) => new Center (
          child: Padding(
            padding: EdgeInsets.all(30.0),
            child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)
          )
        ),
        errorWidget: (context, url, error) => new Container( 
          child:Padding(
            padding: EdgeInsets.all(30.0),
            child: Center(
              child: Text("Failed to load the image.\n\nPlease check your internet connection and try again!", 
                style: TextStyle(fontFamily: 'Schyler', fontSize: 12.0), textAlign: TextAlign.center,
              )
            ),
          ),
        )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 0.0),
      child: RaisedButton(
        padding: EdgeInsets.all(0),
        child: _bannerImage(context),
        color: AppColors.blue2,
        onPressed: onPressed
      ),
    );
  }
}
