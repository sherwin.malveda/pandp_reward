import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';

class VoucherList extends StatelessWidget {

  final VoidCallback onTap;
  final String voucher;

  const VoucherList({Key key, @required this.onTap,  @required this.voucher})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5.0),
      child: InkWell(
        onTap: onTap,
        child: Container(
          color: Color.fromRGBO(46, 25, 78, 0.05),
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 40.0),
            child: Center(
              child: Text(voucher,
                style: TextStyle(
                  fontFamily: 'Schyler', 
                  fontSize: 20.0, 
                  color: AppColors.violet1
                )
              ),
            )
          ),
        ),
      ),
    );
  }
}
