import 'package:flutter/material.dart';

class AppColors {

  static const Color violet1 = Color(0xFF686197);
  static const Color violet2 = Color(0xFF353057);
  static const Color violet3 = Color(0xFFF0EFF5);
  static const Color teal = Color(0xFF33AA9C);
  static const Color gray = Color(0xFF979797);
  static const Color gray1 = Color(0xFF5D5D5D);
  static const Color green = Color(0xFF52EE7D);
  static const Color pink = Color(0xFFEE52AA);
  static const Color blue = Color(0xFF338EFF);
  static const Color blue2 = Color(0xFFF4F5FC);
  static const Color red = Color(0xFFFF3333);

}