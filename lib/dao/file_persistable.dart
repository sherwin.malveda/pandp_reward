import 'package:plainsandprintsloyalty/dao/file_dao.dart';

abstract class FilePersistable<T> {
  void saveFile(FileDao dao);
}
