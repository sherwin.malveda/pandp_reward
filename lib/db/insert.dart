import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Insert {

  static final dbHelper = DatabaseHelper.instance;

  static void insertImage(String table, String page, String url) async {
    Map<String, dynamic> row = {
      DatabaseHelper.page : page,
      DatabaseHelper.imageurl : url
    };
    await dbHelper.insertData(row,table);
  }

  static void insertNewsletter(String id, String title, String content, String image) async {
    Map<String, dynamic> row = {
      DatabaseHelper.newsletterId : id,
      DatabaseHelper.newslettertitle : title,
      DatabaseHelper.newslettercontent : content,
      DatabaseHelper.newsletterimage : image
    };
    await dbHelper.insertData(row,"tbnewsletter");
  }

  static void insertPromotions(String promoid, String title, String content, String image) async {
    Map<String, dynamic> row = {
      DatabaseHelper.promotionid : promoid,
      DatabaseHelper.promotiontitle : title,
      DatabaseHelper.promotioncontent : content,
      DatabaseHelper.promotionimage : image
    };
    await dbHelper.insertData(row,"tbpromotion");
  }

  static void insertBranches(String id, String name, String code, String address) async {
    Map<String, dynamic> row = {
      DatabaseHelper.branchid : id,
      DatabaseHelper.branchname : name,
      DatabaseHelper.branchcode : code,
      DatabaseHelper.branchaddress : address
    };
    await dbHelper.insertData(row,"tblocation");
  }

  static void insertVoucher(String code, String status, String description, String expiration) async {
    Map<String, dynamic> row = {
      DatabaseHelper.voucherCode : code,
      DatabaseHelper.voucherDescription : description,
      DatabaseHelper.voucherStatus : status,
      DatabaseHelper.voucherExpiration : expiration
    };
    await dbHelper.insertData(row,"tbvoucher");
  }

  static void insertTransaction(String branch, String or, String amount,
      String epoints, String rpoints, String location, String balance,
      String type, String status, String date) async {
    Map<String, dynamic> row = {
      DatabaseHelper.transactionBranch : branch,
      DatabaseHelper.transactionOr : or,
      DatabaseHelper.transactionAmount : amount,
      DatabaseHelper.transactionEarnedPoints : epoints,
      DatabaseHelper.transactionRedeemedPoints : rpoints,
      DatabaseHelper.transactionLocation : location,
      DatabaseHelper.transactionBalance : balance,
      DatabaseHelper.transactionType : type,
      DatabaseHelper.transactionStatus : status,
      DatabaseHelper.transactionDate : date
    };
    await dbHelper.insertData(row,"tbtransaction");
  }


  static void insertNotification(String title, String body, String imageurl, String dateTime, String notificon) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString('username');
    Map<String, dynamic> row = {
      DatabaseHelper.user: username,
      DatabaseHelper.title: title,
      DatabaseHelper.body: body,
      DatabaseHelper.datetime: dateTime,
      DatabaseHelper.notifimage: imageurl,
      DatabaseHelper.notificon: notificon
    };
    await dbHelper.insertData(row, "notification");
  }

  static void insertSplashScreen() async {
    Map<String, dynamic> row = {
      DatabaseHelper.plainandprint : '1',
    };
    await dbHelper.insertData(row,"plainandprints");
  }

  static void insertLogin() async {
    Map<String, dynamic> row = {
      DatabaseHelper.greeting : '1',
    };

    await dbHelper.insertData(row,"greeting");
  }
}