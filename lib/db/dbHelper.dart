import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  
  static final _databaseName = "zz.db";
  static final _databaseVersion = 1;

  // For animation
  static final tbplainandprints = 'plainandprints';
  static final tbgreetings = 'greeting';

  // For saving the notification
  static final tbnotification = 'notification';

  // Save the picture
  static final tbbanner = "tbbanner";
  static final tbbackground = "tbbackground";

  // Save the appdata
  static final tbnewsletter = "tbnewsletter";
  static final tbpromotions = "tbpromotion";
  static final tblocation = "tblocation";
  static final tbvoucher = "tbvoucher";
  static final tbtransaction = "tbtransaction";

  // Unique key
  static String id = "id";
  static String plainandprint = "plainandprints";

  // For Animation
  static String greeting = "greeting";

  // For Notification
  static String user = "username";
  static String title = "notification_title";
  static String body = "notification_body";
  static String datetime = "datetime";
  static String notifimage = "notification_image";
  static String notificon = "notification_icon";

  // For saving picture
  static String page = "page";
  static String imageurl = "image";

  // For newsletter
  static String newsletterId = "news_id";
  static String newsletterimage = "image";
  static String newslettertitle = "title";
  static String newslettercontent = "content";

  // For Promotion
  static String promotionid = "promoid";
  static String promotionimage = "image";
  static String promotiontitle = "title";
  static String promotioncontent = "content";

  // For Branches
  static String branchid = "branchid";
  static String branchname = "name";
  static String branchcode = "code";
  static String branchaddress = "address";

  // For Voucher 
  static String voucherCode = "code";
  static String voucherDescription = "description";
  static String voucherStatus = "status";
  static String voucherExpiration = "expiration";

  // For transaction
  static String transactionBranch = "branch";
  static String transactionOr = "invoice";
  static String transactionAmount = "amount";
  static String transactionEarnedPoints = "epoints";
  static String transactionRedeemedPoints = "rpoints";
  static String transactionLocation = "location";
  static String transactionBalance = "balance";
  static String transactionType = "type";
  static String transactionStatus = "status";
  static String transactionDate = "date";

  // make this a singleton class
  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // only have a single app  static String wide reference to the database
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }
  
  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);

    return await openDatabase(path,
      version: _databaseVersion,
      onCreate: _onCreate);

  }

  // SQL code to create the database user
  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tbplainandprints (
        $id INTEGER PRIMARY KEY,
        $plainandprint TEXT NULL
      )'''
    );
    await db.execute('''
      CREATE TABLE $tbnotification (
        $id INTEGER PRIMARY KEY,
        $user TEXT NULL,
        $title TEXT NULL,
        $body TEXT NULL,
        $datetime TEXT NULL,
        $notifimage TEXT NULL,
        $notificon TEXT NULL
      )'''
    );
    await db.execute('''
      CREATE TABLE $tbgreetings (
        $id INTEGER PRIMARY KEY,
        $greeting TEXT NULL
      )'''
    );
    await db.execute('''
      CREATE TABLE $tbbanner (
        $id INTEGER PRIMARY KEY,
        $page TEXT NULL,
        $imageurl TEXT NULL
      )'''
    );
    await db.execute('''
      CREATE TABLE $tbbackground (
        $id INTEGER PRIMARY KEY,
        $page TEXT NULL,
        $imageurl TEXT NULL
      )'''
    );
    await db.execute('''
      CREATE TABLE $tbnewsletter (
        $id INTEGER PRIMARY KEY,
        $newsletterId TEXT NULL,
        $newslettertitle TEXT NULL,
        $newslettercontent TEXT NULL,
        $newsletterimage TEXT NULL
      )'''
    );
    await db.execute('''
      CREATE TABLE $tbpromotions (
        $id INTEGER PRIMARY KEY,
        $promotionid TEXT NULL,
        $promotiontitle TEXT NULL,
        $promotioncontent TEXT NULL,
        $promotionimage TEXT NULL
      )'''
    );
    await db.execute('''
      CREATE TABLE $tblocation (
        $id INTEGER PRIMARY KEY,
        $branchid TEXT NULL,
        $branchname TEXT NULL,
        $branchcode TEXT NULL,
        $branchaddress TEXT NULL
      )'''
    );
    await db.execute('''
      CREATE TABLE $tbvoucher (
        $id INTEGER PRIMARY KEY,
        $voucherCode TEXT NULL,
        $voucherDescription TEXT NULL,
        $voucherStatus TEXT NULL,
        $voucherExpiration TEXT NULL
      )'''
    );
    await db.execute('''
      CREATE TABLE $tbtransaction (
        $id INTEGER PRIMARY KEY,
        $transactionBranch TEXT NULL,
        $transactionOr TEXT NULL,
        $transactionAmount TEXT NULL,
        $transactionEarnedPoints TEXT NULL,
        $transactionRedeemedPoints TEXT NULL,
        $transactionLocation TEXT NULL,
        $transactionBalance TEXT NULL,
        $transactionType TEXT NULL,
        $transactionStatus TEXT NULL,
        $transactionDate TEXT NULL
      )'''
    );
  }
  
  // Insert Data
  Future<int> insertData(Map<String, dynamic> row, String table) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }
  
  // Select Data
  Future<List<Map<String, dynamic>>> selectAllData(String table) async {
    Database db = await instance.database;
    return await db.rawQuery("SELECT * FROM "+table);
  }

  // Select Data
  Future<List<Map<String, dynamic>>> selectAllData2(String table, String username) async {
    Database db = await instance.database;
    return await db.rawQuery("SELECT * FROM "+table+ " WHERE $user = '"+username+"' ORDER BY id ASC");
  }

  // Select Data
  Future<List<Map<String, dynamic>>> selectImage(String table, String pageId) async {
    Database db = await instance.database;
    return await db.rawQuery("SELECT * FROM "+table+ " WHERE $page = '"+pageId+"'");
  }

  // Delete Data
  Future<int> delete(String table) async {
    Database db = await instance.database;
    return await db.delete(table);
  }

  // Select Data
  Future<List<Map<String, dynamic>>> deleteImage(String table, String pageId) async {
    Database db = await instance.database;
    return await db.rawQuery("DELETE FROM "+table+" WHERE $page = "+pageId+"");
  }

}