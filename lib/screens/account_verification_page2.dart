import 'dart:async';
import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/model/request/activation_request.dart';
import 'package:plainsandprintsloyalty/model/request/verification_request.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/arguments/success_arguments.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';

class AccountVerificationPage2 extends StatefulWidget {
  @override
  _AccountVerificationPageState createState() => _AccountVerificationPageState();
}

class _AccountVerificationPageState extends State<AccountVerificationPage2>
    with AfterLayoutMixin<AccountVerificationPage2> {

  final dbHelper = DatabaseHelper.instance;
  String image = "";
  final TextEditingController _code = TextEditingController();
  AuthBloc _authBloc;
  VerificationRequest credentials;
  Timer _timer;
  bool _canResend = false;
  int _currentTime = 30;

  @override
  void initState(){
    super.initState();
    getBackground();
  }

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    credentials = ModalRoute.of(context).settings.arguments;
    _sendCode(credentials);
    _authBloc.activationState.listen(onVerificationState);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          CachedNetworkImage(
              imageUrl: image,
              height: MediaQuery.of(context).size.height * 1.0,
              width: MediaQuery.of(context).size.height * 1.0,
              fit: BoxFit.fitWidth,
              placeholder: (context, url) => new Center (
                  child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)
              ),
              errorWidget: (context, url, error) => new Container(
                  child: Image.asset('assets/images/pagebg.jpg',
                    height: MediaQuery.of(context).size.height * 1.0,
                    width: MediaQuery.of(context).size.height * 1.0,
                    fit: BoxFit.fitWidth,
                  )
              )
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              child: Center(
                child: Container(
                  width: 280,
                  height: 350,
                  decoration: new BoxDecoration(
                      color: Colors.white70,
                      borderRadius: BorderRadius.circular(5.0)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text("Activate Account",
                          style: TextStyle(
                              fontFamily: 'Schyler',
                              color: AppColors.violet1,
                              fontSize: 25.0,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                      Container(
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 16.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              SizedBox(height: 25.0),
                              Container(
                                  height: 40.0,
                                  decoration: new BoxDecoration(
                                      color: Colors.white70,
                                      borderRadius: BorderRadius.circular(10.0)
                                  ),
                                  child: TextFormField(
                                      controller: _code,
                                      style: TextStyle(fontFamily: 'Schyler', color: Colors.black),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.fromLTRB(15, 10, 10, 12),
                                        hintText: "Code",
                                        hintStyle: TextStyle(color: AppColors.violet1,
                                            fontSize: 18.0,
                                            fontFamily: 'Schyler'),
                                        fillColor: Colors.white,
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: AppColors.violet1, width: 1.0),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: AppColors.violet1, width: 1.0),
                                        ),
                                      )
                                  )
                              ),
                              SizedBox(height: 8.0),
                              Text("\tA code has been sent to your email.",
                                style: TextStyle(
                                    fontFamily: 'Schyler',
                                    color: Colors.black,
                                    fontSize: 15
                                ),
                              ),
                              SizedBox(height: 16.0),
                              StreamBuilder<ScreenState>(
                                  stream: _authBloc.activationState,
                                  builder: (context, snapshot) => _buildActivateButton(snapshot)
                              ),
                              SizedBox(height: 16.0),
                              Visibility(
                                visible: !_canResend,
                                child: Text( "You can resend in ($_currentTime)",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'Schyler', color: AppColors.violet1,
                                      fontSize: 15
                                  ),
                                ),
                              ),
                              StreamBuilder<ScreenState>(
                                  stream: _authBloc.verificationState,
                                  builder: (context, snapshot) {
                                    return _buildResendButton(snapshot);
                                  }
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Positioned(
              top: 20.0,
              left: 0.0,
              child: IconButton(
                icon: Icon(Icons.arrow_back, color: AppColors.violet1,size: 23,),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
          )
        ],
      )
    );
  }

  Widget _buildActivateButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        child: Center(child: CircularProgressIndicator()),
      );
    } else {
      return Container(
        height: 50.0,
        width: 200.0,
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(5.0),
          color: AppColors.violet1,
          child: MaterialButton(
            child: Text("ACTIVATE ACCOUNT",
              style: TextStyle(
                fontFamily: 'Schyler',
                color: Colors.white,
                fontSize: 20.0
              ),
            ),
            onPressed: _activateAccount,
          ),
        )
      );
    }
  }

  Widget _buildResendButton(AsyncSnapshot<ScreenState> snapshot) {
    return FlatButton(
      child: Text("RESEND CODE",
        style: TextStyle(
          fontFamily: 'Schyler',
          color: _canResend 
            ? AppColors.blue 
            : Colors.grey
        ),
      ),
      onPressed: _canResend ? () => _sendCode(credentials) : null
    );
  }

  void onVerificationState(ScreenState state) {
    switch (state.state) {
      case States.IDLE:
        break;
      case States.WAITING:
        break;
      case States.DONE:
        final SuccessArguments arguments = SuccessArguments(
          Text("Account Activated", style: TextStyle(fontSize: 24)),
          Text("GO TO LOGIN",style: TextStyle(color: Colors.white),),
          Routes.LOGIN
        );
        Navigator.of(context).pushNamedAndRemoveUntil(
          Routes.SUCCESS_PAGE, ModalRoute.withName(Routes.LANDING),arguments: arguments);
        break;
      case States.ERROR:
        _showError(state.error.toString());
        break;
    }
  }

  void _showError(String errorMessage) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "$errorMessage",
        style: TextStyle(
          fontSize: 15.0, 
          color: Colors.white,
          fontFamily: "Schyler"),
      ),
      duration:  Duration(seconds: 3),              
    )..show(context);
  }

  void _activateAccount() {
    if (_code.text.isEmpty) return;
    final ActivationRequest request =
      ActivationRequest(activationCode: _code.text);
    _authBloc.activateAccount(request);
  }

  void _sendCode(VerificationRequest credentials) {
    setState(() {
      _canResend = false;
    });
    final VerificationRequest request = VerificationRequest(
      email: credentials.email, cardPan: credentials.cardPan);
    _authBloc.verifyAccount(request);
    _timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      setState(() {
        _currentTime -= 1;
        if (_currentTime < 1) {
          _timer.cancel();
          _currentTime = 30;
          _canResend = true;
        }
      });
    });
  }

  getBackground() async {
    var result = await dbHelper.selectImage("tbbackground" ,FORGOT_PASSWORD_BACKGROUND.toString());
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    for(var items in imagelist){
      setState(() {
        image = items["image"];
      });
    }
  }
}