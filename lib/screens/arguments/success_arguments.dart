import 'package:flutter/material.dart';

class SuccessArguments {
  final Widget text;
  final Widget buttonText;
  final String route;

  SuccessArguments(this.text, this.buttonText, this.route);
}
