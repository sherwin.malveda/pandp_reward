import 'package:plainsandprintsloyalty/model/promo.dart';

class PromoFullScreenArguments {
  final Promo promo;

  PromoFullScreenArguments(this.promo);
}
