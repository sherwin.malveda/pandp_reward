import 'package:plainsandprintsloyalty/model/request/member_request.dart';

class RegistrationArguments {
  final bool isExisting;
  final String customerId;
  final MemberRequest request;

  RegistrationArguments(this.isExisting,this.customerId, this.request);
}
