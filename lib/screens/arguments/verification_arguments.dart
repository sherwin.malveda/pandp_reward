import 'package:plainsandprintsloyalty/model/request/verification_request.dart';

class VerificationArguments {
  final VerificationRequest credentials;

  VerificationArguments(this.credentials);
}
