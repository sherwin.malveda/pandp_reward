import 'package:plainsandprintsloyalty/model/request/convert_points_request.dart';

class ConvertPointsArguments {
  final ConvertPointsRequest request;

  ConvertPointsArguments(this.request);
}
