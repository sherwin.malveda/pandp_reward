import 'dart:async';
import 'package:after_layout/after_layout.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/controllers/navigation_controller.dart';
import 'package:plainsandprintsloyalty/master_state.dart';
import 'package:plainsandprintsloyalty/model/request/balance_request.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/model/session.dart';
import 'package:plainsandprintsloyalty/screens/history_page.dart';
import 'package:plainsandprintsloyalty/screens/home_page.dart';
import 'package:plainsandprintsloyalty/screens/notification_page.dart';
import 'package:plainsandprintsloyalty/screens/profile_page.dart';
import 'package:plainsandprintsloyalty/screens/promos_page.dart';

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends MasterState<DashboardPage>
    with SingleTickerProviderStateMixin, AfterLayoutMixin<DashboardPage> {

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final NavigationController _navigationController = NavigationController();
  StreamSubscription _sessionSubscriber;

  int _currentIndex = 0;
  TabController _tabController;

  CardBloc _cardBloc;
  AuthBloc _authBloc;
  StoreBloc _storeBloc;

  @override
  void didChangeDependencies() {
    _initListeners();
    _initBlocs();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _sessionSubscriber.cancel();
    _navigationController.removeListener(_onNavigated);
    super.dispose();
  }

  void _listenToAuth() {
    _sessionSubscriber = _authBloc.session.listen(_onSession);
  }

  void _onSession(Session session) {
    try{
    if (session != null && session.accountId > -1)
      _cardBloc.getAccountDetails(session.accountId);
      _cardBloc.getBalance(BalanceRequest(
        accountId: _authBloc.session.value.accountId,
        cardPan: _authBloc.session.value.cardPan)
      );
      _cardBloc.getHistory(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _cardBloc.getVouchers(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
      _storeBloc.getBranches(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: INFINITE_ROW_LIST,
        page: 1)
      );
      _storeBloc.getPromos(ListRequest(
        accountId: _authBloc.session.value.accountId,
        rows: MAX_ROW_LIST,
        page: 1)
      );
    }catch(Exception){}
  }

  void _initBlocs() {
    _cardBloc = MasterProvider.card(context);
    _storeBloc = MasterProvider.store(context);
    _authBloc = MasterProvider.auth(context);
  }

  @override
  void initState() {
    init();

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        final notif = message['notification'];
        final data = message['data'];
        print(notif);
        print(data);
        _showNotification(notif['title'],notif['body']);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        final token = message['token'];
        final notif = message['notification'];
        final data = message['data'];
        print(message);
        print(notif);
        print(data);
        _onNavigate(2);
      },
      onResume: (Map<String, dynamic> message) async  {
        print("onResume: $message");
        _onNavigate(2);
      },
    );

    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true)
    );

    super.initState();
  }

  void _initListeners() {
    _navigationController.addListener(_onNavigated);
  }

  void _onNavigated() {
    setState(() {
      _currentIndex = _navigationController.mainIndex;
      _tabController.index = _navigationController.mainIndex;
    });
  }

  void init() {
    _tabController = TabController(length: 5, vsync: this);
    _tabController.addListener(() {
      setState(() {
        _currentIndex = _tabController.index;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      body: TabBarView(
        controller: _tabController,
        children: [
          HomePage(navigationController: _navigationController),
          HistoryPage(),
          NotificationPage(),
          PromosPage(),
          ProfilePage()
        ]
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _onNavigate,
        currentIndex: _currentIndex,
        unselectedItemColor: Colors.white38,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.white,
        backgroundColor: AppColors.violet1,
        selectedIconTheme: IconThemeData(size: 25),
        unselectedIconTheme: IconThemeData(size: 17),
        selectedLabelStyle: TextStyle(fontFamily: 'Schyler', fontWeight: FontWeight.bold, fontSize: 11, color: Colors.white),
        unselectedLabelStyle: TextStyle(fontFamily: 'Schyler', fontSize: 9, color: Colors.white38),
        items: [
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/images/home.png")),
            title: Text("Home", 
              style: TextStyle(fontFamily: 'Schyler',color: Colors.white)
            )
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/images/history.png")),
            title: Text("History",
              style: TextStyle(fontFamily: 'Schyler',color: Colors.white)
            )
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/images/Notifications.png")),
            title: Text("Notifications",
              style: TextStyle(fontFamily: 'Schyler',color: Colors.white)
            )
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/images/promos.png")),
            title: Text("Promos",
              style: TextStyle(fontFamily: 'Schyler',color: Colors.white)
            )
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage("assets/images/user.png")),
            title: Text("Account",
              style: TextStyle(fontFamily: 'Schyler',color: Colors.white)
            )
          ),
        ],
      ),
    );
  }

  void _onNavigate(int position) {
    setState(() {
      _currentIndex = position;
      _tabController.index = position;
    });
  }

  Future<void> _showNotification(String title, String body) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, title, body, platformChannelSpecifics,
        payload: body);
  }

  Future<void> onSelectNotification(String payload) async {
    if (payload != null) {
      _onNavigate(2);
    }
    _onNavigate(2);
  }

  Future<void> onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: title != null ? Text(title) : null,
        content: body != null ? Text(body) : null,
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop();
              _onNavigate(2);
            },
          )
        ],
      ),
    );
  }
  
  @override
  void afterFirstLayout(BuildContext context) {
    print("afterFirstLayout");
    _listenToAuth();
  }
}