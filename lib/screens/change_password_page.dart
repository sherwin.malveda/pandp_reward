import 'dart:async';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/form_validators.dart';
import 'package:plainsandprintsloyalty/model/request/change_password_request.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';
import 'package:plainsandprintsloyalty/widgets/input_field.dart';

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  
  final TextEditingController _currentPassword = TextEditingController();
  final TextEditingController _newPassword = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();
  final FocusNode passFocusNode = new FocusNode();
  final FocusNode newpassFocusNode = new FocusNode();
  final FocusNode confirmnewpassFocusNode = new FocusNode(); 

  StreamSubscription _stateSubs;
  AuthBloc _authBloc;

  String newpasswordstrength = "", newconfirmstrength = "";

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _stateSubs = _authBloc.changePasswordState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _stateSubs.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("CHANGE PASSWORD", style: TextStyle(fontFamily: 'Schyler'),),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: Center (
        child: Container(
          padding: EdgeInsets.all(30.0),
          child: ListView(
            children: <Widget>[
              SizedBox(height: 8.0),
              _buildLabel("Current password"),
              SizedBox(height: 8.0),
              InputField(
                focusNode: passFocusNode,
                masked: true,
                controller: _currentPassword,
                submit: (value){
                  FocusScope.of(context).requestFocus(newpassFocusNode);
                },
              ),
              SizedBox(height: 8.0),
              _buildLabel("New password"),
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      focusNode: newpassFocusNode,
                      obscureText: true,
                      style: TextStyle(fontFamily: 'Schyler'),
                      controller: _newPassword,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(5.0),
                        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                      ),
                      onChanged: (value){
                        setState(() {
                          newpasswordstrength = FormValidators.passwordStrength(_newPassword.text); 
                        });
                      },
                      onSubmitted: (value){
                        FocusScope.of(context).requestFocus(confirmnewpassFocusNode);
                      },
                    )
                  ),
                  Text(newpasswordstrength,
                    style: TextStyle(
                      fontSize: 13.0,
                      fontFamily: 'Schyler',
                      fontWeight: FontWeight.bold,
                      color: AppColors.violet1
                    ),
                  ),
                ],  
              ),
              SizedBox(height: 8.0),
              _buildLabel("Confirm password"),
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      focusNode: confirmnewpassFocusNode,
                      obscureText: true,
                      style: TextStyle(fontFamily: 'Schyler'),
                      controller: _confirmPassword,
                      decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(5.0),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
                      onChanged: (value){
                        setState(() {
                          newconfirmstrength = FormValidators.passwordStrength(_confirmPassword.text); 
                        });
                      },
                    )
                  ),
                  Text(newconfirmstrength,
                    style: TextStyle(
                      fontSize: 13.0,
                      fontFamily: 'Schyler',
                      fontWeight: FontWeight.bold,
                      color: AppColors.violet1
                    ),
                  ),
                ],  
              ),
              SizedBox(height: 24.0),
              StreamBuilder<ScreenState>(
                stream: _authBloc.changePasswordState,
                builder: (context, snapshot) =>_buildResetPasswordButton(snapshot)
              )
            ],
          ),
        )
      )
    );
  }

  Widget _buildResetPasswordButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Padding (
        padding: EdgeInsets.symmetric(horizontal: 50.0),
        child: Container(
          height: 40.0,
          decoration: BoxDecoration(
            color: AppColors.violet1,
            borderRadius: BorderRadius.all(Radius.circular(5.0))
          ),
          child: FlatButton(
            onPressed: () {},
            child: Center(
              child: CircularProgressIndicator()
            ),
          ),
        )
      );
    } else {
      return Padding (
        padding: EdgeInsets.symmetric(horizontal: 30.0),
        child: Container(
          height: 40.0,
          decoration: BoxDecoration(
            color: AppColors.violet1,
            borderRadius: BorderRadius.all(Radius.circular(5.0))
          ),
          child: FlatButton(
            onPressed: () {
              _resetPassword();
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text("CHANGE PASSWORD",
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Schyler',
                  letterSpacing: 3.0,
                  fontWeight: FontWeight.w600
                ),
                textAlign: TextAlign.center,
              )
            ),
          ),
        )
      );
    }
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        _showSuccess();
        break;
      case States.ERROR:
        _showError(state.error.toString());
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void _resetPassword() {
    
    if(newpasswordstrength == "\t\t\t Very Weak \t\t\t" || newpasswordstrength == "\t\t\t Weak \t\t\t"){
      _showError("Invalid Password. Please make sure your password is not weak");
      return;
    }

    if(_confirmPassword.text != _newPassword.text){
      _showError("Confirm password does not match");
      return;
    }

    final ChangePasswordRequest request = ChangePasswordRequest(
      accountId: _authBloc.session.value.accountId,
      currentPassword: _currentPassword.text,
      newPassword: _newPassword.text,
      confirmPassword: _confirmPassword.text
    );
    _authBloc.changePassword(request);
  
  }

  void _showError(String errorMessage) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "$errorMessage",
        style: TextStyle(
          fontSize: 15.0, 
          color: Colors.white,
          fontFamily: "Schyler"),
      ),
      duration:  Duration(seconds: 3),              
    )..show(context);
  }

  void _showSuccess() {

    setState(() {
      _confirmPassword.clear();
      _newPassword.clear();
      _currentPassword.clear();
      newpasswordstrength = "";
      newconfirmstrength = "";  
    });
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "Password Changed Successfully",
        style: TextStyle(
          fontSize: 15.0, 
          color: Colors.white,
          fontFamily: "Schyler"),
      ),
      isDismissible: true,
      duration:  Duration(seconds: 5),
      mainButton: FlatButton(
        onPressed: () {
          Navigator.pop(context);
          Navigator.pop(context);
        },
        child: Text(
          "OK",
          style: TextStyle(
            fontFamily: 'Schyler',
            fontSize: 18,
            fontWeight: FontWeight.bold),
        ),
      ),              
    )..show(context);
  }

  Padding _buildLabel(String label) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Text("$label",
        style: TextStyle(fontFamily: 'Schyler',fontSize: 16.0),
      ),
    );
  }
}