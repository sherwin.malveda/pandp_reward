import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html/dom.dart' as dom;

class NewsletterFullScreenPage extends StatefulWidget {
  final String title, content, imagepath;

  const NewsletterFullScreenPage({Key key, @required this.title,@required this.content, @required this.imagepath})
      : super(key: key);
  _NewsletterFullScreenPageState createState() => _NewsletterFullScreenPageState();
}

class _NewsletterFullScreenPageState extends State<NewsletterFullScreenPage> {

    // Set the background
  _bgimage(String image) {
    if (image.length > 0) {
     return CachedNetworkImage(
          imageUrl: image,
            fit: BoxFit.contain,
            height: (MediaQuery.of(context).size.width * (9/16)),
            width: MediaQuery.of(context).size.height * 1.0,
          placeholder: (context, url) => new Padding(
            padding: EdgeInsets.symmetric(vertical: 30),
            child:Center (
              child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)
            )
          ),
          errorWidget: (context, url, error) =>new Container(
            child:Padding(
              padding: EdgeInsets.symmetric(vertical: 50.0),
              child:Center(
                  child:  Text("Failed to load the image.\nPlease check your internet connection and try again!",
                    style: TextStyle(fontFamily: 'Schyler', fontSize: 12.0,), textAlign: TextAlign.center,
                  )
              ),
            ),
          )
        );
    }
    else {
      return new Container(
        height: (MediaQuery.of(context).size.width * (9/16)),
        width: MediaQuery.of(context).size.height * 1.0,
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/images/bannerdefault.jpg'),
            fit: BoxFit.contain,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      body: Stack(
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _bgimage("${widget.imagepath}"),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                        child:Text("${widget.title}",
                        style: TextStyle(
                          fontFamily: 'Schyler',
                          fontSize: 20.0,
                          color: AppColors.violet2,
                          fontWeight: FontWeight.bold
                        ),)
                      ),
                      Expanded(
                        child: ListView(
                          padding: EdgeInsets.all(0),
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20.0),
                              child: Html(
                                data: ''+widget.content+'',
                                defaultTextStyle: TextStyle(
                                    color: AppColors.violet1,
                                    height: 1.5,
                                    fontSize: 13
                                ),
                                linkStyle: const TextStyle(
                                    decoration: TextDecoration.underline
                                ),
                                onLinkTap: (url) async{
                                  if (await canLaunch(url)) {
                                    await launch(url);
                                  } else {
                                    throw 'Could not launch $url';
                                  }
                                },
                                onImageTap: (src) {
                                  print(src);
                                },
                                customRender: (node, children) {
                                  if (node is dom.Element) {
                                    switch (node.localName) {
                                      case "custom_tag":
                                        return Column(children: children);
                                    }
                                  }
                                  return null;
                                },
                                customTextAlign: (dom.Node node) {
                                  if (node is dom.Element) {
                                    switch (node.localName) {
                                      case "p":
                                        return TextAlign.start;
                                      case "h":
                                        return TextAlign.start;
                                    }
                                  }
                                  return null;
                                },
                                customTextStyle: (dom.Node node, TextStyle baseStyle) {
                                  if (node is dom.Element) {
                                    switch (node.localName) {
                                      case "p":
                                        return baseStyle.merge(TextStyle(color: AppColors.violet1, height: 1.5, fontSize: 13));
                                      case "h":
                                        return baseStyle.merge(TextStyle(color: AppColors.violet1, height: 1.5, fontSize: 13));
                                    }
                                  }
                                  return baseStyle;
                                },
                              ),
                            )
                          ],
                        )
                      )
                    ],
                  ),
                  Positioned(
                    top: 20.0,
                    left: 0.0,
                    child: IconButton(
                      icon: Icon(Icons.arrow_back, color: AppColors.violet1,size: 23,),
                      onPressed: () {Navigator.pop(context);},
                    )
                  )
                ],
              ),
    );
  }
}