import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/widgets/faqs_item.dart';

class FAQsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: AppColors.blue2,
        appBar: AppBar(
          backgroundColor: AppColors.violet1,
          title: Text("FAQs",style: TextStyle(fontFamily: 'Schyler')),
          centerTitle: true,
          automaticallyImplyLeading: true,
          leading: IconButton(icon:Icon(Icons.arrow_back),
            onPressed:() => Navigator.pop(context, false),
          )
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView.builder(
            itemBuilder: (BuildContext context, int index) => FAQsItem(faqsData[index]),
            itemCount: faqsData.length,
          )
        ),
      ),
    );
  }
}