import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/history.dart';
import 'package:plainsandprintsloyalty/model/offline_data_model/transactionoffline.dart';
import 'package:plainsandprintsloyalty/model/request/balance_request.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:plainsandprintsloyalty/widgets/transaction_row.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  CardBloc _cardBloc;
  AuthBloc _authBloc;

  RefreshController _refreshController = RefreshController(initialRefresh: false);
  final dbHelper = DatabaseHelper.instance;
  List<TransactionOffline> transactionoffline = new List<TransactionOffline>();

  @override
  void didChangeDependencies() {
    _cardBloc = MasterProvider.card(context);
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  void initState(){
    super.initState();
    getTransaction();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        centerTitle: true,
        title: Text("TRANSACTION HISTORY",
          style: TextStyle(fontFamily: 'Schyler',color: Colors.white)
        ),
        automaticallyImplyLeading: false,
      ),
      body: StreamBuilder(
        stream: _cardBloc.history,
        builder: (BuildContext context, AsyncSnapshot<History> snapshot) {
          return StreamHandler(
              snapshot: snapshot,
              loading: Center(child: CircularProgressIndicator()),
              noData: offline(),
              withError: (error) => offline(),
              withData: (History history) {
                saveOffline(history);
                return  SmartRefresher(
                  enablePullDown: true,
                  enablePullUp: true,
                  header: ClassicHeader(
                    refreshingText: "Refreshing",
                    textStyle: TextStyle(fontFamily: 'Schyler'),
                  ),
                  footer: CustomFooter(
                  builder: (BuildContext context,LoadStatus mode){
                    Widget body ;
                    if(mode == LoadStatus.idle){
                      body =  Text("No more items", style: TextStyle(fontFamily: 'Schyler'));
                    }
                    else if(mode==LoadStatus.loading){
                      body =  CupertinoActivityIndicator();
                    }
                    else if(mode == LoadStatus.failed){
                      body = Text("Load Failed! Click retry!", style: TextStyle(fontFamily: 'Schyler'));
                    }
                    else if(mode == LoadStatus.canLoading){
                        body = Text("Release to load more", style: TextStyle(fontFamily: 'Schyler'));
                    }
                    else{
                      body = Text("No more items", style: TextStyle(fontFamily: 'Schyler'),);
                    }
                    return Container(
                      height: 55.0,
                      child: Center(child:body),
                    );
                    },
                  ),
                  controller: _refreshController,
                  onRefresh: ()=> _onRefresh(),
                  onLoading: ()=> _onLoading(),
                  child:ListView.builder(
                    itemCount: history.transactions.length,
                    itemBuilder: (BuildContext context, int index) {
                      return TransactionRow(
                        branch: history.transactions[index].branch,
                        invoiceNumber: history.transactions[index].invoiceNumber,
                        transactionType: history.transactions[index].transactionType,
                        storeLocation: history.transactions[index].storeLocation,
                        endBalance: history.transactions[index].endBalance,
                        earnedPoints: history.transactions[index].earnedPoints,
                        transactionAmount: history.transactions[index].transactionAmount,
                        redeemedPoints: history.transactions[index].redeemedPoints,
                        transactionStatus: history.transactions[index].transactionStatus,
                        transactionDate : history.transactions[index].transactionDate,
                      );
                    }
                  )
                );
              },
          );
        }
      ),
    );
  }

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      _cardBloc.getHistory(ListRequest(
          accountId: _authBloc.session.value.accountId,
          rows: INFINITE_ROW_LIST,
          page: 1)
      );
      _cardBloc.getBalance(BalanceRequest(
        accountId: _authBloc.session.value.accountId,
        cardPan: _authBloc.session.value.cardPan)
      );
    });
    _refreshController.refreshCompleted();
  }

  
  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    if (mounted)
      setState(() {
        _cardBloc.getHistory(ListRequest(
            accountId: _authBloc.session.value.accountId,
            rows: INFINITE_ROW_LIST,
            page: 1
          )
        );
        _cardBloc.getBalance(BalanceRequest(
          accountId: _authBloc.session.value.accountId,
          cardPan: _authBloc.session.value.cardPan)
        );
      });
    _refreshController.loadComplete();
  }

  saveOffline(History history) async{
    await Insert.dbHelper.delete("tbtransaction");
    for(int x=0; x<history.transactions.length; x++){
      Insert.insertTransaction(
          history.transactions[x].branch, history.transactions[x].invoiceNumber,
          history.transactions[x].transactionAmount.toString(), history.transactions[x].earnedPoints.toString(),
          history.transactions[x].redeemedPoints.toString(), history.transactions[x].storeLocation,
          history.transactions[x].endBalance.toString(),history.transactions[x].transactionType,
          history.transactions[x].transactionStatus.toString(), history.transactions[x].transactionDate.toString()
      );
    }
  }

  getTransaction() async {
    var result = await dbHelper.selectAllData("tbtransaction");
    List<Map<String, dynamic>> transactionList = result;
    final transactionlist = transactionList;
    transactionoffline.clear();
    for(var items in transactionlist){
      transactionoffline.add(new TransactionOffline(
        items["branch"], items["invoice"], items["type"], items["location"],
        double.parse(items["balance"]), double.parse(items["epoints"]),double.parse(items["amount"]),
        double.parse(items["rpoints"]), int.parse(items["status"]), DateTime.parse(items["date"])));
    }
  }

  offline(){
    return SmartRefresher(
      enablePullDown: true,
      header: ClassicHeader(
        refreshingText: "Refreshing",
        textStyle: TextStyle(fontFamily: 'Schyler'),
      ),
      controller: _refreshController,
      onRefresh: ()=> _onRefresh(),
      child: transactionoffline.isEmpty
          ? Center(child: Text("No transactions at the moment.",style: TextStyle(fontFamily: 'Schyler')))
          : ListView.builder(
          itemCount: transactionoffline.length,
          itemBuilder: (BuildContext context, int index) {
            return TransactionRow(
              branch: transactionoffline[index].branch,
              invoiceNumber: transactionoffline[index].invoiceNumber,
              transactionType: transactionoffline[index].transactionType,
              storeLocation: transactionoffline[index].storeLocation,
              endBalance: transactionoffline[index].endBalance,
              earnedPoints: transactionoffline[index].earnedPoints,
              transactionAmount: transactionoffline[index].transactionAmount,
              redeemedPoints: transactionoffline[index].redeemedPoints,
              transactionStatus: transactionoffline[index].transactionStatus,
              transactionDate : transactionoffline[index].transactionDate,
            );
          }
      )
    );
  }
}
