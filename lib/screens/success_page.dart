import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/screens/arguments/success_arguments.dart';
import 'package:plainsandprintsloyalty/widgets/accent_button.dart';

class SuccessPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final SuccessArguments successArguments =ModalRoute.of(context).settings.arguments;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("Success", style: TextStyle(fontFamily: 'Schyler')),
        elevation: 0.0,
        brightness: Brightness.light,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(height: 100.0),
          Icon(Icons.done, size: 200.0,color: AppColors.violet1,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 24.0),
              successArguments.text,
              SizedBox(height: 24.0),
              AccentButton(
                child: successArguments.buttonText,
                onPressed: () => Navigator.of(context).pushReplacementNamed(successArguments.route),
              )
            ]
          )
        ],
      ),
    );
  }
}