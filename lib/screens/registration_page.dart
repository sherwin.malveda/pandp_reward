import 'dart:async';
import 'dart:convert';
import 'package:after_layout/after_layout.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/form_validators.dart';
import 'package:plainsandprintsloyalty/model/city.dart';
import 'package:plainsandprintsloyalty/model/item.dart';
import 'package:plainsandprintsloyalty/model/provinces.dart';
import 'package:plainsandprintsloyalty/model/request/member_request.dart';
import 'package:plainsandprintsloyalty/model/request/verification_request.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/arguments/registration_arguments.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';
import 'package:plainsandprintsloyalty/widgets/input_field.dart';
import 'package:plainsandprintsloyalty/widgets/primary_button.dart';
import 'package:plainsandprintsloyalty/widgets/searchable_list_picker.dart';
import 'package:plainsandprintsloyalty/widgets/selector_field.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:super_tooltip/super_tooltip.dart';

class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> 
    with AfterLayoutMixin<RegistrationPage> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
 
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();
  final TextEditingController _mobileNumberController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  TextEditingController _customerIdController = TextEditingController();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _middleNameController = TextEditingController();
  
  final FocusNode usernameFocusNode = new FocusNode();
  final FocusNode passwordFocusNode = new FocusNode();
  final FocusNode confirmpasswordFocusNode = new FocusNode();
  final FocusNode firstnameFocusNode = new FocusNode();
  final FocusNode lastnameFocusNode = new FocusNode();
  final FocusNode middlenameFocusNode = new FocusNode();
  final FocusNode emailFocusNode = new FocusNode();
  final FocusNode mobileFocusNode = new FocusNode();
  
  AuthBloc _authBloc;
  StreamSubscription _stateSubs;
  StreamSubscription _registrationCredsSub;

  DateTime _birthday;
  Item _city;
  Item _province;
  bool checkBox = false, checkBox1 = false;
  String passwordstrength = "", confirmstrength = "";

  int provinceId = 0;
  SuperTooltip tooltip;

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    if (_authBloc.registerState.hasListener) _stateSubs?.cancel();
    _stateSubs = _authBloc.registerState.listen(onStateChanged);
    if (_authBloc.registrationCredentials.hasListener)
      _registrationCredsSub?.cancel();
    _registrationCredsSub =
        _authBloc.registrationCredentials.listen(onRegistrationCredentials);

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _stateSubs.cancel();
    super.dispose(); 
  }

  void existingMember(RegistrationArguments args) {
    final MemberRequest request = args.request;
    _authBloc.existingmember(request);
  }

  @override
  void initState(){
    super.initState();
    provinceList.clear();
    getProvinceList();
    cityList.clear();
    getCityList(provinceId);
  }

  @override
  Widget build(BuildContext context) {
    final RegistrationArguments args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: 
      args.isExisting ? StreamBuilder<User>(
        stream: _authBloc.existingMember,
        builder: (context, snapshot) {
          return StreamHandler(
            snapshot: snapshot,
            noData: Stack(
              children: <Widget> [
                Container(
                  child: Center(
                    child:Text("No record found", style: TextStyle(color: Colors.black, fontFamily: 'Schyler'),),
                  )
                ),
              ]
            ),
            loading: Center(child: CircularProgressIndicator()),
            withError: (error) => Stack(
              children: <Widget> [
                Container(
                  child: Center(
                    child:Text("$error",
                      style: TextStyle(fontFamily: 'Schyler'),
                      textAlign: TextAlign.center
                    ),
                  )
                ),
              ]
            ),
            withData: (User user) => Container(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Container(
                      child: Text("Complete your personal details",
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'Schyler',
                          color: AppColors.violet1
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    Text("( Field with * is required )",
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: 'Schyler',
                            color: AppColors.violet1
                        ),
                        textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 30.0,),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 15.0),
                        child: ListView(
                          padding: EdgeInsets.all(0),
                          children: <Widget>[
                            SizedBox(height: 5.0),
                            Visibility(
                              visible: false,
                              child: InputField(
                                autoValidate: true,
                                hintText: "Customer ID",
                                controller: _customerIdController = 
                                  TextEditingController( text: args.customerId)),
                            ),
                            SizedBox(height: 5.0),
                            InputField(
                              focusNode: usernameFocusNode,
                              autoValidate: true,
                              validator: _validateUsername,
                              hintText: "Username *",
                              controller: _usernameController,
                              submit: (value){
                                FocusScope.of(context).requestFocus(passwordFocusNode);
                              },
                            ),
                            SizedBox(height: 5.0),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child:TextFormField(
                                    focusNode: passwordFocusNode,
                                    obscureText: true,
                                    style: TextStyle(fontFamily: 'Schyler'),
                                    controller: _passwordController,
                                    decoration: InputDecoration(
                                      labelText: "Password *",
                                      contentPadding: EdgeInsets.all(5.0),
                                      border:OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                                    ),
                                    onChanged: (value){
                                      setState(() {
                                        passwordstrength = FormValidators.passwordStrength(_passwordController.text); 
                                      });
                                    },
                                    onTap: (){
                                      showTooltip();
                                      tooltip.show(context);
                                    },
                                    onFieldSubmitted: (value){
                                      tooltip.close();
                                      FocusScope.of(context).requestFocus(confirmpasswordFocusNode);
                                    },
                                  ),
                                ),
                                Text(passwordstrength,
                                  style: TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Schyler',
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.violet1
                                  ),
                                ),
                              ],  
                            ),
                            SizedBox(height: 5.0),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child:TextFormField(
                                    focusNode: confirmpasswordFocusNode,
                                    obscureText: true,
                                    style: TextStyle(fontFamily: 'Schyler'),
                                    controller: _confirmPasswordController,
                                    decoration: InputDecoration(
                                      labelText: "Confirm Password *",
                                      contentPadding: EdgeInsets.all(5.0),
                                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                                    ),
                                    validator: _validateConfirmPassword,
                                    autovalidate: true,
                                    onChanged: (value){
                                      setState(() {
                                        confirmstrength = FormValidators.passwordStrength(_confirmPasswordController.text); 
                                      });
                                    },
                                    onFieldSubmitted: (value){
                                    FocusScope.of(context).requestFocus(lastnameFocusNode);
                                    },
                                  ),
                                ),
                                Text(confirmstrength,
                                  style: TextStyle(
                                    fontSize: 13.0,
                                    fontFamily: 'Schyler',
                                    fontWeight: FontWeight.bold,
                                    color: AppColors.violet1
                                  ),
                                ),
                              ],  
                            ),
                            SizedBox(height: 5.0),
                            InputField(
                              focusNode: lastnameFocusNode,
                              hintText: "Last Name *",
                              autoValidate: true,
                              validator: _validateName,
                              textCapitalization: TextCapitalization.words,
                              controller: _lastNameController = TextEditingController( text :user.lastName),
                              submit: (value){
                                FocusScope.of(context).requestFocus(firstnameFocusNode);
                              },
                            ),
                            SizedBox(height: 5.0),
                            InputField(
                              focusNode: firstnameFocusNode,
                              hintText: "First Name *",
                              validator: _validateName,
                              autoValidate: true,
                              textCapitalization: TextCapitalization.words,
                              controller: _firstNameController = TextEditingController( text :user.firstname),
                              submit: (value){
                                FocusScope.of(context).requestFocus(middlenameFocusNode);
                              },
                            ),
                            SizedBox(height: 5.0),
                            InputField(
                              focusNode: middlenameFocusNode,
                              hintText: "Middle Name",
                              autoValidate: true,
                              validator: _validateName,
                              textCapitalization: TextCapitalization.words,
                              controller: _middleNameController = TextEditingController( text :user.middleName)
                            ),
                            SizedBox(height: 5.0),
                            SelectorField(
                              child: Text(
                                "${DateFormat('MMMM dd, yyyy').format( _birthday = user.birthDate)}",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: 'Schyler',
                                  color: Colors.black
                                ),
                              ),
                              onPressed: (){
                                _showDatetimePicker();
                              }
                            ),
                            SizedBox(height: 5.0),
                            InputField(
                              focusNode: emailFocusNode,
                              hintText: "Email *",
                              autoValidate: true,
                              validator: _validateEmail,
                              keyboardType: TextInputType.emailAddress,
                              controller: _emailController,
                              submit: (value){
                                FocusScope.of(context).requestFocus(mobileFocusNode);
                              },
                            ),
                            SizedBox(height: 5.0),
                            Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(5.0)
                              ),
                              child:Row(
                                children: <Widget>[
                                  SizedBox(width: 10.0,),
                                  Text("+63",
                                      style: TextStyle(color:Colors.black,
                                          fontWeight: FontWeight.bold)),
                                  SizedBox(width: 5.0),
                                  Flexible(
                                    child: TextFormField(
                                      focusNode: mobileFocusNode,
                                      validator: _validateNumber,
                                      autovalidate: true,
                                      maxLength: 10,
                                      style: TextStyle(fontFamily: 'Schyler'),
                                      keyboardType: TextInputType.phone,
                                      controller: _mobileNumberController,
                                      decoration: InputDecoration(
                                        counterText: '',
                                        hintText: "Mobile Number *",
                                        contentPadding: EdgeInsets.all(5.0),
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                          color: Colors.transparent, width: 0.0),
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                          color: Colors.transparent, width: 0.0),
                                        ),
                                      )
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 5.0),
                            SelectorField(
                              child: Text(
                                "${_province == null ? "Province *" : _province.value}",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontFamily: 'Schyler',
                                    color: _province == null
                                        ? Theme.of(context).hintColor
                                        : Colors.black
                                ),
                              ),
                              onPressed: _provincePicker,
                            ),
                            SizedBox(height: 5.0),
                            SelectorField(
                              child: Text(
                                "${_city == null
                                    ? "City *"
                                    : cityList[_city?.id] == null
                                      ? 'Please select ...'
                                      :_city.value}",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontFamily: 'Schyler',
                                  color: _city == null
                                    ? Theme.of(context).hintColor
                                    : Colors.black
                                ),
                              ),
                              onPressed: _cityPicker,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Checkbox(
                                  value: checkBox1,
                                  activeColor: AppColors.violet1,
                                  onChanged: (value) {
                                    setState(() {
                                      checkBox1 = value;
                                    });
                                  },
                                ),
                                Text("Subscribe on our newsletter",
                                  style: TextStyle(color:Colors.black,fontSize: 8, fontFamily: 'Schyler'),),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Checkbox(
                                  value: checkBox,
                                  activeColor: AppColors.violet1,
                                  onChanged: (value) {
                                    setState(() {
                                      checkBox = value;
                                    });
                                  },
                                ),
                                Row(
                                  children: <Widget>[
                                    Text("I have read and accepted the",
                                      style: TextStyle(color:Colors.black,fontSize:8, fontFamily: 'Schyler'),),
                                    FlatButton(
                                        padding: EdgeInsets.all(0.0),
                                        child: Text(" Terms & Conditions and Privacy Policy",
                                          style: TextStyle(color:Colors.black,
                                              fontSize: 9,
                                              fontFamily: 'Schyler',
                                              fontStyle: FontStyle.italic,
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                        onPressed: () => Navigator.of(context).pushNamed(Routes.TERMS_AND_CONDITIONS)
                                    )
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: 10.0)
                          ],
                        ),
                      )
                    ),
                    Container(
                      height: 60.0,
                      child: StreamBuilder<ScreenState>(
                        stream: _authBloc.registerState,
                        builder: (context, snapshot) {
                          return _buildSubmitButton(snapshot);
                        }
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      )
      : Container(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
            Container(
              child: Text(
                "Complete your personal details",
                style: TextStyle(
                  fontSize: 20,
                  fontFamily: 'Schyler',
                  color: AppColors.violet1
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: 5.0,),
            Text("( Field with * is required )",
              style: TextStyle(
                fontSize: 15,
                fontFamily: 'Schyler',
                color: AppColors.violet1
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20.0,),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal:15),
                child: ListView(
                  children: <Widget>[
                    SizedBox(height: 5.0),
                    InputField(
                      focusNode: usernameFocusNode,
                      autoValidate: true,
                      validator: _validateUsername,
                      hintText: "Username *",
                      controller: _usernameController,
                      submit: (value){
                        FocusScope.of(context).requestFocus(passwordFocusNode);
                      },
                    ),
                    SizedBox(height: 5.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child:TextFormField(
                            focusNode: passwordFocusNode,
                            obscureText: true,
                            style: TextStyle(fontFamily: 'Schyler'),
                            controller: _passwordController,
                            decoration: InputDecoration(
                              labelText: "Password *",
                              contentPadding: EdgeInsets.all(5.0),
                              border:OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                            ),
                            onChanged: (value){
                              setState(() {
                                passwordstrength = FormValidators.passwordStrength(_passwordController.text); 
                              });
                            },
                            onTap: (){
                              showTooltip();
                              tooltip.show(context);
                            },
                            onFieldSubmitted: (value){
                              tooltip.close();
                              FocusScope.of(context).requestFocus(confirmpasswordFocusNode);
                            },
                          ),
                        ),
                        Text(passwordstrength,
                          style: TextStyle(
                            fontSize: 13.0,
                            fontFamily: 'Schyler',
                            fontWeight: FontWeight.bold,
                            color: AppColors.violet1
                          ),
                        ),
                      ],  
                    ),
                    SizedBox(height: 5.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child:TextFormField(
                            focusNode: confirmpasswordFocusNode,
                            obscureText: true,
                            style: TextStyle(fontFamily: 'Schyler'),
                            controller: _confirmPasswordController,
                            decoration: InputDecoration(
                              labelText: "Confirm Password *",
                              contentPadding: EdgeInsets.all(5.0),
                              border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))
                            ),
                            validator: _validateConfirmPassword,
                            autovalidate: true,
                            onChanged: (value){
                              setState(() {
                                confirmstrength = FormValidators.passwordStrength(_confirmPasswordController.text); 
                              });
                            },
                            onFieldSubmitted: (value){
                              FocusScope.of(context).requestFocus(lastnameFocusNode);
                            },
                          ),
                        ),
                        Text(confirmstrength,
                          style: TextStyle(
                            fontSize: 13.0,
                            fontFamily: 'Schyler',
                            fontWeight: FontWeight.bold,
                            color: AppColors.violet1
                          ),
                        ),
                      ],  
                    ),
                    SizedBox(height: 5.0),
                    InputField(
                      focusNode: lastnameFocusNode,
                      hintText: "Last Name *",
                      autoValidate: true,
                      validator: _validateName,
                      textCapitalization: TextCapitalization.words,
                      controller: _lastNameController,
                      submit: (value){
                        FocusScope.of(context).requestFocus(firstnameFocusNode);
                      },
                    ),
                    SizedBox(height: 5.0),
                    InputField(
                      focusNode: firstnameFocusNode,
                      hintText: "First Name *",
                      validator: _validateName,
                      autoValidate: true,
                      textCapitalization: TextCapitalization.words,
                      controller: _firstNameController,
                      submit:(value){
                        FocusScope.of(context).requestFocus(middlenameFocusNode);
                      }
                    ),
                    SizedBox(height: 5.0),
                    InputField(
                      focusNode: middlenameFocusNode,
                      hintText: "Middle Name",
                      autoValidate: true,
                      validator: _validateName,
                      textCapitalization: TextCapitalization.words,
                      controller: _middleNameController,
                    ),
                    SizedBox(height: 5.0),
                    SelectorField(
                      child: Text(
                        "${_birthday == null ? "Birthday *" : DateFormat('MMMM dd, yyyy').format(_birthday)}",
                        style: TextStyle(
                          fontSize: 16.0,
                          fontFamily: 'Schyler',
                          color: _birthday == null
                            ? Theme.of(context).hintColor
                            : Colors.black
                        ),
                      ),
                      onPressed: (){
                        _showDatetimePicker();
                      }
                    ),
                    SizedBox(height: 5.0),
                    InputField(
                      focusNode: emailFocusNode,
                      hintText: "Email *",
                      autoValidate: true,
                      validator: _validateEmail,
                      keyboardType: TextInputType.emailAddress,
                      controller: _emailController,
                      submit: (value){
                        FocusScope.of(context).requestFocus(mobileFocusNode);
                      },
                    ),
                    SizedBox(height: 5.0),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.circular(5.0)
                      ),
                      child:Row(
                        children: <Widget>[
                          SizedBox(width: 10.0,),
                          Text("+63",
                            style: TextStyle(color:Colors.black, fontWeight: FontWeight.bold)),
                          SizedBox(width: 5.0),
                          Flexible(
                            child: TextFormField(
                              focusNode: mobileFocusNode,
                              validator: _validateNumber,
                              autovalidate: true,
                              maxLength: 10,
                              style: TextStyle(fontFamily: 'Schyler'),
                              keyboardType: TextInputType.phone,
                              controller: _mobileNumberController,
                              decoration: InputDecoration(
                                counterText: '',
                                hintText: "Mobile Number *",
                                contentPadding: EdgeInsets.all(5.0),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                  color: Colors.transparent, width: 0.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                  color: Colors.transparent, width: 0.0),
                                ),
                              )
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 5.0),
                    SelectorField(
                      child: Text(
                        "${_province == null ? "Province *" : _province.value}",
                        style: TextStyle(
                            fontSize: 16.0,
                            fontFamily: 'Schyler',
                            color: _province == null
                                ? Theme.of(context).hintColor
                                : Colors.black
                        ),
                      ),
                      onPressed: _provincePicker,
                    ),
                    SizedBox(height: 5.0),
                    SelectorField(
                        child: Text(
                          "${_city == null
                              ? "City *"
                              : cityList[_city?.id] == null
                                ? 'Please select ...'
                                :_city.value}",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontFamily: 'Schyler',
                            color: _city == null
                              ? Theme.of(context).hintColor
                              : Colors.black
                          ),
                        ),
                        onPressed: _cityPicker,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Checkbox(
                            value: checkBox1,
                            activeColor: AppColors.violet1,
                            onChanged: (value) {
                              setState(() {
                                checkBox1 = value;
                              });
                            },
                          ),
                          Text("Subscribe on our newsletter",
                            style: TextStyle(color:Colors.black, fontSize: 8, fontFamily: 'Schyler'),),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Checkbox(
                            value: checkBox,
                            activeColor: AppColors.violet1,
                            onChanged: (value) {
                              setState(() {
                                checkBox = value;
                              });
                            },
                          ),
                          Row(
                            children: <Widget>[
                              Text("I have read and accepted the",
                                style: TextStyle(color:Colors.black,fontSize:8, fontFamily: 'Schyler'),),
                              FlatButton(
                                padding: EdgeInsets.all(0.0),
                                child: Text(" Terms & Conditions and Privacy Policy",
                                  style: TextStyle(color:Colors.black,
                                    fontSize: 9,
                                    fontFamily: 'Schyler',
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold
                                  ),
                                ),
                                onPressed: () => Navigator.of(context).pushNamed(Routes.TERMS_AND_CONDITIONS)
                              )
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 10.0),
                    ],
                  ),
                )
              ),
              Container(
                height: 60.0,
                child: StreamBuilder<ScreenState>(
                  stream: _authBloc.registerState,
                  builder: (context, snapshot) {
                    return _buildSubmitButton(snapshot);
                  }
                ),
              ),
            ],
          ),
        ),
      )
    ); 
  }

  Widget _buildSubmitButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return PrimaryButton(
        child: Container(
          height: 17, 
          width: 17, 
          child: CircularProgressIndicator()
        ),
        onPressed: () {},
      );
    } else {
      return PrimaryButton(
        child: Text(
          "SUBMIT",
          style: TextStyle(
            color: Colors.white, 
            fontFamily: 'Schyler',
            fontWeight: FontWeight.bold, 
            fontSize: 20.0
          ),
        ),
        onPressed: _onSubmit,
      );
    }
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        break;
      case States.ERROR:
        _showErrorSnackBox(state.error.toString());
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void _showErrorSnackBox(String errorMessage) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "$errorMessage",
        style: TextStyle(
          fontSize: 15.0, 
          color: Colors.white,
          fontFamily: "Schyler"),
      ),
      duration:  Duration(seconds: 3),              
    )..show(context);  }

  void _navigateToEmailConfirmation(VerificationRequest credentials) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Routes.ACCOUNT_VERIFICATION2, ModalRoute.withName(Routes.LANDING),
        arguments: credentials);
  }

  String _validateUsername(value) =>
      FormValidators.isAlphanumeric(value,
          "Username must be at least 3 characters long,\nmaximum of 16 characters and must be alpha-numeric.");

 String validatePassword(value) =>
      FormValidators.isPassword(
          _passwordController.text, "Password must be 4 characters long,\n and must not be weak.");

 String _validateConfirmPassword(value) =>
      FormValidators.isIdentical(
          _passwordController.text, value, "Confirm password does not match.");

  String _validateName(String value) =>
      FormValidators.isLettersOnly(value, "Enter a valid name");

  String _validateNumber(String value) => 
      FormValidators.isMobileNumber(value, "Enter a valid mobile number");
  
  String _validateEmail(String value) =>
      FormValidators.isValidEmail(value, "Enter a valid email");

  void _cityPicker() async {
    final Item item = await Navigator.of(context).push(MaterialPageRoute(
      builder: (context) =>
        SearchableListPicker(title: "City", items: _getCity())));
    if (item != null)
      setState(() {
        _city = item;
      });
  }

  void _provincePicker() async {
    final Item item = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>
            SearchableListPicker(title: "Province", items: _getProvince())));
    if (item != null)
      setState(() {
        _province = item;
        provinceId = _province?.id;
        cityList.clear();
        getCityList(provinceId);
      });
  }

  List<Item> _getCity() =>
      cityList.keys.map((key) => Item(id: key, value: cityList[key])).toList();

  List<Item> _getProvince() =>
      provinceList.keys
          .map((key) => Item(id: key, value: provinceList[key]))
          .toList();

  void _showDatetimePicker() async {
    if(_birthday == null){
      _showDateDialog(DateTime.parse('2000-06-15'));
    }else{
      _showDateDialog(DateTime.parse(DateFormat('yyyy-MM-dd').format(_birthday)));
    }
  }

  void _showDateDialog(DateTime initDateTime) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.all(10),
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.white)
          ),
          content: Container(
            height: 280,
            child: DatePickerWidget(
              minDateTime: DateTime.parse(DateTime.now().subtract(Duration(days: 36500)).toString()),
              maxDateTime: DateTime.parse(DateTime.now().subtract(Duration(days: 6570)).toString()),
              initialDateTime: initDateTime,
              dateFormat: "MMM dd,yyyy",
              pickerTheme: DateTimePickerTheme(
                backgroundColor: Colors.white,
                cancelTextStyle: TextStyle(color: Colors.red),
                confirmTextStyle: TextStyle(color: AppColors.violet1),
                itemTextStyle: TextStyle(color: AppColors.violet1, fontSize: 16.0),
                pickerHeight: 250.0,
                titleHeight: 25.0,
                itemHeight: 30.0,
              ),
              onConfirm: (dateTime, selectedIndex) {
                setState(() {
                  _birthday = dateTime;
                });
                FocusScope.of(context).requestFocus(emailFocusNode);
              },
            ),
          ),
        );
      },
    );
  }

  void _onSubmit() {
    final String firstName = _firstNameController.text;
    final String lastName = _lastNameController.text;
    final String middleName = _middleNameController.text;
    final String mobileNumber = _mobileNumberController.text.isNotEmpty
        ? "+63${_mobileNumberController.text}"
        : "";
    final String email = _emailController.text;
    final String userName = _usernameController.text;
    final String password = _passwordController.text;
    final String cardDigit = _customerIdController.text;
    final DateTime birthday = _birthday;
    final int city = _city?.id;
    final int province = _province?.id;
    final bool subscription =  checkBox1;

    final User user = User(
      firstname: firstName,
      lastName: lastName,
      middleName: middleName,
      mobile: mobileNumber,
      address: "null",
      zipCode: "null",
      username: userName,
      password: password,
      email: email,
      birthDate: birthday,
      gender: 1,
      title: 1,
      maritalStatus: 1,
      city: city,
      province: province,
      cardDigits: cardDigit,
      subscription: subscription,
    );

    if (!user.hasNoNullsForRequiredField()) {
      _showErrorSnackBox("Fill up all the required fields");
      return;
    }


    if(cityList[city] == null){
      _showErrorSnackBox("Please check your city");
      return;
    }

    if(passwordstrength == "\t\t\t Very Weak \t\t\t" || passwordstrength == "\t\t\t Weak \t\t\t"){
      _showErrorSnackBox("Invalid Password. Please make sure your password is not weak");
      return;
    }

    if(_passwordController.text != _confirmPasswordController.text){
      _showErrorSnackBox("Confirm password does not match");
      return;
    }

    if(checkBox == false){
      _showErrorSnackBox("Please accept the terms and conditions.");
      return;
    }

    if (_formKey.currentState.validate()) {
      _authBloc.register(user);
    }
  }

  void showTooltip(){  

    tooltip = SuperTooltip(
      arrowLength: 0.0,
      arrowTipDistance: MediaQuery.of(context).size.height * 0.3,
      snapsFarAwayHorizontally: false,
      popupDirection: TooltipDirection.up,
      borderColor: AppColors.violet1,
      hasShadow: true,
      backgroundColor: Colors.white,
      outsideBackgroundColor: Color.fromARGB(0, 0, 0, 0),
      content: new Material(
        child: Padding(
          padding: const EdgeInsets.all(0.0),
          child: Text(
              "* Password should atleast 6 characters\n"+ 
               "* Password can have uppercase, lowercase, number and special character\n"+ 
               "* Password must not be weak",
            softWrap: true,
            style: TextStyle(color:AppColors.violet1)
          ),
        )
      ),
    );
  }

  void onRegistrationCredentials(VerificationRequest credentials) {
    _navigateToEmailConfirmation(credentials);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    final RegistrationArguments args = ModalRoute.of(context).settings.arguments;
    existingMember(args);
  }

  Future getProvinceList() async {
    String url = API_ENDPOINT+"/api/v1/dropdown/provinces";
    Map<String, String> headers = {"Content-type": "application/json",
      "apikey":"b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w=="};
    final response = await get(url, headers: headers);
    var responses = Provinces.fromJson(jsonDecode(response.body));
    for(int x=0; x<responses.provinces.length; x++){
      provinceList[responses.provinces[x].provinceId] = responses.provinces[x].provinceName;
    }
  }

  Future getCityList(int provinceId) async {
    String url = API_ENDPOINT+"/api/v1/dropdown/cities/"+provinceId.toString();
    Map<String, String> headers = {"Content-type": "application/json",
      "apikey":"b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w=="};
    final response = await get(url, headers: headers);
    var responses = Cities.fromJson(jsonDecode(response.body));
    for(int x=0; x<responses.cities.length; x++){
      setState(() {
        cityList[responses.cities[x].cityId] = responses.cities[x].cityName;
      });
    }
  }

}
