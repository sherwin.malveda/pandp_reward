import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/model/balance.dart';
import 'package:plainsandprintsloyalty/model/request/balance_request.dart';
import 'package:plainsandprintsloyalty/model/request/convert_points_request.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/arguments/convert_points_arguments.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConvertPointsPage extends StatefulWidget {
  @override
  _ConvertPointsPageState createState() => _ConvertPointsPageState();
}

class _ConvertPointsPageState extends State<ConvertPointsPage> {
  
  TextEditingController _amountController = TextEditingController(text: '0');
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  CardBloc _cardBloc;
  AuthBloc _authBloc;

  Color color100 = Color.fromRGBO(46, 25, 78, 0.05);
  Color fontColor100 = AppColors.violet1;
  Color color200 = Color.fromRGBO(46, 25, 78, 0.05);
  Color fontColor200 = AppColors.violet1;
  Color color300 = Color.fromRGBO(46, 25, 78, 0.05);
  Color fontColor300 = AppColors.violet1;
  Color color400 = Color.fromRGBO(46, 25, 78, 0.05);
  Color fontColor400 = AppColors.violet1;
  Color color500 = Color.fromRGBO(46, 25, 78, 0.05);
  Color fontColor500 = AppColors.violet1;
  Color color600 = Color.fromRGBO(46, 25, 78, 0.05);
  Color fontColor600 = AppColors.violet1;
  Color color700 = Color.fromRGBO(46, 25, 78, 0.05);
  Color fontColor700 = AppColors.violet1;
  double balances = 500.00;

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _cardBloc = MasterProvider.card(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("REDEEM POINTS", style: TextStyle(fontFamily: 'SChyler'),),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: SmartRefresher(
        enablePullDown: true,
        header: ClassicHeader(
          refreshingText: "Refreshing",
          textStyle: TextStyle(fontFamily: 'Schyler'),
        ),
        controller: _refreshController,
        onRefresh: ()=> _onRefresh(),
        child: ListView(
          children:<Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                      height: 150,
                      width: MediaQuery.of(context).size.height * .8,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(46, 25, 78, 0.05),
                        borderRadius: BorderRadius.all(Radius.circular(5.0))
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text("YOUR POINTS",
                          style: TextStyle(
                            fontSize: 20.0, 
                            fontFamily: 'Schyler',
                            color: AppColors.violet1
                          )
                        ),
                        Container(
                          child: StreamBuilder<Balance>(
                            stream: _cardBloc.balance,
                            builder: (context, snapshot) {
                              return StreamHandler(
                                snapshot: snapshot,
                                loading: Center(child: CircularProgressIndicator()),
                                noData: getPointsOffline(),
                                withError: (error) => getPointsOffline(),
                                withData: (Balance balance) => getPoints(balance),
                              );
                            }
                          )
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 8.0),
                  Text("Conversion",
                    style: TextStyle(
                      fontSize: 15.0, 
                      fontFamily: 'Schyler',
                      color: AppColors.violet1
                    ),
                    textAlign: TextAlign.center
                  ),
                  Text("1 point = 1 peso",
                    style: TextStyle(
                      fontSize: 15.0,
                      fontFamily: 'Schyler',
                      color: AppColors.violet1
                    ),
                    textAlign: TextAlign.center
                  ),
                  SizedBox(height: 8.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: AmountButton(
                          amount: "100",
                          color: color100,
                          fontcolor: fontColor100,
                          fontsize: 25,
                          onTap: () {
                            setState(() {
                              setColoR();
                              color100 =  AppColors.violet1;
                              fontColor100 = Colors.white;
                              _amountController.text = "100";
                            });
                          }
                        ),
                      ),
                      SizedBox(width: 5,),
                      Expanded(
                        child: AmountButton(
                          amount: "200",
                          color: color200,
                          fontcolor: fontColor200,
                          fontsize: 25,
                          onTap: () {
                            setState(() {
                              setColoR();
                              color200 =  AppColors.violet1;
                              fontColor200 = Colors.white;
                              _amountController.text = "200";
                            });
                          }
                        ),
                      ),
                      SizedBox(width: 5,),
                      Expanded(
                        child: AmountButton(
                          amount: "300",
                          color: color300,
                          fontcolor: fontColor300,
                          fontsize: 25,
                          onTap: () {
                            setState(() {
                              setColoR();
                              color300 =  AppColors.violet1;
                              fontColor300 = Colors.white;
                              _amountController.text = "300";
                            });
                          }
                        ),
                      ),
                      SizedBox(width: 5,),
                      Expanded(
                        child: AmountButton(
                          amount: "400",
                          color: color400,
                          fontcolor: fontColor400,
                          fontsize: 25,
                          onTap: () {
                            setState(() {
                              setColoR();
                              color400 =  AppColors.violet1;
                              fontColor400 = Colors.white;
                              _amountController.text = "400";
                            });
                          }
                        ),
                      )  
                    ],
                  ),
                  SizedBox(height: 10.0,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: AmountButton(
                          amount: "500",
                          color: color500,
                          fontcolor: fontColor500,
                          fontsize: 25,
                          onTap: () {
                            setState(() {
                              setColoR();
                              color500 =  AppColors.violet1;
                              fontColor500 = Colors.white;                              
                              _amountController.text = "500";
                            });
                          }
                        ),
                      ),
                      SizedBox(width: 5.0,),
                      Expanded(
                        child: AmountButton(
                          amount: "600",
                          color: color600,
                          fontcolor: fontColor600,
                          fontsize: 25,
                          onTap: () {
                            setState(() {
                              setColoR();
                              color600 =  AppColors.violet1;
                              fontColor600 = Colors.white;
                              _amountController.text = "600";
                            });
                          }
                        ),
                      ),
                      SizedBox(width: 5.0,),
                      Expanded(
                        child: AmountButton(
                          amount: "700",
                          color: color700,
                          fontcolor: fontColor700,
                          fontsize: 25,
                          onTap: () {
                            setState(() {
                              setColoR();
                              color700 =  AppColors.violet1;
                              fontColor700 = Colors.white;
                              _amountController.text = "700";
                            });
                          }
                        ),
                      ),
                      SizedBox(width: 5.0,),
                      Expanded(
                        child: AmountButton(
                          amount: "Custom",
                          color: Color.fromRGBO(46, 25, 78, 0.05),
                          fontcolor: AppColors.violet1,
                          onTap: () {
                            setState(() {
                              setColoR();
                              _amountController.text = "0";                              
                              _showAmountDialog();
                            });
                          }
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 40.0,),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 30.0),
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: AppColors.violet1,
                      borderRadius: BorderRadius.all(Radius.circular(5.0))
                    ),
                    child: FlatButton(
                      onPressed: () {
                        if(int.parse(_amountController.text) > 0){
                          _showConfirmDialog(_amountController.text);
                        }
                        else
                        {
                          _errorMessage("Please choose an amount.");
                        }
                      },
                      child: Text("Convert to Voucher",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Schyler',
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600
                        )
                      ),
                    ),
                  )
                ],
              ),
            ),
          ]
        )
      ),
    );
  }

  getPoints(Balance balance){
    balances = balance.points.first.totalAvailable;
    savePoints(balances);
    return Text(
        "${NumberFormat.decimalPattern("tl-ph")
            .format(balances)}",
        style: TextStyle(
            fontSize: 50.0,
            fontFamily: 'Schyler',
            color: AppColors.violet1,
            fontWeight: FontWeight.bold
        )
    );
  }

  savePoints(double points) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setDouble('points', points);
  }

  getPointsOffline(){
    points();
    return Text(
        "${NumberFormat.decimalPattern("tl-ph")
            .format(balances)}",
        style: TextStyle(
            fontSize: 50.0,
            fontFamily: 'Schyler',
            color: AppColors.violet1,
            fontWeight: FontWeight.bold
        )
    );
  }

  points() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    balances = prefs.getDouble('points');
  }

  void setColoR(){
    color100 = Color.fromRGBO(46, 25, 78, 0.05);
    fontColor100 = AppColors.violet1;
    color200 = Color.fromRGBO(46, 25, 78, 0.05);
    fontColor200 = AppColors.violet1;
    color300 = Color.fromRGBO(46, 25, 78, 0.05);
    fontColor300 = AppColors.violet1;
    color400 = Color.fromRGBO(46, 25, 78, 0.05);
    fontColor400 = AppColors.violet1;
    color500 = Color.fromRGBO(46, 25, 78, 0.05);
    fontColor500 = AppColors.violet1;
    color600 = Color.fromRGBO(46, 25, 78, 0.05);
    fontColor600 = AppColors.violet1;
    color700 = Color.fromRGBO(46, 25, 78, 0.05);
    fontColor700 = AppColors.violet1;
  }

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      _cardBloc.getBalance(BalanceRequest(
        accountId: _authBloc.session.value.accountId,
        cardPan: _authBloc.session.value.cardPan)
      );
      _cardBloc.getHistory(ListRequest(
          accountId: _authBloc.session.value.accountId,
          rows: INFINITE_ROW_LIST,
          page: 1
        )
      );
    });
    _refreshController.refreshCompleted();
  }

  void _showAmountDialog() {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Enter Amount', style: TextStyle(fontFamily: 'Schyler'),),
          content: TextField(
            controller: _amountController,
            style: TextStyle(fontFamily: 'Schyler'),
            keyboardType: TextInputType.number,
            onChanged: (value) {
              if (!_isNumber(value)) {
                _amountController.clear();
              }
            },
            decoration: InputDecoration(
              hintText: "Enter amount",
              hintStyle: TextStyle(fontFamily: 'Schyler'),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('CANCEL', style: TextStyle(fontFamily: 'Schyler', color: AppColors.violet1),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('CONVERT', style: TextStyle(fontFamily: 'Schyler', color: AppColors.violet1),),
              onPressed: () {
                if(int.parse(_amountController.text) > 0){
                  Navigator.of(context).pop();
                  _showConfirmDialog(_amountController.text);
                }else{
                  _errorMessage("Please choose an amount.");
                }
              },
            )
          ],
        );
      }
    );
  }

  void _showConfirmDialog(String amount) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Convert Points", style: TextStyle(fontFamily: 'Schyler'),),
          content: Text(
            "You're about to convert your $amount points to a voucher. Do you wish to continue?", 
            style: TextStyle(fontFamily: 'Schyler')
          ),
          actions: <Widget>[
            FlatButton(
              child: Text("CANCEL", 
                style: TextStyle(fontFamily: 'Schyler', color: AppColors.violet1)
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("CONTINUE", style: TextStyle(fontFamily: 'Schyler', color: AppColors.violet1),),
              onPressed: () {
                setColoR();
                if(balances >= double.parse(amount)){
                  Navigator.of(context).pop();
                  _navigateToNewVoucher(amount);
                }else{
                  _errorMessage("Your balance is not enough.");
                }
              },
            ),
          ],
        );
      },
    );
  }

  bool _isNumber(String value) {
    if (value == null) {
      return true;
    }
    final n = num.tryParse(value);
    return n != null;
  }

  void _navigateToNewVoucher(String amount) {
    final ConvertPointsRequest request = ConvertPointsRequest(
      accountId: _authBloc.session.value.accountId,
      cardPan: _authBloc.session.value.cardPan,
      points: double.parse(amount)
    );

    ConvertPointsArguments args = ConvertPointsArguments(request);
    Navigator.of(context).pushNamed(Routes.NEW_VOUCHER, arguments: args);
  }

  void _errorMessage(String message){
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(message,
        style: TextStyle(
            fontSize: 15.0,
            color: Colors.white,
            fontFamily: "Schyler"
        ),
      ),
      isDismissible: true,
      duration:  Duration(seconds: 5),
    )..show(context);
  }
}

class AmountButton extends StatelessWidget {
  final String amount;
  final Color color;
  final Color fontcolor;
  final double fontsize;
  final VoidCallback onTap;

  const AmountButton({Key key, @required this.amount, this.onTap, this.color, this.fontcolor, this.fontsize})
    : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 80,
        width: MediaQuery.of(context).size.height * .11,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(5.0))
        ),
        child: SizedBox(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4),
            child: Center(
              child: Text("$amount", 
                style: TextStyle(
                  fontFamily: 'Schyler', 
                  color: fontcolor,
                  fontSize: fontsize
                )
              ),
            ),
          ),
        ),
      ),
    );
  }
}