import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:plainsandprintsloyalty/api/session_invalidation_notifier.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/city.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/about_us_page.dart';
import 'package:plainsandprintsloyalty/screens/contact_us_page.dart';
import 'package:plainsandprintsloyalty/screens/edit_profile_page.dart';
import 'package:plainsandprintsloyalty/screens/faqs_page.dart';
import 'package:plainsandprintsloyalty/screens/terms_and_condition_page.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  CardBloc _cardBloc;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  SessionInvalidationNotifier _sessionInvalidationNotifier;
  String name = "John Doe", birthday = "Jan 01,1995", mobile = "099999999999", email = "johndoe@email.com" ,province = "Manila", city = "Manila";

  @override
  void initState(){
    super.initState();
    getData();
  }

  @override
  void didChangeDependencies() {
    _cardBloc = MasterProvider.card(context);
    _sessionInvalidationNotifier = MasterProvider.sessionInvalidator(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("ACCOUNT", style: TextStyle(fontFamily: 'Schyler'),),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: SmartRefresher(
        enablePullDown: true,
        header: ClassicHeader(
          refreshingText: "Refreshing",
          textStyle: TextStyle(fontFamily: 'Schyler'),
        ),
        controller: _refreshController,
        onRefresh: ()=> _onRefresh(),
        child:SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
              children: <Widget>[
                StreamBuilder<User>(
                    stream: _cardBloc.currentUser,
                    builder: (context, snapshot) {
                      return StreamHandler(
                          loading: Center(child: CircularProgressIndicator()),
                          snapshot: snapshot,
                          noData: offline(),
                          withError: (error) => offline(),
                          withData: (User user) {
                            getCityList(user.province);
                            return  Container(
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              child: Column(
                                  children: <Widget>[
                                    Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: <Widget>[
                                          Container(
                                              width: 190.0,
                                              height: 30.0,
                                              child: FlatButton(
                                                padding: EdgeInsets.all(5.0),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  children: <Widget>[
                                                    Text("Change Password",
                                                        style: TextStyle(
                                                            fontFamily: 'Schyler',
                                                            fontSize: 15.0,
                                                            color: AppColors.violet1,
                                                            fontWeight: FontWeight.bold
                                                        )
                                                    ),
                                                    SizedBox(width: 5.0,),
                                                    Image(
                                                        image: AssetImage('assets/images/lock.png'),
                                                        color: AppColors.violet1,
                                                        width: 10.0,
                                                        height: 10.0
                                                    )
                                                  ],
                                                ),
                                                onPressed: () => Navigator.of(context) .pushNamed(Routes.CHANGE_PASSWORD),
                                              )
                                          ),
                                          Container(
                                              width: 75.0,
                                              height: 30.0,
                                              child: FlatButton(
                                                padding: EdgeInsets.all(5.0),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  children: <Widget>[
                                                    Text("Edit",
                                                        style: TextStyle(
                                                            fontFamily: 'Schyler',
                                                            fontSize: 15.0,
                                                            color: AppColors.violet1,
                                                            fontWeight: FontWeight.bold
                                                        )
                                                    ),
                                                    SizedBox(width: 5.0,),
                                                    Image(
                                                        image: AssetImage('assets/images/Edit.png'),
                                                        color: AppColors.violet1,
                                                        width: 10.0,
                                                        height: 10.0
                                                    )
                                                  ],
                                                ),
                                                onPressed: () => Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            EditProfilePage(
                                                              user: user,
                                                            )
                                                    )
                                                ),
                                              )
                                          )
                                        ]
                                    ),
                                    SizedBox(height: 10.0),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          width: 100,
                                          child: Text("Name",
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 18.0,
                                                color: AppColors.violet1,
                                                fontWeight: FontWeight.bold
                                            )
                                          ),
                                        ),
                                        Expanded(
                                          child:Text(': ${user.firstname} ${user.middleName} ${user.lastName}',
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 15.0,
                                                color: AppColors.violet1
                                            )
                                          )
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 3.0,),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          width: 100,
                                          child: Text("Birthday",
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 18.0,
                                                color: AppColors.violet1,
                                                fontWeight: FontWeight.bold
                                            )
                                          )
                                        ),
                                        Expanded(
                                          child:Text(': ${DateFormat("MMM dd,yyyy").format(user.birthDate)}',
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 15.0,
                                                color: AppColors.violet1
                                            )
                                          )
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 3.0,),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          width: 100,
                                          child: Text("Mobile",
                                            style: TextStyle(fontFamily: 'Schyler',
                                                fontSize: 18.0,
                                                color: AppColors.violet1,
                                                fontWeight: FontWeight.bold
                                            )
                                          )
                                        ),
                                        Expanded(
                                          child:Text(': ${user.mobile}',
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 15.0,
                                                color: AppColors.violet1
                                            )
                                          )
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 3.0,),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          width: 100,
                                          child: Text("Email",
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 18.0,
                                                color: AppColors.violet1,
                                                fontWeight: FontWeight.bold
                                            )
                                          )
                                        ),
                                        Expanded(
                                          child:Text(': ${user.email}',
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 15.0,
                                                color: AppColors.violet1
                                            )
                                          )
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 3.0,),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          width: 100,
                                          child: Text("Province",
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 18.0,
                                                color: AppColors.violet1,
                                                fontWeight: FontWeight.bold
                                            )
                                          ),
                                        ),
                                        Expanded(
                                          child:Text(': ${provinceList[user.province]}',
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 15.0,
                                                color: AppColors.violet1
                                            )
                                          )
                                        )
                                      ],
                                    ),
                                    SizedBox(height: 3.0,),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          width: 100,
                                          child: Text("City",
                                            style: TextStyle(fontFamily: 'Schyler',
                                                fontSize: 18.0,
                                                color: AppColors.violet1,
                                                fontWeight: FontWeight.bold
                                            )
                                          )
                                        ),
                                        Expanded(
                                          child:Text(': ${cityList[user.city]}',
                                            style: TextStyle(
                                                fontFamily: 'Schyler',
                                                fontSize: 15.0,
                                                color: AppColors.violet1
                                            )
                                          )
                                        )
                                      ],
                                    )
                                  ]
                              ),
                            );
                        }
                      );
                    }
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 40.0,
                  width: MediaQuery.of(context).size.width *0.8,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(46, 25, 78, 0.05),
                      borderRadius: BorderRadius.all(Radius.circular(5.0))
                  ),
                  child: FlatButton(
                    onPressed: () => Navigator.of(context).push(
                        PageTransition(
                            type: PageTransitionType.rippleRightUp,
                            child: TermsAndConditionPage()
                        )
                    ),
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child:Text("Terms & Conditions",
                            style: TextStyle(
                                color: AppColors.violet1,
                                fontFamily: 'Schyler',
                                fontWeight: FontWeight.w500
                            )
                        )
                    ),
                  ),
                ),
                SizedBox(height: 7.0),
                Container(
                  height: 40.0,
                  width: MediaQuery.of(context).size.width *0.8,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(46, 25, 78, 0.05),
                      borderRadius: BorderRadius.all(Radius.circular(5.0))
                  ),
                  child: FlatButton(
                    onPressed: () => Navigator.of(context).push(
                        PageTransition(
                            type: PageTransitionType.rippleRightUp,
                            child: AboutUsPage()
                        )
                    ),
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 50.0),
                        child:Text("About Us",
                            style: TextStyle(
                                color: AppColors.violet1,
                                fontFamily: 'Schyler',
                                fontWeight: FontWeight.w500
                            )
                        )
                    ),
                  ),
                ),
                SizedBox(height: 7.0),
                Container(
                  height: 40.0,
                  width: MediaQuery.of(context).size.width *.8,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(46, 25, 78, 0.05),
                      borderRadius: BorderRadius.all(Radius.circular(5.0))
                  ),
                  child: FlatButton(
                    onPressed: () => Navigator.of(context).push(
                        PageTransition(
                            type: PageTransitionType.rippleRightUp,
                            child: ContactUsPage()
                        )
                    ),
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 45.0),
                        child: Text("Contact Us",
                            style: TextStyle(
                                color: AppColors.violet1,
                                fontFamily: 'Schyler',
                                fontWeight: FontWeight.w500
                            )
                        )
                    ),
                  ),
                ),
                SizedBox(height: 7.0),
                Container(
                  height: 40.0,
                  width: MediaQuery.of(context).size.width *.8,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(46, 25, 78, 0.05),
                      borderRadius: BorderRadius.all(Radius.circular(5.0))
                  ),
                  child: FlatButton(
                    onPressed: () => Navigator.of(context).push(
                        PageTransition(
                            type: PageTransitionType.rippleRightUp,
                            child: FAQsPage()
                        )
                    ),
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 63.0),
                        child: Text("FAQs",
                            style: TextStyle(
                                color: AppColors.violet1,
                                fontFamily: 'Schyler',
                                fontWeight: FontWeight.w500
                            )
                        )
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 40.0,
                  decoration: BoxDecoration(
                      color: AppColors.violet1,
                      borderRadius: BorderRadius.all(Radius.circular(5.0))
                  ),
                  child: FlatButton(
                    onPressed: () {
                      _firebaseMessaging.deleteInstanceID();
                      _sessionInvalidationNotifier.notifyListeners();
                      clearData();
                    },
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 50),
                        child: Text("LOG OUT",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Schyler',
                                letterSpacing: 3.0,
                                fontWeight: FontWeight.w600
                            )
                        )
                    ),
                  ),
                )
              ],
            ),
          ),
          ),
        ),
    );
  }

  getData() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      name = prefs.getString('name');
      birthday = prefs.getString('birthday');
      mobile = prefs.getString('mobile');
      email = prefs.getString('email');
      province = prefs.getString('province');
      city = prefs.getString('city');
  }

  offline(){
    getData();
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
          children: <Widget>[
            SizedBox(height: 30.0),
            Row(
              children: <Widget>[
                Container(
                  width: 100,
                  child: Text("Name",
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 18.0,
                        color: AppColors.violet1,
                        fontWeight: FontWeight.bold
                    )
                  )
                ),
                Expanded(
                  child:Text(": "+name.toString(),
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 15.0,
                        color: AppColors.violet1
                    )
                  )
                )
              ],
            ),
            SizedBox(height: 3.0,),
            Row(
              children: <Widget>[
                Container(
                  width: 100,
                  child: Text("Birthday",
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 18.0,
                        color: AppColors.violet1,
                        fontWeight: FontWeight.bold
                    )
                  )
                ),
                Expanded(
                  child:Text(": "+birthday.toString(),
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 15.0,
                        color: AppColors.violet1
                    )
                  )
                )
              ],
            ),
            SizedBox(height: 3.0,),
            Row(
              children: <Widget>[
                Container(
                  width: 100,
                  child: Text("Mobile",
                    style: TextStyle(fontFamily: 'Schyler',
                        fontSize: 18.0,
                        color: AppColors.violet1,
                        fontWeight: FontWeight.bold
                    )
                  )
                ),
                Expanded(
                  child:Text(": "+mobile.toString(),
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 15.0,
                        color: AppColors.violet1
                    )
                  )
                )
              ],
            ),
            SizedBox(height: 3.0,),
            Row(
              children: <Widget>[
                Container(
                  width: 100,
                  child: Text("Email",
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 18.0,
                        color: AppColors.violet1,
                        fontWeight: FontWeight.bold
                    )
                  )
                ),
                Expanded(
                  child:Text(": "+email.toString(),
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 15.0,
                        color: AppColors.violet1
                    )
                  )
                )
              ],
            ),
            SizedBox(height: 3.0,),
            Row(
              children: <Widget>[
                Container(
                  width: 100,
                  child: Text("Province",
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 18.0,
                        color: AppColors.violet1,
                        fontWeight: FontWeight.bold
                    )
                  )
                ),
                Expanded(
                  child:Text(": "+province.toString(),
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 15.0,
                        color: AppColors.violet1
                    )
                  )
                )
              ],
            ),
            SizedBox(height: 3.0,),
            Row(
              children: <Widget>[
                Container(
                  width: 100,
                  child: Text("City",
                    style: TextStyle(fontFamily: 'Schyler',
                        fontSize: 18.0,
                        color: AppColors.violet1,
                        fontWeight: FontWeight.bold
                    )
                  )
                ),
                Expanded(
                  child:Text(": "+city.toString(),
                    style: TextStyle(
                        fontFamily: 'Schyler',
                        fontSize: 15.0,
                        color: AppColors.violet1
                    )
                  )
                )
              ],
            )
          ]
      ),
    );
  }

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds:500));
    setState(() {
    });
    _refreshController.refreshCompleted();
  }

  Future getCityList(int provinceId) async {
    String url = API_ENDPOINT+"/api/v1/dropdown/cities/"+provinceId.toString();
    Map<String, String> headers = {"Content-type": "application/json",
      "apikey":"b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w=="};
    final response = await get(url, headers: headers);
    var responses = Cities.fromJson(jsonDecode(response.body));
    for(int x=0; x<responses.cities.length; x++){
        try{
          setState(() {
          cityList[responses.cities[x].cityId] = responses.cities[x].cityName;
          });
        }catch(e){}
    }
  }

  clearData() async{
    await Insert.dbHelper.delete("tbtransaction");
    await Insert.dbHelper.delete("tbvoucher");
  }
}