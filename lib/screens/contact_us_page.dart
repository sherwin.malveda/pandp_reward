import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';

class ContactUsPage extends StatefulWidget {
  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {

@override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: AppColors.violet1,
        title: Text("CONTACT US",
          style: TextStyle(fontFamily: 'Schyler'),
        ),
        centerTitle: true,
      ),
      body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 100.0,),
                  Text("Plains & Prints Head Office",
                    style: TextStyle(
                      fontFamily: 'Schyler',
                      fontSize: 17.0,
                      fontWeight: FontWeight.bold,
                      color: AppColors.violet1
                    )
                  ),
                  SizedBox(height: 30),
                  Text("From Monday to Friday",
                    style: TextStyle(
                      fontFamily: 'Schyler',
                      fontSize: 15.0,
                      color: AppColors.violet1
                    )
                  ),
                  Text("10:00 a.m. to 05:00 p.m.",
                    style: TextStyle(
                      fontFamily: 'Schyler',
                      fontSize: 15.0,
                      color: AppColors.violet1
                    )
                  ),
                  Text("Telephone No.: 372-6190",
                    style: TextStyle(
                      fontFamily: 'Schyler',
                      fontSize: 15.0,
                      color: AppColors.violet1
                    )
                  ),
                  Text("Email: customersupport@plainsandprints.com",
                    style: TextStyle(
                      fontFamily: 'Schyler',
                      fontSize: 15.0,
                      color: AppColors.violet1
                    )
                  ),
                  SizedBox(height: 30.0),
                  Text("www.plainsandprints.com",
                    style: TextStyle(
                      fontFamily: 'Schyler',
                      fontSize: 15.0,
                      color: AppColors.violet1
                    )
                  ),
                ],
              )
            )
    );
  }
}