import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/offline_data_model/offlinepromo.dart';
import 'package:plainsandprintsloyalty/model/promos.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/screens/promo_fullscreen_page.dart';
import 'package:plainsandprintsloyalty/widgets/promo_list.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PromosPage extends StatefulWidget {
  @override
  _PromosPageState createState() => _PromosPageState();
}

class _PromosPageState extends State<PromosPage> {

  List<OfflinePromo> promooffline = new List<OfflinePromo>();
  StoreBloc _storeBloc;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  final dbHelper = DatabaseHelper.instance;

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    super.didChangeDependencies();
  }

  @override
  void initState(){
    super.initState();
    getPromotions();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("PROMOTIONS", style: TextStyle(fontFamily: 'Schyler'),),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: StreamBuilder(
        stream: _storeBloc.promos,
        builder: (BuildContext context, AsyncSnapshot<Promos> snapshot) {
          return StreamHandler(
            snapshot: snapshot,
            loading: Center(child: CircularProgressIndicator()),
            noData: Center(child: Text("No promos at the moment.",style: TextStyle(fontFamily: 'Schyler'))),
            withError: (error) => offline(),
            withData: (Promos promos) {
              saveOffline(promos);
              return SmartRefresher(
                enablePullDown: true,
                enablePullUp: true,
                header: ClassicHeader(
                  refreshingText: "Refreshing",
                  textStyle: TextStyle(fontFamily: 'Schyler'),
                ),
                footer: CustomFooter(
                  builder: (BuildContext context,LoadStatus mode){
                    Widget body ;
                    if(mode == LoadStatus.idle){
                      body =  Text("No more items", style: TextStyle(fontFamily: 'Schyler'));
                    }
                    else if(mode==LoadStatus.loading){
                      body =  CupertinoActivityIndicator();
                    }
                    else if(mode == LoadStatus.failed){
                      body = Text("Load Failed! Click retry!", style: TextStyle(fontFamily: 'Schyler'));
                    }
                    else if(mode == LoadStatus.canLoading){
                        body = Text("Release to load more", style: TextStyle(fontFamily: 'Schyler'));
                    }
                    else{
                      body = Text("No more items", style: TextStyle(fontFamily: 'Schyler'),);
                    }
                    return Container(
                      height: 55.0,
                      child: Center(child:body),
                    );
                  },
                ),
                controller: _refreshController,
                onRefresh:()=> _onRefresh(promos.promotions.length),
                onLoading:()=> _onLoading(promos.promotions.length),
                child: ListView.builder(
                  itemCount: promos.promotions.length,
                  itemBuilder: (BuildContext context, int index) {
                    return PromoList(
                        onPressed: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    PromoFullScreenPage(
                                      title: promos.promotions[index].promoName,
                                      content: promos.promotions[index].promoDescription,
                                      image: promos.promotions[index].promoImage,
                                      id: promos.promotions[index].promoId,
                                    )
                            )
                        ),
                        promo: promos.promotions[index].promoImage
                    );
                  }
                ),
              );
            },
          );
        }
       ),
    );
  }

  void _onRefresh(int currentCount) async{
    await Future.delayed(Duration(milliseconds: 1000));
    try{
      setState(() {
        _storeBloc.getPromos(ListRequest(
          rows: MAX_ROW_LIST,
          page: (currentCount ~/ MAX_ROW_LIST) + 1)
        );
      });
    }catch(e){}
    _refreshController.refreshCompleted();
  }

  void _onLoading(int currentCount) async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      _storeBloc.getPromos(ListRequest(
        rows: MAX_ROW_LIST,
        page: (currentCount ~/ MAX_ROW_LIST) + 1)
      );
    });
    _refreshController.loadComplete();
  }

  saveOffline(Promos promos) async {
    await Insert.dbHelper.delete("tbpromotion");
    for(int x=0; x<promos.promotions.length; x++){
      Insert.insertPromotions(promos.promotions[x].promoId.toString(),promos.promotions[x].promoName,
          promos.promotions[x].promoDescription, promos.promotions[x].promoImage);
    }
  }

  getPromotions() async {
    var result = await dbHelper.selectAllData("tbpromotion");
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    promooffline.clear();
    for(var items in imagelist){
      promooffline.add(new OfflinePromo(items["promoid"],items["title"], items["content"], items["image"]));
    }
  }

  offline(){
    return SmartRefresher(
      enablePullDown: true,
      header: ClassicHeader(
        refreshingText: "Refreshing",
        textStyle: TextStyle(fontFamily: 'Schyler'),
      ),
      controller: _refreshController,
      onRefresh: ()=> _onRefresh(0),
      child: promooffline.isEmpty
        ? Center(child: Text("No promos at the moment.",style: TextStyle(fontFamily: 'Schyler')))
        : ListView.builder(
          itemCount: promooffline.length,
          itemBuilder: (BuildContext context, int index) {
            return PromoList(
                onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            PromoFullScreenPage(
                              title: promooffline[index].promoName,
                              content: promooffline[index].promoDescription,
                              image: promooffline[index].promoImage,
                              id: int.parse(promooffline[index].promoId),
                            )
                    )
                ),
                promo: promooffline[index].promoImage
            );
          }
      ),
    );
  }
}