import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/model/banners.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/slider/dot_animation_enum.dart';
import 'package:plainsandprintsloyalty/slider/intro_slider.dart';
import 'package:plainsandprintsloyalty/slider/slide_object.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';

class GettingStarted extends StatefulWidget {
  @override
  GettingStartedState createState() => new GettingStartedState();
}

class GettingStartedState extends State<GettingStarted> {

  List<Slide> slides = new List();
  Function goToTab;
  StoreBloc _storeBloc;

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    _storeBloc.getBanner(ListRequest(rows: MAX_ROW_LIST, page: GETTING_STARTED_PAGE));
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    slides.add(new Slide(pathImage: "assets/images/pagebg.jpg"));
    slides.add(new Slide(pathImage: "assets/images/pagebg.jpg"));
    slides.add(new Slide(pathImage: "assets/images/pagebg.jpg"));
  }

  Widget renderDoneBtn() {
    return Icon(
      Icons.done,
      color: Colors.white,
    );
  }

  _setImage(String image) {
    if (image == null || image == "") {
      return new Image.asset(
        "assets/images/pagebg.jpg",
        height: MediaQuery.of(context).size.height * 1.0,
        width: MediaQuery.of(context).size.height * 1.0,
        fit: BoxFit.fitWidth,
      );
    } else {
      return CachedNetworkImage(
        imageUrl: image,
          height: MediaQuery.of(context).size.height * 1.0,
          width: MediaQuery.of(context).size.height * 1.0,
          fit: BoxFit.fitWidth,
        placeholder: (context, url) => new Center (
          child: CircularProgressIndicator(backgroundColor: AppColors.violet1)
        ),
        errorWidget: (context, url, error) => new Container(
          child:Center(
            child:  Text("Failed to load the image.\n\nPlease check your internet connection and try again!", 
              style: TextStyle(fontFamily: 'Schyler', fontSize: 20.0)
            )
          ),
        )
      );
    }
  }

  List<Widget> renderListCustomTabs(Banners banner) {
    List<Widget> tabs = new List();
    for (int i = 0; i < slides.length; i++) {
      tabs.add(
        Column(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  GestureDetector(child: _setImage(banner.banners[i].imageUrl)),
                ],
              ),
            ),
          ],
        ),
      );
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<Banners>(
        stream: _storeBloc.banner,
        builder: (context, AsyncSnapshot<Banners> snapshot) {
          return StreamHandler<Banners>(
            snapshot: snapshot,
            withData: (Banners banner) {
              return new Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/pagebg.jpg"),
                    fit: BoxFit.fitWidth,
                  )
                ),
                child: IntroSlider(
                  slides: this.slides,
                  renderDoneBtn: this.renderDoneBtn(),
                  onDonePress: this.onDonePress,
                  colorDoneBtn: AppColors.violet1,
                  colorDot: AppColors.violet1,
                  sizeDot: 10.0,
                  typeDotAnimation: dotSliderAnimation.SIZE_TRANSITION,
                  listCustomTabs: this.renderListCustomTabs(banner),
                  backgroundColorAllSlides: Colors.white,
                  refFuncGoToTab: (refFunc) {
                    this.goToTab = refFunc;
                  },
                )
              );
            },
            loading: Container(
              child: Center(child: CircularProgressIndicator()),
            ),
            noData: Center(
              child: Text("Image not sucessfully Loaded.\n\nPlease check your internet connection \nand try again!", textAlign: TextAlign.center),
            ),
            withError: (Object error) {
              return Center(
                child: Text("Image not sucessfully Loaded.\n\nPlease check your internet connection \nand try again!", textAlign: TextAlign.center),
              );
            }
          );
        }
      ),
    );
  }

  void onDonePress() {
    Navigator.pushReplacementNamed(context, Routes.LANDING);
  }

}