import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/offline_data_model/background.dart';
import 'package:plainsandprintsloyalty/model/banners.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LandingPage extends StatefulWidget {
  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage>
    with AfterLayoutMixin<LandingPage> {
  
  StoreBloc _storeBloc;
  List<ImageBackground> bannerImage = new List<ImageBackground>();
  final dbHelper = DatabaseHelper.instance;

  @override
  void initState() {
    super.initState();
    getBackground();
    setData();
  }

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    super.didChangeDependencies();
  }

  _carouselImage(String image) {
    if (image.length == 0) {
      return AssetImage('assets/images/pagebg.jpg',);
    }
    else {
      return CachedNetworkImage(
        imageUrl: image,
          fit: BoxFit.fitWidth,
        placeholder: (context, url) => new Center (
          child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)
        ),
        errorWidget: (context, url, error) => new Container(
          child:Center(
            child:  Text("Failed to load the image.\n\nPlease check your internet connection and try again!",
              style: TextStyle(fontFamily: 'Schyler', fontSize: 12.0), textAlign: TextAlign.center
            )
          ),
        )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Container(
          width: MediaQuery.of(context).size.width * 1.0,
          height: MediaQuery.of(context).size.height * 1.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 1.0 - 56,
                child: StreamBuilder<Banners>(
                  stream: _storeBloc.landing,
                  builder: (context, AsyncSnapshot<Banners> snapshot) {
                    return StreamHandler<Banners>(
                      snapshot: snapshot,
                      withData: (Banners banners) {
                        saveOffline(banners);
                        return Carousel(
                          dotColor: AppColors.violet1,
                          indicatorBgPadding: MediaQuery.of(context).size.height * .15,
                          dotBgColor: Colors.transparent,
                          dotIncreasedColor: AppColors.violet1,
                          dotSize: 5.0,
                          dotSpacing: 20.0,
                          autoplay: true,
                          autoplayDuration: Duration(seconds: 5),
                          boxFit: BoxFit.fitWidth,
                          images: banners.banners
                          .take(banners.banners.length > 5
                          ? 5
                          : banners.banners.length)
                          .map((banner) =>
                            _carouselImage(banner.imageUrl)
                          ).toList(),
                        );
                      },
                      loading: Container(
                        child: Center(child: CircularProgressIndicator()),
                      ),
                      noData: bannerImage.isEmpty
                      ?  Image.asset("assets/images/pagebg.jpg", fit: BoxFit.fitWidth,)
                      : Carousel(
                        dotColor: AppColors.violet1,
                        indicatorBgPadding: 8.0,
                        dotBgColor: Colors.transparent,
                        dotIncreasedColor: AppColors.violet1,
                        dotSize: 5.0,
                        dotSpacing: 20.0,
                        autoplay: true,
                        autoplayDuration: Duration(seconds: 5),
                        boxFit: BoxFit.fitWidth,
                        images: bannerImage
                          .take(bannerImage.length > 5
                            ? 5
                            : bannerImage.length)
                          .map((bannerImage) =>_carouselImage(bannerImage.imageUrl)).toList(),
                      ),
                      withError: (Object error) => bannerImage.isEmpty
                      ? Image.asset("assets/images/pagebg.jpg", fit: BoxFit.fitWidth,)
                      : Carousel(
                          dotColor: AppColors.violet1,
                          indicatorBgPadding: 8.0,
                          dotBgColor: Colors.transparent,
                          dotIncreasedColor: AppColors.violet1,
                          dotSize: 5.0,
                          dotSpacing: 20.0,
                          autoplay: true,
                          autoplayDuration: Duration(seconds: 5),
                          boxFit: BoxFit.fitWidth,
                          images: bannerImage
                            .take(bannerImage.length > 5
                              ? 5
                              : bannerImage.length)
                            .map((bannerImage) =>_carouselImage(bannerImage.imageUrl)).toList(),
                        )
                    );
                  }
                )
              ),
            ],
          ),
          ),
          Positioned(
                bottom: 0,
                child: Container(
                color: Colors.white,
                height: 140,
                width: MediaQuery.of(context).size.width *1.0,
                padding: EdgeInsets.all(0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(height: 10.0,),
                    Container(
                      height: 50,
                        child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 0.0),
                            child: Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(5.0),
                              color: AppColors.violet1,
                              child: MaterialButton(
                                  child: Text("CREATE ACCOUNT",
                                      style: TextStyle(
                                          fontFamily: 'Schyler',
                                          color: Colors.white,
                                          fontSize: 20.0,
                                          letterSpacing: 3.0
                                      ),
                                      textAlign: TextAlign.center
                                  ),
                                  onPressed: () =>_navigateToRegistrationPage(context)
                              ),
                            )
                        )
                    ),
                    SizedBox(height: 12),
                    Text("Already have an account?",
                        style: TextStyle(fontFamily: 'Schyler', color: AppColors.violet2),
                        textAlign: TextAlign.center
                    ),
                    FlatButton(
                        child: Text("LOG IN",
                            style: TextStyle(
                                fontFamily: 'Schyler',
                                color: AppColors.violet2,
                                fontSize: 15.0
                            )
                        ),
                        onPressed: () => _navigateToLoginPage(context)
                    ),
                  ],
                ),
              )
              )
        ],
      )
    );
  }

  void _navigateToRegistrationPage(BuildContext context) {
    Navigator.pushNamed(context, Routes.REGISTRATION_QUESTION);
  }

  void _navigateToLoginPage(BuildContext context) {
    Navigator.pushNamed(context, Routes.LOGIN);
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    _storeBloc.getBanner2(ListRequest(rows: MAX_ROW_LIST, page: LANDING_PAGE));
  }

  saveOffline(Banners banners) async {
    await Insert.dbHelper.deleteImage("tbbanner","3");
    for(int x=0; x<banners.banners.length; x++){
      Insert.insertImage("tbbanner", "3", banners.banners[x].imageUrl);
    }
  }

  getBackground() async {
    bannerImage.clear();
    var result = await dbHelper.selectImage("tbbanner" ,"3");
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    for(var items in imagelist){
      setState(() {
        bannerImage.add(new ImageBackground(int.parse(items["page"]), items["image"]));
      });
    }
  }

  setData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setDouble('points', 0.0);
    await prefs.setString('username', "Sir/Ma'am");
    await prefs.setString('name', "John Doe");
    await prefs.setString('birthday', "Jan 01,1995");
    await prefs.setString('mobile', "09999999999");
    await prefs.setString('email', "johdoe@email.com");
    await prefs.setString('province', "Manila");
    await prefs.setString('city', "Manila City");
    await prefs.setString('cardType', "");
  }
  
}