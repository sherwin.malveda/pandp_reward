import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/model/promo_details.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:html/dom.dart' as dom;
import 'package:url_launcher/url_launcher.dart';

class PromoFullScreenPage extends StatefulWidget {
  final String title, content, image;
  final int id;

  const PromoFullScreenPage({Key key, @required this.title, @required this.content, @required this.image,  @required this.id})
      : super(key: key);
  _PromoFullScreenPageState createState() => _PromoFullScreenPageState();
}

class _PromoFullScreenPageState extends State<PromoFullScreenPage> {

  StoreBloc _storeBloc;

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    _storeBloc.getPromoDetails(widget.id);
    super.didChangeDependencies();
  }

  // Set the background
  _bgimage(String image) {
    if (image.length > 0) {
      return CachedNetworkImage(
          imageUrl: image,
          fit: BoxFit.fitWidth,
          height: MediaQuery.of(context).size.height * 1.0,
          width: MediaQuery.of(context).size.width * 1.0,
          placeholder: (context, url) => new Center (
              child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)
          ),
          errorWidget: (context, url, error) => new Container(
            child:Center(
                child:  Text("Failed to load the image.\n\nPlease check your internet connection and try again!",
                    style: TextStyle(fontFamily: 'Schyler', fontSize: 15.0), textAlign: TextAlign.center,
                )
            ),
          )
      );
    }
    else {
      return new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: AssetImage('assets/images/pagebg.jpg'),
            fit: BoxFit.fitWidth,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.violet1,
          title: Text("PROMOTIONS", style: TextStyle(fontFamily: 'Schyler'),),
          centerTitle: true,
          automaticallyImplyLeading: true,
        ),
        body: StreamBuilder<PromoDetails>(
            stream: _storeBloc.promoDetails,
            builder: (context, snapshot) {
              return StreamHandler(
                  snapshot: snapshot,
                  loading: Center(child: CircularProgressIndicator()),
                  withError: (error) => offline(),
                  noData: offline(),
                  withData: (PromoDetails promoDetails) {
                    return Stack(
                      children: <Widget>[
                        _bgimage("${promoDetails.promoImage}"),
                        Positioned(
                          bottom: 0.0,
                          left: 0.0,
                          right: 0.0,
                          child: Center(
                            child: Container(
                                width: MediaQuery.of(context).size.height * 1.0,
                                height: MediaQuery.of(context).size.height * .55,
                                child: Card(
                                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                                  color: Colors.white54,
                                  child: Column(
                                    children: <Widget>[
                                      SizedBox(height: 10.0),
                                      Padding(
                                        padding: EdgeInsets.all(0),
                                        child: Text("${widget.title}",
                                          style: TextStyle(
                                              fontFamily: 'Schyler',
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.violet2,
                                              fontSize: 20.0
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      Expanded(
                                          child: ListView(
                                              children: <Widget>[
                                                Padding(
                                                  padding: EdgeInsets.all(10.0),
                                                  child: Html(
                                                    data: ""+widget.content+"",
                                                    defaultTextStyle: TextStyle(
                                                      color: AppColors.violet1,
                                                      fontSize: 13,
                                                      height: 1.5,
                                                      fontWeight: FontWeight.bold
                                                    ),
                                                    linkStyle: const TextStyle(
                                                      decoration: TextDecoration.underline,
                                                    ),
                                                    onLinkTap: (url) async{
                                                      if (await canLaunch(url)) {
                                                        await launch(url);
                                                      } else {
                                                        throw 'Could not launch $url';
                                                      }
                                                    },
                                                    onImageTap: (src) {
                                                      print(src);
                                                    },
                                                    customRender: (node, children) {
                                                      if (node is dom.Element) {
                                                        switch (node.localName) {
                                                          case "custom_tag":
                                                            return Column(children: children);
                                                        }
                                                      }
                                                      return null;
                                                    },
                                                    customTextAlign: (dom.Node node) {
                                                      if (node is dom.Element) {
                                                        switch (node.localName) {
                                                          case "p":
                                                            return TextAlign.justify;
                                                        }
                                                      }
                                                      return null;
                                                    },
                                                    customTextStyle: (dom.Node node, TextStyle baseStyle) {
                                                      if (node is dom.Element) {
                                                        switch (node.localName) {
                                                          case "p":
                                                            return baseStyle.merge(TextStyle(color: AppColors.violet1, height: 1.5, fontSize: 13, fontWeight: FontWeight.bold));
                                                          case "h":
                                                            return baseStyle.merge(TextStyle(color: AppColors.violet1, height: 1.5, fontSize: 13, fontWeight: FontWeight.bold));
                                                        }
                                                      }
                                                      return baseStyle;
                                                    },
                                                  ),
                                                )
                                              ]
                                          )
                                      ),
                                      SizedBox(height: 10),
                                    ],
                                  ),
                                )
                            ),
                          ),
                        )
                      ],
                    );
                  }
              );
            }
        )
    );
  }

  offline(){
    return Stack(
      children: <Widget>[
        Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: AssetImage('assets/images/pagebg.jpg'),
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        Positioned(
          bottom: 0.0,
          left: 0.0,
          right: 0.0,
          child: Center(
            child: Container(
                width: MediaQuery.of(context).size.height * 1.0,
                height: MediaQuery.of(context).size.height * .48,
                child: Card(
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                  color: Colors.white54,
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10.0),
                      Padding(
                        padding: EdgeInsets.all(0),
                        child: Text("${widget.title}",
                          style: TextStyle(
                              fontFamily: 'Schyler',
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 20.0
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Expanded(
                          child: ListView(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: Html(
                                    data: ""+widget.content+"",
                                    defaultTextStyle: TextStyle(
                                      color: Colors.black,
                                      fontSize: 13,
                                      height: 1.5,
                                      fontWeight: FontWeight.bold
                                    ),
                                    linkStyle: const TextStyle(
                                      decoration: TextDecoration.underline,
                                    ),
                                    onLinkTap: (url) async{
                                      if (await canLaunch(url)) {
                                        await launch(url);
                                      } else {
                                        throw 'Could not launch $url';
                                      }
                                    },
                                    onImageTap: (src) {
                                      print(src);
                                    },
                                    customRender: (node, children) {
                                      if (node is dom.Element) {
                                        switch (node.localName) {
                                          case "custom_tag":
                                            return Column(children: children);
                                        }
                                      }
                                      return null;
                                    },
                                    customTextAlign: (dom.Node node) {
                                      if (node is dom.Element) {
                                        switch (node.localName) {
                                          case "p":
                                            return TextAlign.justify;
                                        }
                                      }
                                      return null;
                                    },
                                    customTextStyle: (dom.Node node, TextStyle baseStyle) {
                                      if (node is dom.Element) {
                                        switch (node.localName) {
                                          case "p":
                                            return baseStyle.merge(TextStyle(color: AppColors.violet1, height: 1.5, fontSize: 13, fontWeight: FontWeight.bold));
                                          case "h":
                                            return baseStyle.merge(TextStyle(color: AppColors.violet1, height: 1.5, fontSize: 13, fontWeight: FontWeight.bold));
                                        }
                                      }
                                      return baseStyle;
                                    },
                                  ),
                                )
                              ]
                          )
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                )
            ),
          ),
        )
      ],
    );
  }
}
