import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/offline_data_model/voucheroffline.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/model/voucher.dart';
import 'package:plainsandprintsloyalty/model/vouchers.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/arguments/voucher_details_arguments.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:plainsandprintsloyalty/widgets/voucher_list.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class VouchersPage extends StatefulWidget {
  @override
  _VouchersPageState createState() => _VouchersPageState();
}

class _VouchersPageState extends State<VouchersPage> {

  CardBloc _cardBloc;
  AuthBloc _authBloc;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  final dbHelper = DatabaseHelper.instance;
  List<VoucherOffline> voucheroffline = new List<VoucherOffline>();

  @override
  void didChangeDependencies() {
    _cardBloc = MasterProvider.card(context);
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  void initState(){
    super.initState();
    getVouchers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("VOUCHER", style: TextStyle(fontFamily: 'Schyler'),),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: StreamBuilder(
          stream: _cardBloc.vouchers,
          builder: (BuildContext context, AsyncSnapshot<Vouchers> snapshot) {
            return StreamHandler(
              snapshot: snapshot,
              withError: (error) => offline(),
              loading: Center(child: CircularProgressIndicator()),
              noData: SmartRefresher(
                enablePullDown: true,
                header: ClassicHeader(
                  refreshingText: "Refreshing",
                  textStyle: TextStyle(fontFamily: 'Schyler'),
                ),
                controller: _refreshController,
                onRefresh:()=> _onRefresh(0),
                child: Center(child: Text("No vouchers at the moment.",style: TextStyle(fontFamily: 'Schyler')))
              ),
              withData: (Vouchers vouchers) {
                saveOffline(vouchers);
                return Column(
                  children: <Widget>[
                    Expanded(
                      child:SmartRefresher(
                        enablePullDown: true,
                        header: ClassicHeader(
                          refreshingText: "Refreshing",
                          textStyle: TextStyle(fontFamily: 'Schyler'),
                        ),
                        controller: _refreshController,
                        onRefresh:()=> _onRefresh(vouchers.vouchers.length),
                        child: ListView.builder(
                          itemCount: vouchers.vouchers.length,
                          itemBuilder: (BuildContext context, int index) {
                            final Voucher voucher = vouchers.vouchers[index];
                            return VoucherList(
                              voucher: voucher.description,
                              onTap: () {
                                _navigateToVoucherDetails(voucher.voucherCode, voucher.status,
                                voucher.description, voucher.expiration.toString());
                              } 
                            );
                          }
                        ),
                      )
                    )
                  ],
                );
              },
            );
          }
        ),
      ),
    );
  }

  void _onRefresh(int currentCount) async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      try{
        _cardBloc.getVouchers(ListRequest(
            accountId: _authBloc.session.value.accountId,
            rows: MAX_ROW_LIST,
            page: (currentCount ~/ MAX_ROW_LIST) + 1)
        );
      }catch(Exception){}
    });
    _refreshController.refreshCompleted();
  }

  void _navigateToVoucherDetails(String code, String status, String description, String expiration) {
    final VoucherDetailsArguments args = VoucherDetailsArguments(code, description, status,expiration);
    Navigator.of(context).pushNamed(Routes.VOUCHER_DETAILS, arguments: args);
  }

  saveOffline(Vouchers vouchers) async{
    await Insert.dbHelper.delete("tbvoucher");
    for(int x=0; x<vouchers.vouchers.length; x++){
      Insert.insertVoucher(vouchers.vouchers[x].voucherCode,vouchers.vouchers[x].status,
          vouchers.vouchers[x].description, vouchers.vouchers[x].expiration.toString());
    }
  }

  getVouchers() async {
    var result = await dbHelper.selectAllData("tbvoucher");
    List<Map<String, dynamic>> voucherList = result;
    final voucherlist = voucherList;
    voucheroffline.clear();
    for(var items in voucherlist){
      voucheroffline.add(new VoucherOffline(items["code"], items["description"], items["status"], items["expiration"]));
    }
  }

  offline(){
    return Column(
      children: <Widget>[
        Expanded(
            child:SmartRefresher(
              enablePullDown: true,
              header: ClassicHeader(
                refreshingText: "Refreshing",
                textStyle: TextStyle(fontFamily: 'Schyler'),
              ),
              controller: _refreshController,
              onRefresh:()=> _onRefresh(voucheroffline.length),
              child: voucheroffline.isEmpty
                ? Center(child: Text("No vouchers at the moment.",style: TextStyle(fontFamily: 'Schyler')))
                : ListView.builder(
                  itemCount: voucheroffline.length,
                  itemBuilder: (BuildContext context, int index) {
                    final VoucherOffline voucher = voucheroffline[index];
                    return VoucherList(
                      voucher: voucher.description,
                      onTap: () => _navigateToVoucherDetails(voucher.voucherCode, voucher.status,
                          voucher.description, voucher.expiration.toString()),
                    );
                  }
              ),
            )
        )
      ],
    );
  }
}
