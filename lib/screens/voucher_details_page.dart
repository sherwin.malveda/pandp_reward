import 'dart:async';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/request/cancel_voucher_request.dart';
import 'package:plainsandprintsloyalty/screens/arguments/voucher_details_arguments.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';
import 'package:plainsandprintsloyalty/widgets/voucher_row.dart';
import 'package:screen/screen.dart';

class VoucherDetailsPage extends StatefulWidget {
  @override
  _VoucherDetailsPageState createState() => _VoucherDetailsPageState();
}

class _VoucherDetailsPageState extends State<VoucherDetailsPage>{

  AuthBloc _authBloc;
  CardBloc _cardBloc;
  bool isKeptOn = false;
  double brightness = 1.0;

  StreamSubscription _cancelStateSubscription;

  @override
  void dispose() {
    _cancelStateSubscription.cancel();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _cardBloc = MasterProvider.card(context);
    _cancelStateSubscription = _cardBloc.cancelVoucherState.listen(_cancelStateListener);
    super.didChangeDependencies();
  }

  @override
  void initState(){
    super.initState();
    initPlatformState();
  }

  initPlatformState() async {
    bool keptOn = await Screen.isKeptOn;
    brightness = await Screen.brightness;
    setState((){
      isKeptOn = keptOn;
      Screen.setBrightness(1.0);
    });
  }

  @override
  Widget build(BuildContext context) {
    final VoucherDetailsArguments args = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("VOUCHER", style: TextStyle(fontFamily: 'Schyler'),),
        centerTitle: true,
        leading: IconButton(icon:Icon(Icons.arrow_back),
          onPressed:() {
             Screen.setBrightness(brightness);
             Navigator.pop(context, false);          
          }
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 50.0,),
            Container(
              margin: EdgeInsets.symmetric(horizontal:20.0),
              child: VoucherRow(
                  code: args.vouchercode,
                  status: args.status,
                  description: args.description,
                  expiration: args.expiration,
                  onTap: null),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal:22.0),
              child: Text("*Present the voucher to the cashier upon payment.",
                style: TextStyle(
                  fontSize: 12,
                  fontFamily: 'Schyler',
                  color: AppColors.violet2
                ),
              ),
            ),
            SizedBox(height: 15.0),
            FlatButton(
              child: Text("Delete Voucher",
                style: TextStyle(fontFamily: 'Schyler',color: Colors.red),
              ),
              onPressed: () {
                _showConfirmDialog(args.vouchercode);
                clearVoucher();
              },
            )
          ],
        )
      ),
    );
  }

  void _showConfirmDialog(String voucherCode) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Voucher " + voucherCode),
          content: Text("Are you sure you want to delete this voucher?"),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Continue"),
              onPressed: () {
                Screen.setBrightness(brightness);
                Navigator.of(context).pop();
                _cancelVoucher(voucherCode);
              },
            ),
          ],
        );
      },
    );
  }

  void _cancelStateListener(ScreenState state) {
    switch (state.state) {
      case States.IDLE:
        break;
      case States.WAITING:
        break;
      case States.DONE:
        Navigator.of(context).pop();
        break;
      case States.ERROR:
        _showError("${state.error.toString()}");
        break;
    }
  }

  void _showError(String errorMessage) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "$errorMessage",
        style: TextStyle(
            fontSize: 15.0,
            color: Colors.white,
            fontFamily: "Schyler"),
      ),
      isDismissible: true,
      duration:  Duration(seconds: 5),
      mainButton: FlatButton(
        onPressed: () {
          Navigator.pop(context);
          Navigator.pop(context);
        },
        child: Text(
          "OK",
          style: TextStyle(
              fontFamily: 'Schyler',
              fontSize: 15,
              color: Colors.white,
              fontWeight: FontWeight.bold),
        ),
      ),
    )..show(context);
  }

  void _cancelVoucher(String voucherCode) {
    final CancelVoucherRequest request = CancelVoucherRequest(
        accountId: _authBloc.session.value.accountId,
        cardPan: _authBloc.session.value.cardPan,
        voucherCode: voucherCode
    );
    _cardBloc.cancelVoucher(request);
    clearVoucher();
  }

  clearVoucher() async{
    await Insert.dbHelper.delete("tbvoucher");
  }
}