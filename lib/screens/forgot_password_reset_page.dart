import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/form_validators.dart';
import 'package:plainsandprintsloyalty/model/request/reset_password_request.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';
import 'arguments/success_arguments.dart';

class ForgotPasswordResetPage extends StatefulWidget {
  @override
  _ForgotPasswordResetPageState createState() =>
      _ForgotPasswordResetPageState();
}

class _ForgotPasswordResetPageState extends State<ForgotPasswordResetPage> {

  final dbHelper = DatabaseHelper.instance;
  String image = "";
  final TextEditingController _code = TextEditingController();
  final TextEditingController _newPassword = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();
  final FocusNode codeFocusNode = new FocusNode();
  final FocusNode newpassFocusNode = new FocusNode();
  final FocusNode confirmnewpassFocusNode = new FocusNode();

  StreamSubscription _stateSubs;

  AuthBloc _authBloc;

  String newpasswordstrength = "", newconfirmstrength = "";

  @override
  void initState(){
    super.initState();
    getBackground();
  }

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _stateSubs = _authBloc.resetPasswordState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _stateSubs.cancel();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          CachedNetworkImage(
              imageUrl: image,
              height: MediaQuery.of(context).size.height * 1.0,
              width: MediaQuery.of(context).size.height * 1.0,
              fit: BoxFit.fitWidth,
              placeholder: (context, url) => new Center (
                  child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)
              ),
              errorWidget: (context, url, error) => new Container(
                  child: Image.asset('assets/images/pagebg.jpg',
                    height: MediaQuery.of(context).size.height * 1.0,
                    width: MediaQuery.of(context).size.height * 1.0,
                    fit: BoxFit.fitWidth,
                  )
              )
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
                child: Center(
                    child: Container(
                        width: 280, height: 320,
                        decoration: new BoxDecoration(
                            color: Colors.white70,
                            borderRadius: BorderRadius.circular(5.0)
                        ),
                        child: ListView(
                          children: <Widget>[
                            Container(
                              child: Text("Reset Password",
                                  style: TextStyle(
                                      fontFamily: 'Schyler',
                                      color: AppColors.violet1,
                                      fontSize: 25.0,
                                      fontWeight: FontWeight.w500
                                  ),
                                  textAlign: TextAlign.center
                              ),
                            ),
                            Container(
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 16.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                  children: <Widget>[
                                    SizedBox(height: 16.0),
                                    Text("* A code has been sent to your email.",
                                        style: TextStyle(fontFamily: 'Schyler', fontSize: 12)
                                    ),
                                    SizedBox(height: 7),
                                    Container(
                                        height: 40.0,
                                        decoration: new BoxDecoration(
                                            color: Colors.white70,
                                            borderRadius: BorderRadius.circular(10.0)
                                        ),
                                        child: TextFormField(
                                          focusNode: codeFocusNode,
                                          controller: _code,
                                          style: TextStyle(fontFamily: 'Schyler', color: Colors.black),
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.fromLTRB(15, 10, 10, 12),
                                            hintText: "Code",
                                            hintStyle: TextStyle(
                                                color: AppColors.violet1,
                                                fontSize: 18.0,
                                                fontFamily: 'Schyler'
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: AppColors.violet1, width: 1.0),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(color: AppColors.violet1, width: 1.0),
                                            ),
                                          ),
                                          onFieldSubmitted: (value){
                                            FocusScope.of(context).requestFocus(newpassFocusNode);
                                          },
                                        )
                                    ),
                                    SizedBox(height: 8.0),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                            child: Container(
                                                height: 40.0,
                                                decoration: new BoxDecoration(
                                                    color: Colors.white70,
                                                    borderRadius: BorderRadius.circular(10.0)
                                                ),
                                                child: TextField(
                                                  focusNode: newpassFocusNode,
                                                  obscureText: true,
                                                  controller: _newPassword,
                                                  style: TextStyle(fontFamily: 'Schyler', color: Colors.black),
                                                  decoration: InputDecoration(
                                                    contentPadding: EdgeInsets.fromLTRB(15, 10, 10, 12),
                                                    hintText: "New Password",
                                                    hintStyle: TextStyle(
                                                        color: AppColors.violet1,
                                                        fontSize: 18.0,
                                                        fontFamily: 'Schyler'
                                                    ),
                                                    focusedBorder: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: AppColors.violet1, width: 1.0),
                                                    ),
                                                    enabledBorder: OutlineInputBorder(
                                                      borderSide: BorderSide(
                                                          color: AppColors.violet1, width: 1.0),
                                                    ),
                                                  ),
                                                  onChanged: (value){
                                                    setState(() {
                                                      newpasswordstrength = FormValidators.passwordStrength(_newPassword.text);
                                                    });
                                                  },
                                                  onSubmitted: (value){
                                                    FocusScope.of(context).requestFocus(confirmnewpassFocusNode);
                                                  },
                                                )
                                            )
                                        ),
                                        Text(newpasswordstrength,
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontFamily: 'Schyler',
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.violet1
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 8.0),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                            child: Container(
                                                height: 40.0,
                                                decoration: new BoxDecoration(
                                                    color: Colors.white70,
                                                    borderRadius: BorderRadius.circular(10.0)
                                                ),
                                                child: TextField(
                                                  focusNode: confirmnewpassFocusNode,
                                                  obscureText: true,
                                                  controller: _confirmPassword,
                                                  style: TextStyle(fontFamily: 'Schyler', color: Colors.black),
                                                  decoration: InputDecoration(
                                                    contentPadding: EdgeInsets.fromLTRB(15, 10, 10, 12),
                                                    hintText: "Confirm Password",
                                                    hintStyle: TextStyle(
                                                        color: AppColors.violet1,
                                                        fontSize: 18.0,
                                                        fontFamily: 'Schyler'
                                                    ),
                                                    focusedBorder: OutlineInputBorder(
                                                      borderSide: BorderSide(color: AppColors.violet1, width: 1.0),
                                                    ),
                                                    enabledBorder: OutlineInputBorder(
                                                      borderSide: BorderSide(color: AppColors.violet1, width: 1.0),
                                                    ),
                                                  ),
                                                  onChanged: (value){
                                                    setState(() {
                                                      newconfirmstrength = FormValidators.passwordStrength(_confirmPassword.text);
                                                    });
                                                  },
                                                )
                                            )
                                        ),
                                        Text(newconfirmstrength,
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              fontFamily: 'Schyler',
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.violet1
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 24.0),
                                    StreamBuilder<ScreenState>(
                                        stream: _authBloc.resetPasswordState,
                                        builder: (context, snapshot) => _buildResetPasswordButton(snapshot)
                                    ),
                                    SizedBox(height: 20.0),
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                    )
                )
            ),
          ),
          Positioned(
              top: 20.0,
              left: 0.0,
              child: IconButton(
                icon: Icon(Icons.arrow_back, color: AppColors.violet1,size: 23,),
                onPressed: () {Navigator.pop(context);},
              )
          )
        ],
      )
    );
  }

  Widget _buildResetPasswordButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        child: Center(child: CircularProgressIndicator()),
      );
    } else {
      return Container(
        height: 40.0,
        width: 200.0,
        margin: EdgeInsets.symmetric(horizontal: 20.0),
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(5.0),
          color: AppColors.violet1,
          child: MaterialButton(
            child: Text("RESET PASSWORD",
              style: TextStyle(
                fontFamily: 'Schyler',
                color: Colors.white,
                fontSize: 16.0
              )
            ),
            onPressed: _resetPassword,
          ),
        )
      );
    }
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        _navigateToPasswordSuccessPage();
        break;
      case States.ERROR:
        _showError(state.error.toString());
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void _showError(String errorMessage) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "$errorMessage",
        style: TextStyle(
          fontSize: 15.0, 
          color: Colors.white,
          fontFamily: "Schyler"),
      ),
      duration:  Duration(seconds: 3),              
    )..show(context);
  }

  void _resetPassword() {
    final String code = _code.text;
    final String newPassword = _newPassword.text;
    final String confirmPassword = _confirmPassword.text;

    if (code.isEmpty || newPassword.isEmpty || confirmPassword.isEmpty) {
      _showError("Fill all the fields");
      return;
    }

    if(newpasswordstrength == "\t\t\t Very Weak \t\t\t" || newpasswordstrength == "\t\t\t Weak \t\t\t"){
      _showError("Invalid Password. Please make sure your password is not weak");
      return;
    }

    if(_confirmPassword.text != _newPassword.text){
      _showError("Confirm password does not match");
      return;
    }

    final ResetPasswordRequest request = ResetPasswordRequest(
      code: code, newPassword: newPassword, confirmPassword: confirmPassword
    );
    _authBloc.resetPassword(request);
  }

  void _navigateToPasswordSuccessPage() {
    final SuccessArguments arguments = SuccessArguments(
      Text("Password successfully reset", style: TextStyle(fontSize: 24)),
      Text("GO TO LOGIN",style: TextStyle(color: Colors.white),),
      Routes.LOGIN
    );

    Navigator.of(context).pushNamedAndRemoveUntil(
      Routes.SUCCESS_PAGE, ModalRoute.withName(Routes.LANDING),arguments: arguments
    );
  }

  getBackground() async {
    var result = await dbHelper.selectImage("tbbackground" ,FORGOT_PASSWORD_BACKGROUND.toString());
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    for(var items in imagelist){
      setState(() {
        image = items["image"];
      });
    }
  }
}