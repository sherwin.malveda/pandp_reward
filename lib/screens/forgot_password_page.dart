import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/model/request/forgot_password_password.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  final dbHelper = DatabaseHelper.instance;
  String image = "";
  final TextEditingController _emailController = TextEditingController();
  StreamSubscription stateSubs;
  AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
    getBackground();
  }

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    stateSubs = _authBloc.forgotPasswordState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    stateSubs.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
          children: <Widget>[
            CachedNetworkImage(
                imageUrl: image,
                height: MediaQuery.of(context).size.height * 1.0,
                width: MediaQuery.of(context).size.height * 1.0,
                fit: BoxFit.fitWidth,
                placeholder: (context, url) => new Center (
                    child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)
                ),
                errorWidget: (context, url, error) => new Container(
                    child: Image.asset('assets/images/pagebg.jpg',
                      height: MediaQuery.of(context).size.height * 1.0,
                      width: MediaQuery.of(context).size.height * 1.0,
                      fit: BoxFit.fitWidth,
                    )
                )
            ),
            Positioned(
              top: 0.0,
              bottom: 0.0,
              left: 0.0,
              right: 0.0,
              child: Container(
                  child: Center(
                      child: Container(
                          width: 280,
                          height: 320,
                          decoration: new BoxDecoration(
                              color: Colors.white70,
                              borderRadius: BorderRadius.circular(5.0)
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text("RESET\nPASSWORD",
                                    style: TextStyle(
                                        fontFamily: 'Schyler',
                                        color: AppColors.violet1,
                                        fontSize: 25.0,
                                        fontWeight: FontWeight.w500
                                    ),
                                    textAlign: TextAlign.center
                                ),
                                SizedBox(height: 20.0,),
                                Container(
                                    height: 40.0,
                                    margin: EdgeInsets.symmetric(horizontal: 20.0),
                                    decoration: new BoxDecoration(
                                        color: Colors.white70,
                                        borderRadius: BorderRadius.circular(10.0)
                                    ),
                                    child: TextFormField(
                                        controller: _emailController,
                                        style: TextStyle(fontFamily: 'Schyler', color: Colors.black),
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.fromLTRB(15, 10, 10, 12),
                                          hintText: "Email",
                                          hintStyle: TextStyle(
                                              color: AppColors.violet1,
                                              fontSize: 18.0,
                                              fontFamily: 'Schyler'
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: AppColors.violet1, width: 1.0),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: AppColors.violet1, width: 1.0),
                                          ),
                                        )
                                    )
                                ),
                                SizedBox(height: 30.0,),
                                StreamBuilder<ScreenState>(
                                    stream: _authBloc.forgotPasswordState,
                                    builder: (context, snapshot) => _buildResetButton(snapshot)
                                )
                              ]
                          )
                      )
                  )
              ),
            ),
            Positioned(
                top: 20.0,
                left: 0.0,
                child: IconButton(
                  icon: Icon(Icons.arrow_back, color: AppColors.violet1,size: 23,),
                  onPressed: () {Navigator.pop(context);},
                )
            )
          ]
      )
    );
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        _navigateToPasswordSuccessPage();
        break;
      case States.ERROR:
        _showError(state.error.toString());
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void _showError(String errorMessage) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "$errorMessage",
        style: TextStyle(
          fontSize: 15.0, 
          color: Colors.white,
          fontFamily: "Schyler"),
      ),
      duration:  Duration(seconds: 3),              
    )..show(context);
  }

  Widget _buildResetButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        height: 17,
        width: 17,
        child: CircularProgressIndicator()
      );
    } else {
      return Container(
        height: 35.0,
        width: 200.0,
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(5.0),
          color: AppColors.violet1,
          child: MaterialButton(
            child: Text("RESET",
              style: TextStyle(
                fontFamily: 'Schyler',
                color: Colors.white,
                fontSize: 20.0
              )
            ),
            onPressed: _resetPassword,
          ),
        )
      );
    }
  }

  void _navigateToPasswordSuccessPage() =>
    Navigator.pushReplacementNamed(context, Routes.FORGOT_RESET_PASSWORD);

  void _resetPassword() {
    if (_emailController.text.isEmpty) return;
    final ForgotPasswordRequest request = ForgotPasswordRequest(email: _emailController.text);
    _authBloc.forgotPassword(request);
  }

  getBackground() async {
    var result = await dbHelper.selectImage("tbbackground" ,FORGOT_PASSWORD_BACKGROUND.toString());
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    for(var items in imagelist){
      setState(() {
        image = items["image"];
      });
    }
  }
}