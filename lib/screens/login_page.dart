import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/errors/not_activated_error.dart';
import 'package:plainsandprintsloyalty/model/request/login_request.dart';
import 'package:plainsandprintsloyalty/model/request/verification_request.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flushbar/flushbar.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  String image = "";
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FocusNode usernameFocusNode = new FocusNode();
  final FocusNode passwordFocusNode = new FocusNode();
  final dbHelper = DatabaseHelper.instance;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  StreamSubscription stateSubs;

  AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
    getBackground();
    getFCMToken();
  }

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    stateSubs = _authBloc.loginState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    stateSubs.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: image,
            height: MediaQuery.of(context).size.height * 1.0,
            width: MediaQuery.of(context).size.height * 1.0,
            fit: BoxFit.fitWidth,
            placeholder: (context, url) => new Center (
              child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)
            ),
            errorWidget: (context, url, error) => new Container(
              child: Image.asset('assets/images/pagebg.jpg',
                height: MediaQuery.of(context).size.height * 1.0,
                width: MediaQuery.of(context).size.height * 1.0,
                fit: BoxFit.fitWidth,
              )
            )
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              child: Center(
                child: Container(
                    width: 280,
                    height: 320,
                    decoration: new BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.circular(5.0)
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("LOG IN",
                            style: TextStyle(
                                fontFamily: 'Schyler',
                                color: AppColors.violet1,
                                fontSize: 25.0,
                                fontWeight: FontWeight.w500
                            )
                        ),
                        SizedBox(height: 20.0,),
                        Container(
                            height: 40.0,
                            margin: EdgeInsets.symmetric(horizontal: 20.0),
                            decoration: new BoxDecoration(
                                color: Colors.white70,
                                borderRadius: BorderRadius.circular(5.0)
                            ),
                            child: TextFormField(
                              focusNode: usernameFocusNode,
                              controller: _usernameController,
                              style: TextStyle(fontFamily: 'Schyler', color: Colors.black),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.fromLTRB(15, 10, 10, 12),
                                hintText: "Username or Email Address",
                                hintStyle: TextStyle(
                                    color: AppColors.violet1,
                                    fontSize: 16.0,
                                    fontFamily: 'Schyler'
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide( color: AppColors.violet1, width: 1.0 ),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide( color: AppColors.violet1, width: 1.0 ),
                                ),
                              ),
                              onFieldSubmitted: (value){
                                FocusScope.of(context).requestFocus(passwordFocusNode);
                              },
                            )
                        ),
                        SizedBox(height: 10.0),
                        Container(
                            height: 40.0,
                            margin: EdgeInsets.symmetric(horizontal: 20.0),
                            decoration: new BoxDecoration(
                                color: Colors.white70,
                                borderRadius: BorderRadius.circular(5.0)
                            ),
                            child: TextFormField(
                                focusNode: passwordFocusNode,
                                obscureText: true,
                                controller: _passwordController,
                                style: TextStyle(fontFamily: 'Schyler', color: Colors.black),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.fromLTRB(15, 10, 10, 12),
                                  hintText: "Password",
                                  hintStyle: TextStyle(
                                      color: AppColors.violet1,
                                      fontSize: 16.0,
                                      fontFamily: 'Schyler'
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: AppColors.violet1, width: 1.0),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: AppColors.violet1, width: 1.0),
                                  ),
                                ),
                              onEditingComplete: _login,
                            )
                        ),
                        SizedBox(height: 20.0,),
                        StreamBuilder<ScreenState>(
                            stream: _authBloc.loginState,
                            builder: (context, snapshot) => _buildLoginButton(snapshot)
                        ),
                        FlatButton(
                            child: Text("Forgot Password?",
                                style: TextStyle(
                                    fontFamily: 'Schyler',
                                    fontSize: 15.0,
                                    color: AppColors.violet1
                                )
                            ),
                            onPressed: _navigateToForgotPasswordPage
                        )
                      ],
                    )
                ),
              ),
            ),
          ),
          Positioned(
              top: 20.0,
              left: 0.0,
              child: IconButton(
                icon: Icon(Icons.arrow_back, color: AppColors.violet1,size: 23,),
                onPressed: () {Navigator.pop(context);},
              )
          )
        ],
      )
    );
  }

  Widget _buildLoginButton(AsyncSnapshot<ScreenState> snapshot) {
    if (snapshot.hasData && snapshot.data.state == States.WAITING) {
      return Container(
        child: Center(child: CircularProgressIndicator()),
      );
    } else {
      return Container(
        height: 35.0,
        width: 200.0,
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(5.0),
          color: AppColors.violet1,
          child: MaterialButton(
            child: Text("LOGIN",
              style: TextStyle(
                fontFamily: 'Schyler',
                color: Colors.white,
                fontSize: 20.0
              )
            ),
            onPressed: _login,
          ),
        )
      );
    }
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        _navigateToDashboardPage();
        break;
      case States.ERROR:
        _handleError(state);
        break;
      case States.IDLE:
        break;
      case States.WAITING:
        break;
    }
  }

  void _handleError(ScreenState state) {
    if (state.error is NotActivatedError) {
      _navigateToEmailConfirmation((state.error as NotActivatedError).request);
    } else {
      _showError(state.error.toString());
    }
  }

  Future _navigateToEmailConfirmation(VerificationRequest credentials) async {
    Navigator.of(context).pushNamed(Routes.ACCOUNT_VERIFICATION, arguments: credentials);
  }

  void _navigateToDashboardPage() =>
    Navigator.pushNamedAndRemoveUntil(context, Routes.DASHBOARD, ModalRoute.withName(Routes.DASHBOARD));

  void _navigateToForgotPasswordPage() =>
    Navigator.pushNamed(context, Routes.FORGOT_PASSWORD);

  void _showError(String errorMessage) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "$errorMessage",
        style: TextStyle(
          fontSize: 15.0, 
          color: Colors.white,
          fontFamily: "Schyler"),
      ),
      duration:  Duration(seconds: 3),              
    )..show(context);
  }

  void _login() async{
    getFCMToken();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('username', _usernameController.text);
    final LoginRequest request = LoginRequest(
      username: _usernameController.text, password: _passwordController.text, fcmtoken: prefs.getString('token')
    );
    _authBloc.login(request);
    Insert.insertLogin();
  }

  void getFCMToken(){
    _firebaseMessaging.getToken().then((token) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', token);
    });
  }

  getBackground() async {
    var result = await dbHelper.selectImage("tbbackground" ,LOGIN_BACKGROUND.toString());
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    for(var items in imagelist){
     setState(() {
       image = items["image"];
     });
    }
  }

}