import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';

class TermsAndConditionPage extends StatefulWidget {
  @override
  _TermsAndConditionPageState createState() => _TermsAndConditionPageState();
}

class _TermsAndConditionPageState extends State<TermsAndConditionPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: AppColors.violet1,
        title: Text(
          "TERMS AND CONDITIONS",
          style: TextStyle(fontFamily: 'Schyler'),
        ),
        centerTitle: true,
      ),
      body: Container(
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 0.0),
                    child: Text(
                      "By downloading, accessing, and using this Plains and Prints Mobile App, you agree to"+
                      " be bounded by these Terms and Conditions. We reserve the right to revoke membership,"+
                      " terminate the program, amend terms and conditions and/or restrict or modify usage of"+
                      " mobile app membership due to inconspicuous activity or as it deems fit, without prior notice."+
                      " The information that the customer will provide will be used by Plains & Prints to send you"+
                      " communications about our promos and offers. All information you will share will be kept"+
                      " confidential by Plains & Prints and will only be shared to our accredited and reputable third"+
                      " party agencies for the purpose of database managements and analysis.",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(50.0, 10.0, 30.0, 0.0),
                    child: Text(
                      "1.1	All Plains & Prints and RAF boutiques nationwide, both company owned, Franchise,"+
                      " online store www.plainsandprints.com and Plains & Prints mobile app are qualified to "+
                      " issue or transact points any time during Store Operations.\n"+
                      "1.2	Department Stores, wholesalers and concessions are not qualified branches.\n"+
                      "1.3	To enjoy full loyalty benefits, customer must always present the Plains & Prints"+
                      " app on every purchase at any qualified participating Plains & Prints and RAF boutiques.\n"+
                      "1.4	For online www.plainsandprints.com, customer must log in to his/her account before"+
                      " proceeding with his/her transaction.",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                      textAlign: TextAlign.start
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 0.0),
                    child: Text(
                      "REGISTRATION AND ACTIVATION VIA APP(EXISTING/NEW)",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(50.0, 10.0, 30.0, 0.0),
                    child: Text(
                      "2.1	Plains & Prints App can be downloaded by all.\n"+
                      "2.2	Customer must download the Plains and Prints Mobile App.\n"+
                      "2.3	Customer must select from the mobile app whether he/she is EXISTING or NEW member.\n"+
                      "2.4 For EXISTING members, customer must fill out the Customer ID, email address, mobile"+
                      " number and city address to complete registration or through FB registered information.\n"+
                      "2.5 For NEW members, customer must fill out Full Name, Birthdate, (MM-DD-YYYY), Email,"+
                      " Mobile     Number, City Address or through FB registered information.\n"+
                      "2.6	To activate the account, customer must verify his/her account via email.",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                      textAlign: TextAlign.start
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 0.0),
                    child: Text("LOYALTY LEVEL",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                          color: AppColors.violet1,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold
                        ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(50.0, 10.0, 30.0, 0.0),
                    child: Text(
                      "3.1 Regular – New/Existing Members\n"+
                      "\t\t\t\t\t•	Default loyalty level\n"+
                      "\t\t\t\t\t•	Earning of points is 3% of total transaction. System will round "+
                      "up if decimal is .5 & above and drop if decimal is .4 below.\n"+
                      "3.2	Prestige- Existing Members\n"+
                      "\t\t\t\t\t•	Earning of points is 10% of total transaction. System will round up "+
                      "if decimal is .5 & above and drop if decimal is .4 below.\n"+
                      "3.3 Elite- Existing Members\n"+
                      "\t\t\t\t\t•	Earning of points is 20% of total transaction. System will round up "+
                      "if decimal is .5 & above and drop if decimal is .4 below.",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                      textAlign: TextAlign.start
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 0.0),
                    child: Text("POINT/s EARNING",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(50.0, 10.0, 30.0, 0.0),
                    child: Text(
                      "4.1	For every purchase of any regular priced Plains & Prints and/or RAF merchandise, "+
                      "no minimum purchase required, cash, credit card or through purchased Gift Certificates, "+
                      "customer shall earn points depending on his/her loyalty type:\n"+
                      "\t\t\t\t\t•	Regular – 3% of total transaction\n"+
                      "\t\t\t\t\t•	Prestige – 10% of total transaction \n"+
                      "\t\t\t\t\t•	Elite – 20% of total transaction\n"+
                      "4.2	Plains & Prints/RAF merchandise is defined as regular priced items carrying the "+
                      "Plains & Prints/RAF brand name. Particularly: apparel, accessories, fragrances, gift items. \n"+
                      "\t\t\t\t\tNOT qualified purchases: \n"+
                      "\t\t\t\t\t\t\t\t\t\t•	Complimentary Gift Certificates \n"+
                      "\t\t\t\t\t\t\t\t\t\t•	Purchased Gift Certificates\n"+
                      "\t\t\t\t\t\t\t\t\t\t•	Consigned and Happy Skin Items\n"+
                      "\t\t\t\t\t\t\t\t\t\t•	Ruru and Filgifts Gift Certificates or other tie up GC promos\n"+
                      "\t\t\t\t\tFOR PURCHASED Gift Certificate:\n"+
                      "\t\t\t\t\t\t\t\t\t\t•	Purchase of Gift Certificate – NOT qualified \n"+
                      "\t\t\t\t\t\t\t\t\t\t•	Redemption/Usage of Gift Certificate –Qualified to earn points\n"+
                      "4.3	Points are earned based on total purchased amount only, either thru cash, credit "+
                      "card or purchased Gift Certificate redemption.\n"+
                      "4.4	For stores, Plains & Prints Mobile App must be presented at the cashier before "+
                      "payment in order to earn points. Cashier must scan the digital barcode in the "+
                      "Plains & Prints mobile app to earn points. NO Plains & Prints Mobile App, no points "+
                      "policy will be strictly implemented. \n"+
                      "4.5	For online (www.plainsandprints.com), customer must log in to his/her account "+
                      "before payment in order to earn points.  Not logged in to his/her account, no points "+
                      "policy will be strictly implemented.\n"+
                      "4.6	Points earned are credited REAL TIME.\n"+
                      "4.7	Points earned for the current year shall be valid for redemption until February "+
                      "28 of the succeeding year.\n"+
                      "4.8	All points earned shall be reflected on customers’ mobile app.\n"+
                      "4.9	In the event that a customer wishes for a refund/return based on a valid reason, "+
                      "points shall also be deducted from the customer account.\n"+
                      "4.10 Should the system go offline during a transaction, earning of points may still "+
                      "be permitted but no redemption will take place.\n"+
                      "4.11	One Plains & Prints mobile app e-loyalty card per transaction only.\n"+
                      "4.12	No splitting of points is allowed with multiple Plains & Prints app.\n"+
                      "4.13	Points may be earned at all Plains & Prints and RAF participating boutiques "+
                      "nationwide and online www.plainsandprints.com. Department Stores and other concessions "+
                      "not included.",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                      textAlign: TextAlign.start
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 0.0),
                    child: Text("POINT/s REDEMPTION",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                          color: AppColors.violet1,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(50.0, 10.0, 30.0, 0.0),
                    child: Text(
                      "5.1	“Redemption” is defined as the use of points to purchase merchandise.\n"+
                      "5.2	Point to Peso conversion: 1 point is equivalent to 1 Peso.\n"+
                      "5.3	Points may be used to purchase any Plains & Prints/RAF merchandise. "+
                      "Plains & Prints/RAF merchandise is defined as items carrying the "+
                      "Plains & Prints/RAF brand name particularly apparel, accessories, fragrances, "+
                      "gift items. \n"+
                      "5.4	Points CANNOT be used to purchase the following:\n"+
                      "\t\t\t\t\t•	Consigned items, such as Happy Skin\n"+
                      "\t\t\t\t\t•	Sale items \n"+
                      "\t\t\t\t\t•	Purchase of GC\n"+
                      "5.5	Customer may redeem his/her points with any purchase at any "+
                      "Plains and Prints and RAF boutiques or www.plainsandrints.com.\n"+
                      "5.6	For store based redemption, Customer must present his/her "+
                      "Plains & Prints mobile app to the cashier.\n"+
                      "5.7	For store based and online (www.plainsandprints.com) point’s redemption, "+
                      "customer needs to convert his/her points for voucher codes that can be used "+
                      "during transaction. Voucher code is valid within 48 hours. Unused vouchers will "+
                      "be reverted back to the account. \n"+
                      "5.8	For store based redemption, a valid ID must be presented to cashier for a "+
                      "successful redemption.\n"+
                      "5.9	For online (www.plainsandprints.com), customer must always log in to his/her "+
                      "account for a successful redemption.\n"+
                      "5.10	Points may be redeemed only in increments of P100. \n"+
                      "5.11	When system is offline, points cannot be redeemed BUT earning is allowed.\n"+
                      "5.12	One voucher code per transaction.\n"+
                      "5.13	All points’ redemption shall be reflected on customers’ mobile app.",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                      textAlign: TextAlign.start
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 0.0),
                    child: Text("POINT/s CHECKING",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(50.0, 10.0, 30.0, 0.0),
                    child: Text(
                      "6.1	Checking of points may be done at all participating "+
                      "Plains & Prints and RAF boutiques nationwide or thru Plains & "+
                      "Prints mobile app.",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                      textAlign: TextAlign.start
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 0.0),
                    child: Text("POINT/s VALIDITY",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(50.0, 10.0, 30.0, 0.0),
                    child: Text(
                      "7.1	Plains & Prints Mobile App users are eligible for a lifetime "+
                      "membership with the loyalty program. \n"+
                      "7.2	Members shall be responsible in maintaining accurate and "+
                      "updated information with Plains & Prints.\n"+
                      "7.3	Points earned for the current year are valid for redemption "+
                      "until February 28 of the succeeding year. ",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                      textAlign: TextAlign.start
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 0.0),
                    child: Text("CUSTOMER DETAILS UPDATE",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(50.0, 10.0, 30.0, 0.0),
                    child: Text(
                      "8.1 Customers can add or update their city address and mobile "+
                      "number thru Plains & Prints mobile app.\n"+
                      "8.2	In the case of wrong information input or change information "+
                      "specifically name, birth date and email address, customer must "+
                      "report via Plains & Prints’ support "+
                      "(customersupport@plainsandprints.com) or Plains & Prints boutiques.",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                      textAlign: TextAlign.start
                    ),
                  ),
                  SizedBox(height: 40,),
                ]
              )
            )
    );
  }
}