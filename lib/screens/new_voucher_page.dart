import 'dart:async';
import 'package:after_layout/after_layout.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/model/request/cancel_voucher_request.dart';
import 'package:plainsandprintsloyalty/model/request/convert_points_request.dart';
import 'package:plainsandprintsloyalty/model/voucher.dart';
import 'package:plainsandprintsloyalty/screens/arguments/convert_points_arguments.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:plainsandprintsloyalty/widgets/voucher_row.dart';

class NewVoucherPage extends StatefulWidget {
  @override
  _NewVoucherPageState createState() => _NewVoucherPageState();
}

class _NewVoucherPageState extends State<NewVoucherPage>
    with AfterLayoutMixin<NewVoucherPage> {
  
  AuthBloc _authBloc;
  CardBloc _cardBloc;

  StreamSubscription _cancelStateSubscription;

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _cardBloc = MasterProvider.card(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("New Voucher", style: TextStyle(fontFamily: 'Schyler',),),
        centerTitle: true,
      ),
      body: Container(
        child: StreamBuilder<Voucher>(
          stream: _cardBloc.newVoucher,
          builder: (context, snapshot) {
            return StreamHandler(
              snapshot: snapshot,
              noData: Center(
                child: Text("No vouchers at the moment."),
              ),
              loading: Center(child: CircularProgressIndicator()),
              withError: (error) => Center(
                child: Text("$error"),
              ),
              withData: (Voucher voucher) => Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(height: 30),
                    Container(
                      margin: EdgeInsets.all(20.0),
                      child:VoucherRow(
                          code: voucher.voucherCode,
                          status: voucher.status,
                          description: voucher.description,
                          expiration: voucher.expiration.toString(),
                          onTap: null),
                    ),
                    FlatButton(
                      child: Text("Cancel Voucher",
                        style: TextStyle(fontFamily: 'Schyler',color: Colors.red),
                      ),
                      onPressed: () => _showConfirmDialog(voucher),
                    )
                  ],
                )
              ),
            );
          }
        ),
      )
    );
  }

  @override
  void dispose() {
    _cancelStateSubscription.cancel();
    super.dispose();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _cancelStateSubscription = _cardBloc.cancelVoucherState.listen(_cancelStateListener);

    final ConvertPointsArguments args = ModalRoute.of(context).settings.arguments;
    convertPoints(args);
  }

  void _showConfirmDialog(Voucher voucher) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Convert Points"),
          content: Text("Are you sure you want to cancel this voucher?"),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Continue"),
              onPressed: () {
                Navigator.of(context).pop();
                _cancelVoucher(voucher);
              },
            ),
          ],
        );
      },
    );
  }

  void _cancelStateListener(ScreenState state) {
    switch (state.state) {
      case States.IDLE:
        break;
      case States.WAITING:
        break;
      case States.DONE:
        Navigator.of(context).pop();
        break;
      case States.ERROR:
        _showError("${state.error.toString()}");
        break;
    }
  }

  void _showError(String errorMessage) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "$errorMessage",
        style: TextStyle(
          fontSize: 15.0, 
          color: Colors.white,
          fontFamily: "Schyler"),
      ),
      isDismissible: true,
      duration:  Duration(seconds: 5),
      mainButton: FlatButton(
        onPressed: () {
          Navigator.pop(context);
          Navigator.pop(context);
        },
        child: Text(
          "OK",
          style: TextStyle(
            fontFamily: 'Schyler',
            fontSize: 18,
            fontWeight: FontWeight.bold),
        ),
      ),              
    )..show(context);
  }

  void _cancelVoucher(Voucher voucher) {
    final CancelVoucherRequest request = CancelVoucherRequest(
      accountId: _authBloc.session.value.accountId,
      cardPan: _authBloc.session.value.cardPan,
      voucherCode: voucher.voucherCode
    );
    _cardBloc.cancelVoucher(request);
  }

  void convertPoints(ConvertPointsArguments args) {
    final ConvertPointsRequest request = args.request;
    _cardBloc.convertPoints(request);
  }
}
