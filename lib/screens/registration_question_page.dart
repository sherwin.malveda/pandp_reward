import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/model/request/member_request.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/arguments/registration_arguments.dart';

class RegistrationQuestionPage extends StatefulWidget {
  @override
  _RegistrationQuestionPageState createState() =>_RegistrationQuestionPageState();
}

class _RegistrationQuestionPageState extends State<RegistrationQuestionPage> {

  final dbHelper = DatabaseHelper.instance;
  String image = "";

  @override
  void initState(){
    super.initState();
    getBackground();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  void _navigateToRegistration(bool isExisting) {
    MemberRequest request;
    final RegistrationArguments arguments = RegistrationArguments(isExisting,"",request);
    Navigator.of(context).pushNamed(Routes.REGISTRATION, arguments: arguments);
  }

  void _navigateToExistingMember() {
    Navigator.of(context).pushNamed(Routes.EXISTING_MEMBER);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          CachedNetworkImage(
              imageUrl: image,
              height: MediaQuery.of(context).size.height * 1.0,
              width: MediaQuery.of(context).size.height * 1.0,
              fit: BoxFit.fitWidth,
              placeholder: (context, url) => new Center (
                  child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)
              ),
              errorWidget: (context, url, error) => new Container(
                  child: Image.asset('assets/images/pagebg.jpg',
                    height: MediaQuery.of(context).size.height * 1.0,
                    width: MediaQuery.of(context).size.height * 1.0,
                    fit: BoxFit.fitWidth,
                  )
              )
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              child: Center(
                child: Container(
                    width: 280,
                    height: 300,
                    decoration: new BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.circular(5.0)
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("Are you an",
                            style: TextStyle(
                                fontFamily: 'Schyler',
                                color: AppColors.violet1,
                                fontSize: 20.0,
                                fontWeight: FontWeight.w500
                            )
                        ),
                        SizedBox(height: 15.0,),
                        Container(
                            height: 40.0,
                            width: 200.0,
                            child: Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(5.0),
                              color: AppColors.violet1,
                              child: MaterialButton(
                                  child: Text( "Existing Member",
                                    style: TextStyle(
                                        fontFamily: 'Schyler',
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        letterSpacing: 2.0
                                    ),
                                  ),
                                  onPressed: () => _navigateToExistingMember()
                              ),
                            )
                        ),
                        SizedBox(height: 15.0,),
                        Text("or a",
                            style: TextStyle(
                                fontFamily: 'Schyler',
                                color: AppColors.violet1,
                                fontSize: 20.0,
                                fontWeight: FontWeight.w500
                            )
                        ),
                        SizedBox(height: 15.0,),
                        Container(
                            height: 40.0,
                            width: 200.0,
                            child: Material(
                              elevation: 5.0,
                              borderRadius: BorderRadius.circular(5.0),
                              color: AppColors.violet1,
                              child: MaterialButton(
                                  child: Text("New Member",
                                    style: TextStyle(
                                        fontFamily: 'Schyler',
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        letterSpacing: 2.0
                                    ),
                                  ),
                                  onPressed: () => _navigateToRegistration(false)
                              ),
                            )
                        )
                      ],
                    )
                ),
              ),
            ),
          ),
          Positioned(
              top: 20.0,
              left: 0.0,
              child: IconButton(
                icon: Icon(Icons.arrow_back, color: AppColors.violet1,size: 23,),
                onPressed: () {Navigator.pop(context);},
              )
          )
        ],
      )
    );
  }

  getBackground() async {
    var result = await dbHelper.selectImage("tbbackground" ,EXISTING_MEMBER_BACKGROUND.toString());
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    for(var items in imagelist){
      setState(() {
        image = items["image"];
      });
    }
  }
}