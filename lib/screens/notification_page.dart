import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/notifications.dart';
import 'package:plainsandprintsloyalty/model/offline_data_model/notificationoffline.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/screens/notificationbody_page.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {

  StoreBloc _storeBloc;
  AuthBloc _authBloc;
  final List<NotificationOffline> notificationoffline = new List<NotificationOffline>();
  final dbHelper = DatabaseHelper.instance;

  RefreshController _refreshController = RefreshController(
      initialRefresh: false);

  void initState() {
    super.initState();
    getNotification();
  }

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _storeBloc = MasterProvider.store(context);
    _storeBloc.getNotification(ListRequest(accountId: _authBloc.session.value.accountId, rows: INFINITE_ROW_LIST, page: 1));
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text(
          "NOTIFICATIONS",
          style: TextStyle(fontFamily: 'Schyler'),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: StreamBuilder(
          stream: _storeBloc.notification,
          builder: (BuildContext context, AsyncSnapshot<Notifications> snapshot) {
            return StreamHandler(
              snapshot: snapshot,
              loading: Center(child: CircularProgressIndicator()),
              noData: Center(child: Text("No notification at the moment", style: TextStyle(fontFamily: 'Schyler'),)),
              withError: (error) => offline(),
              withData: (Notifications notification) {
                saveOffline(notification);
                return  SmartRefresher(
                    enablePullDown: true,
                    enablePullUp: true,
                    header: ClassicHeader(
                      refreshingText: "Refreshing",
                      textStyle: TextStyle(fontFamily: 'Schyler'),
                    ),
                    footer: CustomFooter(
                      builder: (BuildContext context,LoadStatus mode){
                        Widget body ;
                        if(mode == LoadStatus.idle){
                          body =  Text("No more items", style: TextStyle(fontFamily: 'Schyler'));
                        }
                        else if(mode==LoadStatus.loading){
                          body =  CupertinoActivityIndicator();
                        }
                        else if(mode == LoadStatus.failed){
                          body = Text("Load Failed! Click retry!", style: TextStyle(fontFamily: 'Schyler'));
                        }
                        else if(mode == LoadStatus.canLoading){
                            body = Text("Release to load more", style: TextStyle(fontFamily: 'Schyler'));
                        }
                        else{
                          body = Text("No more items", style: TextStyle(fontFamily: 'Schyler'),);
                        }
                        return Container(
                          height: 55.0,
                          child: Center(child:body),
                        );
                      },
                    ),
                    controller: _refreshController,
                    onRefresh: () => _onRefresh(),
                    onLoading: () => _onLoading(),
                    child: ListView.builder(
                        itemCount: notification.message.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                              color: Colors.white,
                              child: FlatButton(
                                padding: EdgeInsets.all(0),
                                child: Row(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.symmetric(horizontal: 8.0,vertical: 5.0),
                                      child: _notificationImage(notification.message[index].messageIcon)
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: <Widget>[
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(0.0, 5.0, 3.0, 0.0),
                                              child: Text(notification.message[index].messageTitle,
                                                  style: TextStyle(
                                                      fontFamily: 'Schyler',
                                                      color: AppColors.violet2,
                                                      fontSize: 12.0,
                                                      fontWeight: FontWeight.bold
                                                  ),
                                                  textAlign: TextAlign.start
                                              )
                                          ),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(5.0, 2.0, 10.0, 5.0),
                                              child: Text(notification.message[index].messageBody,
                                                style: TextStyle(
                                                    fontFamily: 'Schyler',
                                                    color: AppColors.violet1,
                                                    fontSize: 10.0
                                                ),
                                                maxLines: 2,
                                              )
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                onPressed: () => Navigator.push(
                                    context, MaterialPageRoute(
                                    builder: (_) {
                                      return NotificationBodyPage(
                                        notifbody: notification.message[index].messageContent,
                                        notiftitle: notification.message[index].messageTitle,
                                        notifimage: notification.message[index].messageImage,
                                      );
                                    }
                                )
                                ),
                              )
                          );
                        }
                    )
                );
              },
            );
          }
      ),
    );
  }

  void _onRefresh() async {
    await Future.delayed(Duration(milliseconds: 1000));
    if (mounted)
      setState(() {
        _storeBloc.getNotification(ListRequest(accountId: _authBloc.session.value.accountId, rows: INFINITE_ROW_LIST, page: 1));
      });
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await Future.delayed(Duration(milliseconds: 1000));
    if (mounted)
      setState(() {
        _storeBloc.getNotification(ListRequest(accountId: _authBloc.session.value.accountId, rows: INFINITE_ROW_LIST, page: 1));
      });
    _refreshController.loadComplete();
  }

  _notificationImage(String image) {
    if (image.length == 0) {
      return Container(
          width: 65.0,
          height: 65.0,
          decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                  fit: BoxFit.cover,
                  image: new AssetImage(
                      "assets/images/logo.png")
              ),
              border: Border.all(color: AppColors.violet1)
          )
      );
    }
    else {
      return Container(
          width: 65.0,
          height: 65.0,
          decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(image),
              ),
              border: Border.all(color: AppColors.violet1)
          )
      );
    }
  }

  saveOffline(Notifications notification) async{
    await Insert.dbHelper.delete("notification");
    for(int x=0; x<notification.message.length; x++){
      Insert.insertNotification(
          notification.message[x].messageTitle,
          notification.message[x].messageContent,
          notification.message[x].messageImage,
        notification.message[x].messageBody,
        notification.message[x].messageIcon
      );
    }
  }

  void getNotification() async {
    notificationoffline.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var result = await dbHelper.selectAllData2(
        "notification", prefs.getString('username'));
    List<Map<String, dynamic>> notificationList = result;
    if (this.mounted) {
      setState(() {
        final item = notificationList;
        for (var items in item) {
          notificationoffline.add(
              new NotificationOffline(items['notification_title'], items['notification_body'],
                  items['datetime'], items['notification_image'], items['notification_icon']
              )
          );
        }
      });
    }
  }

  offline(){
    return SmartRefresher(
        enablePullDown: true,
        header: ClassicHeader(
          refreshingText: "Refreshing",
          textStyle: TextStyle(fontFamily: 'Schyler'),
        ),
        controller: _refreshController,
        onRefresh: () => _onRefresh(),
        child: notificationoffline.isEmpty
        ? Center(child: Text("No notification at the moment", style: TextStyle(fontFamily: 'Schyler'),))
        : ListView.builder(
            itemCount: notificationoffline.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                  color: Colors.white,
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    child: Row(
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8.0,vertical: 5.0),
                            child: _notificationImage(notificationoffline[index].notificon)
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.fromLTRB(0.0, 5.0, 3.0, 0.0),
                                  child: Text(notificationoffline[index].title,
                                      style: TextStyle(
                                          fontFamily: 'Schyler',
                                          color: AppColors.violet2,
                                          fontSize: 12.0,
                                          fontWeight: FontWeight.bold
                                      ),
                                      textAlign: TextAlign.start
                                  )
                              ),
                              Padding(
                                  padding: EdgeInsets.fromLTRB(5.0, 2.0, 10.0, 5.0),
                                  child: Text(notificationoffline[index].datetime,
                                    style: TextStyle(
                                        fontFamily: 'Schyler',
                                        color: AppColors.violet1,
                                        fontSize: 10.0
                                    ),
                                    maxLines: 2,
                                  )
                              ),/*
                              Padding(
                                  padding: EdgeInsets.fromLTRB(20.0, 2.0, 10.0, 5.0),
                                  child: Text(DateFormat("yyyy-MM-dd hh:mm:ss").format(DateTime.now()),
                                      style: TextStyle(
                                          fontFamily: 'Schyler',
                                          color: Colors.black,
                                          fontSize: 10.0
                                      ),
                                      textAlign: TextAlign.end
                                  )
                              ),*/
                            ],
                          ),
                        )
                      ],
                    ),
                    onPressed: () => Navigator.push(
                        context, MaterialPageRoute(
                        builder: (_) {
                          return NotificationBodyPage(
                            notifbody: notificationoffline[index].body,
                            notiftitle: notificationoffline[index].title,
                            notifimage: notificationoffline[index].imageurl,
                          );
                        }
                    )
                    ),
                  )
              );
            }
        )
    );
  }

}