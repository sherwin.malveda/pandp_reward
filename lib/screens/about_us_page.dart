import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
class AboutUsPage extends StatefulWidget {
  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: AppColors.violet1,
        title: Text(
          "ABOUT US",
          style: TextStyle(fontFamily: 'Schyler'),
        ),
        centerTitle: true,
      ),
      body: Container(
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(30.0),
                    child: Text(
                      "Plains & Prints is a ready-to-go wear women's apparel brand with the "+
                      "modern wowen in mind. Its distinct yet stylish pieces has made Plains & "+
                      "Prints one of the top local brand in the Philippines.\n\n"+
                      "The brand specializes in casual tops, bottoms, dresses, playsuits and"+
                      " accessories for that fashion forward sophisticated woman.\n\n"+
                      "Today, Plains & Prints is one of the leading fashion brands in the country. "+
                      "But its humble beginnings trace back to a young college girl's dream of starting "+
                      "her own clothing line.\n\n"+
                      "In 1994, the first Plains & Prints boutique was born at Shoppersville Plus in "+
                      "Greenhills. An 11-square meter stall manned by two staffs. It was then that Plains "+
                      "& Prints began its trade mark creation of timeless pieces that never went out of style.\n\n"+
                      "In the years that followed, the Plains & Prints vision of creating elegant, quality women's"+
                      " apparel flourished and business continued to expand.\n\n",
                      style: TextStyle(
                        fontFamily: 'Schyler',
                        color: AppColors.violet1,
                        fontSize: 15.0
                      ),
                        textAlign: TextAlign.center
                    ),
                  )
                ]
              )
            ),
    );
  }
}
