import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/branch.dart';
import 'package:plainsandprintsloyalty/model/branches.dart';
import 'package:plainsandprintsloyalty/model/offline_data_model/offlinebranch.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/screens/branch_details_page.dart';
import 'package:plainsandprintsloyalty/widgets/branch_row.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BranchesPage extends StatefulWidget {
  @override
  _BranchesPageState createState() => _BranchesPageState();
}

class _BranchesPageState extends State<BranchesPage> {
  final TextEditingController _searchController = TextEditingController();

  StoreBloc _storeBloc;
  AuthBloc _authBloc;
  RefreshController _refreshController = RefreshController(initialRefresh: false);
  List<OfflineBranch> offlinebranch = new List<OfflineBranch>();
  final dbHelper = DatabaseHelper.instance;

  @override
  void initState() {
    _searchController.addListener(_onSearch);
    super.initState();
    getBranches();
  }

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("STORE LOCATION", style: TextStyle(fontFamily: 'Schyler'),),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: StreamBuilder(
        stream: _storeBloc.branches,
        builder: (BuildContext context, AsyncSnapshot<Branches> snapshot) {
          return SmartRefresher(
              enablePullDown: true,
              header: ClassicHeader(
              refreshingText: "Refreshing",
              textStyle: TextStyle(fontFamily: 'Schyler'),
            ),
            controller: _refreshController,
            onRefresh:()=> _onRefresh(),
            child: StreamHandler(
              snapshot: snapshot,
              withError: (error) =>  offline(),
              loading: Center(child: CircularProgressIndicator()),
              noData: offline(),
              withData: (Branches branches) {
                saveOffline(branches);
                final List<Branch> _filteredBranch = _filterBranches(branches);
                return Column(
                  children: <Widget>[
                    SizedBox(height: 8.0),
                    searchbar(),
                    SizedBox(height: 8.0),
                    Expanded(
                      child: ListView.builder(
                          itemCount: _filteredBranch.length,
                          itemBuilder: (BuildContext context, int index) {
                            return BranchRow(
                              onPressed: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => BranchDetailsPage( branch: _filteredBranch[index].branchId,)
                                )
                              ),
                              branch: _filteredBranch[index].branchName
                            );
                          }
                        ),
                      ),
                  ],
                );
              },
            )
          );
        }
      ),
    );
  }

  searchbar(){
    return Container(
        margin: EdgeInsets.all(4.0),
        color: AppColors.violet3,
        padding: EdgeInsets.all(4.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                controller: _searchController,
                cursorColor: AppColors.violet1,
                style: TextStyle(fontFamily: 'Schyler',color: AppColors.violet1),
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: TextStyle(fontFamily: 'Schyler',color: AppColors.violet1),
                    hintText: "Search Location",
                    prefixIcon: Icon(Icons.search, color: AppColors.violet1)
                ),
              ),
            ),
            _searchController.text.length == 0
                ? Container()
                : Container(
              width: 50.0,
              height: 50.0,
              child: IconButton(
                icon: Icon(Icons.close, color: AppColors.violet1,),
                onPressed: (){
                  _searchController.text = "";
                },
              ),
            )
          ],
        )
    );
  }

  List<Branch> _filterBranches(Branches branches) {
    final List<Branch> branchesCopy = List<Branch>.from(branches.branches)
      .where((branch) =>
      branch.branchName.toLowerCase().contains(
        _searchController.text.toLowerCase()
      )).toList();
    return branchesCopy;
  }

  void _onSearch() {
    setState(() {});
  }

  void _onRefresh() async{
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {
      _storeBloc.getBranches(ListRequest(
          accountId: _authBloc.session.value.accountId,
          rows: INFINITE_ROW_LIST,
          page: 1)
      );
    });
    _refreshController.refreshCompleted();
  }

  saveOffline(Branches branches) async {
    await Insert.dbHelper.delete("tblocation");
    for(int x=0; x<branches.branches.length; x++){
      Insert.insertBranches(branches.branches[x].branchId.toString(),branches.branches[x].branchName,
          branches.branches[x].branchCode, branches.branches[x].branchAddress);
    }
  }

  getBranches() async {
    var result = await dbHelper.selectAllData("tblocation");
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    offlinebranch.clear();
    for(var items in imagelist){
      setState(() {
        offlinebranch.add(new OfflineBranch(items["branchid"],items["name"], items["code"], items["address"]));
      });
    }
  }

  offline(){
    return Column(
      children: <Widget>[
        SizedBox(height: 8.0),
        searchbar(),
        SizedBox(height: 8.0),
        Expanded(
          child: offlinebranch.isEmpty
            ? Center(child:Text("No branches at the moment", style: TextStyle(fontFamily: 'Schyler'),))
            : ListView.builder(
                itemCount: offlinebranch.length,
                itemBuilder: (BuildContext context, int index) {
                  return BranchRow(
                      onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => BranchDetailsPage(branch: int.parse(offlinebranch[index].branchId))
                          )
                      ),
                      branch: offlinebranch[index].branchName
                  );
                }
            ),
        ),
      ],
    );
  }
}
