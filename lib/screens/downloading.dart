import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/pages_background.dart';
import 'package:plainsandprintsloyalty/model/provinces.dart';
import 'package:plainsandprintsloyalty/routes.dart';

class Downloading extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DownloadingState();
  }
}

class DownloadingState extends State<Downloading> {

  @override
  void initState(){
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    getPage("7");
    getPage("4");
    getPage("8");
    getProvinceList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.white);
  }

  Future getPage(String pageId) async {
    try{
      String url = API_ENDPOINT+"/api/v1/page/"+pageId;
      Map<String, String> headers = {"Content-type": "application/json",
        "apikey":"b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w=="};
      final response = await get(url, headers: headers);
      var responses = PageBackground.fromMap(jsonDecode(response.body));
      if(response.statusCode == 200){
        await Insert.dbHelper.deleteImage("tbbackground",pageId);
        Insert.insertImage("tbbackground", pageId, responses.pageImage);
      }
    }catch(Exception){ print(Exception);}
  }

  Future getProvinceList() async {
    try{
      String url = API_ENDPOINT+"/api/v1/dropdown/provinces";
      Map<String, String> headers = {"Content-type": "application/json",
        "apikey":"b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w=="};
      final response = await get(url, headers: headers);
      var responses = Provinces.fromJson(jsonDecode(response.body));
      for(int x=0; x<responses.provinces.length; x++){
        provinceList[responses.provinces[x].provinceId] = responses.provinces[x].provinceName;
      }
    }catch(Exception){print(Exception);}
    loadData();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 1), gotoSplashScreen());
  }

  gotoSplashScreen(){
    Navigator.of(context).pushReplacementNamed(Routes.SPLASH_SCREEN);
  }

}