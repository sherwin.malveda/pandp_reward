import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/model/branch_details.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';

class BranchDetailsPage extends StatefulWidget {
  final int branch;

  const BranchDetailsPage({Key key, this.branch}) : super(key: key);
  
  @override
  _BranchDetailsPageState createState() => _BranchDetailsPageState();
}

class _BranchDetailsPageState extends State<BranchDetailsPage> {
  final Completer<GoogleMapController> _controller = Completer();

  StoreBloc _storeBloc;

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    _storeBloc.getBranchDetails(widget.branch);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("STORE DETAILS", style: TextStyle(fontFamily: 'Schyler'),),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: StreamBuilder<BranchDetails>(
        stream: _storeBloc.branchDetails,
        builder: (context, snapshot) {
          return StreamHandler(
            snapshot: snapshot,
            loading: Center(child: CircularProgressIndicator()),
            withError: (error) => Padding(
              padding: EdgeInsets.all(16),
              child: Center(
                child: Text("Map not available",
                  textAlign: TextAlign.center,
                )
              )
            ),
            noData: Padding(
              padding: EdgeInsets.all(16),
              child: Center(
                child: Text("Map not available",
                  textAlign: TextAlign.center,
                )
              )
            ),
            withData: (BranchDetails branchDetails) {
              final CameraPosition branchPosition = CameraPosition(
                target: LatLng(double.parse(branchDetails.latitude),double.parse(branchDetails.longitude)),
                zoom: 14.4746,
              );
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0,10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("PLAINS & PRINTS "+branchDetails.branchName,
                            style: TextStyle(
                              fontSize: 20,
                              fontFamily: 'Schyler',
                              color: AppColors.violet1
                            ),
                            textAlign: TextAlign.center
                          ),
                          Text(branchDetails.branchAddress,
                            style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Schyler',
                              color: AppColors.violet1
                            ),
                            textAlign: TextAlign.center
                          ),
                          Text(branchDetails.landlineNumber,
                            style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Schyler',
                              color: AppColors.violet1
                            ),
                            textAlign: TextAlign.center),
                          Text(branchDetails.mobileNumber,
                            style: TextStyle(
                              fontSize: 15,
                              fontFamily: 'Schyler',
                              color: AppColors.violet1
                            ),
                            textAlign: TextAlign.center
                          ),
                        ],
                      ),
                    )
                  ),
                  Expanded(
                    child: GoogleMap(
                      markers: Set()
                        ..add(Marker(
                          markerId: MarkerId(branchDetails.branchCode),
                          position: LatLng(
                            double.parse(branchDetails.latitude),
                            double.parse(branchDetails.longitude)
                          )
                        )
                      ),
                      mapType: MapType.normal,
                      initialCameraPosition: branchPosition,
                      onMapCreated: (GoogleMapController controller) {
                        _controller.complete(controller);
                      },
                    ),
                  ),
                ],
              );
            },
          );
        }
      )
    );
  }
}