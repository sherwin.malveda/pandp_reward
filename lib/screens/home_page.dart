import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/controllers/navigation_controller.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/city.dart';
import 'package:plainsandprintsloyalty/model/news_letters.dart';
import 'package:plainsandprintsloyalty/model/offline_data_model/newsletter.dart';
import 'package:plainsandprintsloyalty/model/provinces.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/newsletter_fullscreen_page.dart';
import 'package:plainsandprintsloyalty/utils/date_utils.dart';
import 'package:plainsandprintsloyalty/widgets/icon_card.dart';
import 'package:plainsandprintsloyalty/widgets/large_app_bar.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:plainsandprintsloyalty/widgets/type_writer_text.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  final NavigationController navigationController;

  const HomePage({Key key, @required this.navigationController})
      : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List<OfflineNewsLetter> newsletteroffline = new List<OfflineNewsLetter>();
  final dbHelper = DatabaseHelper.instance;
  StoreBloc _storeBloc;
  CardBloc _cardBloc;
  AuthBloc _authBloc;
  var count, name = "";

  @override
  void didChangeDependencies() {
    _storeBloc = MasterProvider.store(context);
    _cardBloc = MasterProvider.card(context);
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  void initState(){
    super.initState();
    getCount();
    getNewsletter();
    getProvinceList();
  }

  @override
  Widget build(BuildContext context) {
    updateNewsletter();
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: LargeAppBar(
        child: Image.asset("assets/images/logo-white.png",
          height: 90, 
          width: 90
        )
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: ListView(
                children: <Widget>[
                  Container(
                    child: StreamBuilder<User>(
                      stream: _cardBloc.currentUser,
                      builder: (context, snapshot) {
                        return StreamHandler(
                          snapshot: snapshot,
                          loading: Container(child: Center(child: CircularProgressIndicator(),),),
                          noData: offlinegreet(),
                          withError: (error) => offlinegreet(),
                          withData: (User user) => Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              greet()
                            ],
                          )
                        );
                      }
                    ),
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                  ),
                  SizedBox(height: 20),
                  Text("ACTIONS",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: AppColors.violet1, 
                      fontFamily: 'Schyler',
                      fontSize: 20,
                      fontWeight: FontWeight.w500
                    )
                  ),
                  SizedBox(height: 10),
                  SizedBox( height: 70,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SizedBox(width: 5.0,),
                        IconCard(
                          assetImage: "assets/images/convert.png",
                          title: "REDEEM POINTS",
                          onPressed: () => Navigator.pushNamed(context, Routes.CONVERT_POINTS)
                        ),
                        SizedBox(width: 5.0,),
                        IconCard(
                          assetImage: "assets/images/voucher.png",
                          title: "VIEW VOUCHER",
                          onPressed: () => Navigator.pushNamed(context, Routes.VOUCHERS)
                        ),
                        SizedBox(width: 5.0,),
                        IconCard(
                          assetImage: "assets/images/branch.png",
                          title: "VIEW STORE LOCATIONS",
                          onPressed: () => Navigator.pushNamed(context, Routes.BRANCHES)
                        ),
                        SizedBox(width: 5.0,),
                        IconCard(
                          assetImage: "assets/images/online-shop.png",
                          title: "SHOP ONLINE",
                          onPressed: _goToWebsite
                        ),
                        SizedBox(width: 5.0,),
                        IconCard(
                          assetImage: "assets/images/view-card.png",
                          title: "VIEW E-LOYALTY CARD",
                          onPressed: () => Navigator.pushNamed(context, Routes.CARD)
                        ),
                        SizedBox(width: 5.0,),
                      ],
                    ),
                  ),
                  SizedBox(height: 15),
                  Text("WHAT'S NEW?",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: AppColors.violet1,
                      fontFamily: 'Schyler',
                      fontSize: 20,
                      fontWeight: FontWeight.w500
                    )
                  ),
                  SizedBox(height: 10),
                  StreamBuilder<NewsLetters>(
                    stream: _storeBloc.newsLetters,
                    builder: (context, snapshot) {
                      return StreamHandler(
                        snapshot: snapshot,
                        noData: Center(
                            child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                                child: Text("No news at the moment",
                                  style: TextStyle(fontFamily: 'Schyler'),
                                )
                            )
                        ),
                        loading: Center(child: CircularProgressIndicator()),
                        withError: (error) => newsletteroffline.isEmpty
                        ? Center(
                            child: Padding(padding: EdgeInsets.symmetric(vertical:50),
                                child: Text("No news at the moment",
                                  style: TextStyle(fontFamily: 'Schyler'),
                                )
                            )
                        )
                        : offline(),
                        withData: (NewsLetters newsletters) {
                          saveOffline(newsletters);
                          return SizedBox(
                              height: (MediaQuery.of(context).size.width * (9/16)),
                              child: Container(
                                  margin: EdgeInsets.fromLTRB(5.0,0.0,5.0,5.0),
                                  child: Carousel(
                                    dotColor: AppColors.violet1,
                                    indicatorBgPadding: 8.0,
                                    dotBgColor: Colors.transparent,
                                    dotIncreasedColor: AppColors.violet1,
                                    dotSize: 5.0,
                                    dotSpacing: 20.0,
                                    autoplay: true,
                                    autoplayDuration: Duration(seconds: 5),
                                    boxFit: BoxFit.contain,
                                    images: newsletters.newsLetters
                                        .take(newsletters.newsLetters.length > 5
                                        ? 5
                                        : newsletters.newsLetters.length)
                                        .map((newsLetter) =>
                                        _bannerImage('${newsLetter.imagePath}')
                                    ).toList(),
                                    onImageTap: (value) {
                                      Navigator.push(
                                          context, MaterialPageRoute(
                                          builder: (_) {
                                            return NewsletterFullScreenPage(
                                              title: newsletters
                                                  .newsLetters[value].title,
                                              content: newsletters
                                                  .newsLetters[value].content,
                                              imagepath: newsletters
                                                  .newsLetters[value].imagePath,
                                            );
                                          }
                                      )
                                      );
                                    },
                                  )
                              )
                          );
                        }
                      );
                    }
                  ),
                ]
              ),
            ),
          )
        ],
      ),
    );
  }

  greet() {
    getCityList();
    savedUser();
    try{
      if(count.length != 0){
        deleteData();
        return ControlledAnimation(
          duration: Duration(milliseconds: 500),
          tween: Tween(begin: 0.0, end: 80.0),
          builder: (context, height) {
            return ControlledAnimation(
              duration: Duration(milliseconds: 1200),
              delay: Duration(milliseconds: 500),
              tween: Tween(begin: 2.0, end: 300.0),
              builder: (context, width) {
                return Container(
                decoration: BoxDecoration(
                  color: AppColors.violet3,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withAlpha(40),
                      blurRadius: 10,
                      offset: Offset(0, 5),
                      spreadRadius: 1
                    )
                  ]
                ),
                width: width + 40,
                height: 60,
                child: isEnoughRoomForTypewriter(width)
                  ? TypewriterText(DateUtils.getGreetings(DateTime.now()) + ", ${_cardBloc.currentUser.value.firstname}!")
                  : Container(),
                );
              },
            );
          },
        );
      }else{
        return Container(
            decoration: BoxDecoration(
                color: AppColors.violet3,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withAlpha(40),
                      blurRadius: 10,
                      offset: Offset(0, 5),
                      spreadRadius: 1
                  )
                ]
            ),
            width: MediaQuery.of(context).size.width *1.0,
            height: 60,
            child: Center(
              child: Text( DateUtils.getGreetings(DateTime.now()) + ", ${_cardBloc.currentUser.value.firstname}!",
                  style: TextStyle(
                      fontFamily: 'Schyler',
                      fontSize: 22,
                      color: AppColors.violet1,
                      fontWeight: FontWeight.bold
                  ),
                  textAlign: TextAlign.center
              ),
            )
        );
      }
    }catch(Exception){
      return Container(
          decoration: BoxDecoration(
              color: AppColors.violet3,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                    color: Colors.black.withAlpha(40),
                    blurRadius: 10,
                    offset: Offset(0, 5),
                    spreadRadius: 1
                )
              ]
          ),
          width: MediaQuery.of(context).size.width *1.0,
          height: 60,
          child: Center(
            child: Text( DateUtils.getGreetings(DateTime.now()) + ", ${_cardBloc.currentUser.value.firstname}!",
                style: TextStyle(
                    fontFamily: 'Schyler',
                    fontSize: 22,
                    color: AppColors.violet1,
                    fontWeight: FontWeight.bold
                ),
                textAlign: TextAlign.center
            ),
          )
      );
    }

  }

  savedUser() async{
    String name = _cardBloc.currentUser.value.firstname +" " +
      _cardBloc.currentUser.value.middleName + " "+_cardBloc.currentUser.value.lastName;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('username', _cardBloc.currentUser.value.firstname);
    await prefs.setString('name', name);
    await prefs.setString('birthday', DateFormat("MMM dd,yyyy").format(_cardBloc.currentUser.value.birthDate).toString());
    await prefs.setString('mobile', _cardBloc.currentUser.value.mobile);
    await prefs.setString('email', _cardBloc.currentUser.value.email);
    await prefs.setString('province', provinceList[_cardBloc.currentUser.value.province]);
    await prefs.setString('city', cityList[_cardBloc.currentUser.value.city]);
    await prefs.setString('cardType', _cardBloc.currentUser.value.cardType);
  }

  getCount() async {
    count = await dbHelper.selectAllData("greeting");
  }

  deleteData() async {
    await dbHelper.delete('greeting');
  }

  isEnoughRoomForTypewriter(width) => width > 20;

  _goToWebsite() async {
    const url = 'https://www.plainsandprints.com/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _bannerImage(String image){
    if(image.length == 0 || image == null){
      return Image.asset('assets/images/bannerdefault.jpg', fit: BoxFit.contain);
    }
    else{
      return CachedNetworkImage(
        imageUrl: image,
        fit: BoxFit.contain,
        placeholder: (context, url) => new Center(child: CircularProgressIndicator(backgroundColor: AppColors.violet1,)),
        errorWidget: (context, url, error) => new Container( child:Center(
            child:  Text("Failed to load the image.",
              style: TextStyle(fontFamily: 'Schyler', fontSize: 15.0))
        ),
      )
    );
  }
}

void updateNewsletter(){
    try{
      _storeBloc.getNewsLetter(ListRequest(
          accountId: _authBloc.session.value.accountId,
          rows: MAX_ROW_LIST,
          page: 1)
      );
    }catch(Exception){}
  }

  Future getProvinceList() async {
    try{
      String url = API_ENDPOINT+"/api/v1/dropdown/provinces";
      Map<String, String> headers = {"Content-type": "application/json",
        "apikey":"b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w=="};
      final response = await get(url, headers: headers);
      var responses = Provinces.fromJson(jsonDecode(response.body));
      for(int x=0; x<responses.provinces.length; x++){
        provinceList[responses.provinces[x].provinceId] = responses.provinces[x].provinceName;
      }
    }catch(Exception){print(Exception);}
  }

  Future getCityList() async {
    try {
      String url = API_ENDPOINT+"/api/v1/dropdown/cities/" +
          _cardBloc.currentUser.value.province.toString();
      Map<String, String> headers = {
        "Content-type": "application/json",
        "apikey": "b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w=="
      };
      final response = await get(url, headers: headers);
      var responses = Cities.fromJson(jsonDecode(response.body));
      for (int x = 0; x < responses.cities.length; x++) {
        cityList[responses.cities[x].cityId] = responses.cities[x].cityName;
      }
    }catch(Exception){print(Exception);}
  }

  saveOffline(NewsLetters newsletters) async {
    await Insert.dbHelper.delete("tbnewsletter");
    for(int x=0; x<newsletters.newsLetters.length; x++){
      Insert.insertNewsletter(newsletters.newsLetters[x].id.toString(),newsletters.newsLetters[x].title,
          newsletters.newsLetters[x].content, newsletters.newsLetters[x].imagePath);
    }
  }

  getNewsletter() async {
    newsletteroffline.clear();
    var result = await dbHelper.selectAllData("tbnewsletter");
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    for(var items in imagelist){
      if(mounted)
      setState(() {
        newsletteroffline.add(new OfflineNewsLetter(int.parse(items["news_id"]), items["title"], items["content"], items["image"]));
      });
    }
  }

  offline(){
    return SizedBox(
        height: (MediaQuery.of(context).size.width * (9/16)),
        child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5.0),
            child:Carousel(
              dotColor: AppColors.violet1,
              indicatorBgPadding: 8.0,
              dotBgColor: Colors.transparent,
              dotIncreasedColor: AppColors.violet1,
              dotSize: 5.0,
              dotSpacing: 20.0,
              autoplay: true,
              autoplayDuration: Duration(seconds: 5),
              boxFit: BoxFit.contain,
              images: newsletteroffline
                  .take(newsletteroffline.length > 5
                  ? 5
                  : newsletteroffline.length)
                  .map((newsletteroffline) => _bannerImage('${newsletteroffline.imagePath}')
              ).toList(),
              onImageTap: (value){
                Navigator.push(
                    context, MaterialPageRoute(
                    builder: (_) {
                      return NewsletterFullScreenPage(
                        title: newsletteroffline[value].title,
                        content: newsletteroffline[value].content,
                        imagepath: newsletteroffline[value].imagePath,
                      );
                    }
                )
                );
              },
            )
        )
    );
  }

  offlinegreet(){
    getName();
    return Container(
        decoration: BoxDecoration(
            color: AppColors.violet3,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withAlpha(40),
                  blurRadius: 10,
                  offset: Offset(0, 5),
                  spreadRadius: 1
              )
            ]
        ),
        width: MediaQuery.of(context).size.width *1.0,
        height: 60,
        child: Center(
          child: Text( DateUtils.getGreetings(DateTime.now()) + ", " +name+"!",
              style: TextStyle(
                  fontFamily: 'Schyler',
                  fontSize: 22,
                  color: AppColors.violet1,
                  fontWeight: FontWeight.bold
              ),
              textAlign: TextAlign.center
          ),
        )
    );
  }

  getName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    name = prefs.getString('username');
  }
}