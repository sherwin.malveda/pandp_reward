import 'package:barcode_flutter/barcode_flutter.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/model/session.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:screen/screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CardPage extends StatefulWidget {
  @override
  _CardPageState createState() => _CardPageState();
}

class _CardPageState extends State<CardPage> {
  AuthBloc _authBloc;
  bool isKeptOn = false;
  double brightness = 1.0;
  String cardType = "";

  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    super.didChangeDependencies();
  }

  @override
  initState() {
    super.initState();
    initPlatformState();
    getData();
  }

  initPlatformState() async {
    bool keptOn = await Screen.isKeptOn;
    brightness = await Screen.brightness;
    setState((){
      isKeptOn = keptOn;
      Screen.setBrightness(1.0);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("E-LOYALTY CARD", style: TextStyle(fontFamily: 'Schyler'),),
        centerTitle: true,
        leading: IconButton(icon:Icon(Icons.arrow_back),
          onPressed:() {
             Screen.setBrightness(brightness);
             Navigator.pop(context, false);          
          }
        ),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            SizedBox(height: 20),
            Text(
              "Your E-Loyalty Card",
              style: TextStyle(
                fontFamily: 'Schyler',
                color: AppColors.violet1, 
                fontSize: 20
              ),
              textAlign: TextAlign.center,
            ),
            Stack(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(horizontal:30.0, vertical: 15.0),
                  child:Card(
                    elevation: 5.0,
                    child:Image.asset("assets/images/loyalty-card.jpg",
                      fit: BoxFit.contain
                    ),
                  )
                ),
                Positioned(
                  bottom: 5,
                  right: 10,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                    child:Text(cardType, style:
                          TextStyle( fontFamily: "Schyler", color: AppColors.violet1,
                              fontSize: 18, fontWeight: FontWeight.bold),
                    )
                  )
                )
              ],
            ),
            StreamBuilder<Session>(
              stream: _authBloc.session,
              builder: (context, snapshot) {
                return StreamHandler(
                  snapshot: snapshot,
                  loading: Center(child: CircularProgressIndicator()),
                  noData: Container(),
                  withError: (error) => Padding(
                    padding: EdgeInsets.all(16),
                    child: Center(
                      child: Text(
                        "Failed to load your card.\n\nPlease check your internet connection and try again!",
                        style: TextStyle(fontFamily: 'Schyler'),
                        textAlign: TextAlign.center,
                      )
                    )
                  ),
                  withData: (Session session) => Padding(
                    padding: EdgeInsets.all(0),
                    child: Column(
                      children: <Widget>[
                        BarCodeImage(
                          data: "${session.cardPan}",
                          // Code string. (required)
                          codeType: BarCodeType.Code39,
                          // Code type (required)
                          lineWidth: MediaQuery.of(context).size.height > 720.0
                              ? 1.3
                              : 1.3,
                          barHeight: 80.0,
                          onError: (error) {
                          },
                        ),
                        Text("${session.cardPan}", 
                          style: TextStyle(
                            fontFamily: 'Schyler',
                            fontSize: 25.0,
                            color: Colors.black
                          ),
                        )
                      ],
                    )
                  ),
                );
              }
            ),
          ],
        ),
      ),
    );
  }

  getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      cardType = prefs.getString('cardType');
    });
  }
}