import 'dart:async';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/db/dbHelper.dart';
import 'package:plainsandprintsloyalty/db/insert.dart';
import 'package:plainsandprintsloyalty/model/offline_data_model/background.dart';
import 'package:plainsandprintsloyalty/model/banners.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/screens/notification_page.dart';
import 'package:plainsandprintsloyalty/widgets/stream_handler.dart';
import 'package:cached_network_image/cached_network_image.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {

  List<ImageBackground> bannerImage = new List<ImageBackground>();
  final dbHelper = DatabaseHelper.instance;
  StoreBloc _storeBloc;
  AuthBloc _authBloc;

  @override
  void initState() {
    super.initState();
    getBackground();
  }


  @override
  void didChangeDependencies() {
    _authBloc = MasterProvider.auth(context);
    _storeBloc = MasterProvider.store(context);
    _storeBloc.getBanners(ListRequest(rows: MAX_ROW_LIST, page: SPLASH_SCREEN_PAGE));
    _storeBloc.getBanner(ListRequest(rows: MAX_ROW_LIST, page: GETTING_STARTED_PAGE));
    loadData();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 1.0,
            width: MediaQuery.of(context).size.height * 1.0,
            child: StreamBuilder<Banners>(
              stream: _storeBloc.banners,
              builder: (context, AsyncSnapshot<Banners> snapshot) {
                return StreamHandler<Banners>(
                  snapshot: snapshot,
                  withData: (Banners banner) {
                    saveOffline(banner);
                    return Carousel(
                      autoplay: true,
                      autoplayDuration: Duration(seconds: 3),
                      boxFit: BoxFit.fitWidth,
                      dotSize: 5.0,
                      dotSpacing: 20.0,
                      dotColor: AppColors.violet1,
                      dotIncreasedColor: AppColors.violet1,
                      indicatorBgPadding: 20.0,
                      dotBgColor: Colors.transparent,
                      borderRadius: false,
                      noRadiusForIndicator: true,
                      overlayShadow: true,
                      overlayShadowColors: Colors.white,
                      overlayShadowSize: 1.0,
                      images: banner.banners
                      .take(banner.banners.length > 2
                        ? 2
                        : banner.banners.length
                      )
                      .map((banner) => _carouselImage(banner.imageUrl)).toList(),
                    );
                  },
                  loading: Container(
                    child: Center(child: CircularProgressIndicator()),
                  ),
                  noData: bannerImage.isEmpty
                    ? Container(color: Colors.white)
                    : offline(),
                  withError: (Object error) =>  bannerImage.isEmpty
                    ? Container(color: Colors.white)
                    : offline()
                );
              }
            ),
          ),
          Positioned(
            bottom: 20.0,
            left: 0.0,
            right: 0.0,
            child: Center(
              child: Container(
                width: 150.0,
                height: 150.0,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logo.png"),
                    fit: BoxFit.contain,
                  ),
                ),
                child: Center(
                  child:Text("",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )
              ),
            ),
          ),
          Positioned(
            bottom: 33.0,
            left: 0.0,
            right: 0.0,
            child: Center(
              child: Container(
                  child: Center(
                    child:Text("1.0.1",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  )
              ),
            ),
          ),
        ],
      )
    );
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 7), onDoneLoading);
  }

  onDoneLoading() async {
    final count = await dbHelper.selectAllData("plainandprints");
    if (count.length == 0) {
      Insert.insertSplashScreen();
      Navigator.of(context).pushReplacementNamed(Routes.GETTING_STARTED);
    } else {
      checkSession(context);
    }
  }

  _carouselImage(String image) {
    if (image.length == 0 || image == "" || image == null) {
      return AssetImage("assets/images/pagebg.jpg");
    } else {
      return CachedNetworkImageProvider(
        image,
      );
    }
  }

  Future checkSession(BuildContext context) async {
    if (await _authBloc.hasSession()) {
      Navigator.of(context).pushReplacementNamed(Routes.DASHBOARD);
    } else {
      Navigator.of(context).pushReplacementNamed(Routes.LANDING);
    }
  }

  saveOffline(Banners banners) async {
    await Insert.dbHelper.deleteImage("tbbanner","1");
    for(int x=0; x<banners.banners.length; x++){
      Insert.insertImage("tbbanner", "1", banners.banners[x].imageUrl);
    }
  }

  getBackground() async {
    bannerImage.clear();
    var result = await dbHelper.selectImage("tbbanner" ,"1");
    List<Map<String, dynamic>> imageList = result;
    final imagelist = imageList;
    for(var items in imagelist){
      if(this.mounted)
        setState(() {
          bannerImage.add(new ImageBackground(int.parse(items["page"]), items["image"]));
        });
    }
  }
  offline(){
    return new Carousel(
      autoplay: true,
      autoplayDuration: Duration(seconds: 3),
      boxFit: BoxFit.fitWidth,
      dotSize: 5.0,
      dotSpacing: 20.0,
      dotColor: AppColors.violet1,
      dotIncreasedColor: AppColors.violet1,
      indicatorBgPadding: 20.0,
      dotBgColor: Colors.transparent,
      borderRadius: false,
      noRadiusForIndicator: true,
      overlayShadow: true,
      overlayShadowColors: Colors.white,
      overlayShadowSize: 1.0,
      images: bannerImage
          .take(bannerImage.length > 2
          ? 2
          : bannerImage.length
      ).map((bannerImage) => _carouselImage(bannerImage.imageUrl)).toList(),
    );
  }
}
