import 'dart:async';
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/master_provider.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/form_validators.dart';
import 'package:plainsandprintsloyalty/model/city.dart';
import 'package:plainsandprintsloyalty/model/item.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';
import 'package:plainsandprintsloyalty/widgets/searchable_list_picker.dart';
import 'package:plainsandprintsloyalty/widgets/selector_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EditProfilePage extends StatefulWidget {
  final User user;

  const EditProfilePage({Key key, @required this.user})
      : super(key: key);
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {

  CardBloc _cardBloc;
  AuthBloc _authBloc;
  Map<int, String> cityList2 = {};

  TextEditingController _mobileNumberController = new TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  
  StreamSubscription _stateSubs;

  Item _city;
  Item _province;

  bool loading = true;
  int city, province;
  String mobilenumber;

  @override
  void initState() {
    getCityList2(widget.user.province);
    mobilenumber = widget.user.mobile;
    city = widget.user.city;
    province = widget.user.province;
    super.initState();
  }
  @override
  void didChangeDependencies() {
    _cardBloc = MasterProvider.card(context);
    _authBloc = MasterProvider.auth(context);
    if (_cardBloc.updateState.hasListener) _stateSubs?.cancel();
    _stateSubs = _cardBloc.updateState.listen(onStateChanged);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.blue2,
      appBar: AppBar(
        backgroundColor: AppColors.violet1,
        title: Text("EDIT ACCOUNT",
          style: TextStyle(fontFamily: 'Schyler'),
        ),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Container(
                      padding: EdgeInsets.symmetric(vertical: 50.0, horizontal: 5.0),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 100,
                                  child: Text("Name:",
                                    style: TextStyle(
                                      fontFamily: 'Schyler',
                                      fontSize: 18.0,
                                      color: AppColors.violet1,
                                      fontWeight: FontWeight.bold
                                    ),
                                  )
                                ),
                                Text(': ${widget.user.firstname} ${widget.user.middleName} ${widget.user.lastName}',
                                  style: TextStyle(
                                    fontFamily: 'Schyler',
                                    fontSize: 15.0,
                                    color: Colors.black26
                                  )
                                )
                              ],
                            ),
                            SizedBox(height: 10.0),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 100,
                                  child:Text("Birthday:",
                                    style: TextStyle(
                                      fontFamily: 'Schyler',
                                      fontSize: 18.0,
                                      color: AppColors.violet1,
                                      fontWeight: FontWeight.bold
                                    ),
                                  )
                                ),
                                Text(': ${DateFormat("MMM dd,yyyy").format(widget.user.birthDate)}',
                                  style: TextStyle(
                                    fontFamily: 'Schyler',
                                    fontSize: 15.0,
                                    color: Colors.black26
                                  )
                                )
                              ],
                            ),
                            SizedBox(height: 10.0,),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 100,
                                  child:Text("Mobile:",
                                    style: TextStyle(
                                      fontFamily: 'Schyler',
                                      fontSize: 18.0,
                                      color: AppColors.violet1,
                                      fontWeight: FontWeight.bold
                                    ),
                                  )
                                ),
                                Expanded(
                                  child:Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(),
                                      borderRadius: BorderRadius.all(Radius.circular(5))
                                    ),
                                    child:Row(
                                      children: <Widget>[
                                        Text("  +63",
                                          style: TextStyle(
                                            fontFamily: 'Schyler',
                                            fontSize: 14.0,
                                            color: Colors.black
                                          ),
                                        ),
                                        Expanded(
                                          child: TextFormField(
                                            validator: _validateNumber,
                                            autovalidate: true,
                                            style:TextStyle(fontFamily: 'Schyler', fontSize: 13),
                                            keyboardType: TextInputType.phone,
                                            maxLength: 10,
                                            controller: _mobileNumberController,
                                            decoration: new InputDecoration(
                                              hintText: widget.user.mobile.toString().substring(3),
                                              counterText: '',
                                              contentPadding: EdgeInsets.symmetric(vertical:5.0, horizontal:0),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: Colors.transparent,
                                                  width: 1.0
                                                ),
                                              ),
                                              enabledBorder: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                  color: Colors.transparent,
                                                  width: 1.0
                                                ),
                                              ),
                                            ),
                                          )
                                        ),
                                      ]
                                    )
                                  )
                                )
                              ]
                            ),
                            SizedBox(height: 10.0),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 100,
                                  child:Text("Email:",
                                    style: TextStyle(
                                      fontFamily: 'Schyler',
                                      fontSize: 18.0,
                                      color: AppColors.violet1,
                                      fontWeight: FontWeight.bold
                                    ),
                                  )
                                ),
                                Text(': ${widget.user.email}',
                                  style: TextStyle(
                                    fontFamily: 'Schyler',
                                    fontSize: 15.0,
                                    color: Colors.black26
                                  )
                                )
                              ],
                            ),
                            SizedBox(height: 10.0),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 100,
                                  child:Text("Province",
                                    style: TextStyle(
                                      fontFamily: 'Schyler',
                                      fontSize: 18.0,
                                      color: AppColors.violet1,
                                      fontWeight: FontWeight.bold
                                    )
                                  )
                                ),
                                Expanded(
                                  child: SelectorField(
                                    child: Text("${_province == null ? '${provinceList[widget.user.province]}' : _province.value}",
                                        style: TextStyle(
                                            fontFamily: 'Schyler',
                                            fontSize: 13.0,
                                            color: Colors.black
                                        )
                                    ),
                                    onPressed: _provincePicker,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 10.0),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 100,
                                  child:Text("City",
                                    style: TextStyle(
                                      fontFamily: 'Schyler',
                                      fontSize: 18.0,
                                      color: AppColors.violet1,
                                      fontWeight: FontWeight.bold
                                    ),
                                  )
                                ),
                                Expanded(
                                  child: SelectorField(
                                    child: Text(
                                      "${_city == null
                                        ? cityList2[widget.user.city] == null
                                          ? 'Please select ..'
                                          : '${cityList2[widget.user.city]}' 
                                        : cityList2[_city?.id] == null
                                          ? 'Please select .. '
                                          : _city.value}",
                                      style: TextStyle(
                                        fontFamily: 'Schyler',
                                        fontSize: 13.0,
                                        color: Colors.black
                                      )
                                    ),
                                    onPressed: _cityPicker,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 30.0),
                            loading ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: 40.0,
                                  decoration: BoxDecoration(
                                    color: AppColors.violet1,
                                    borderRadius: BorderRadius.all(Radius.circular(5.0))
                                  ),
                                  child: FlatButton(
                                    onPressed: () {
                                      _onSubmit(widget.user.firstname,widget.user.middleName,widget.user.lastName,widget.user.birthDate,
                                          widget.user.username, widget.user.cardDigits, widget.user.maritalStatus,widget.user.title,
                                          widget.user.gender, widget.user.zipCode, widget.user.address, widget.user.email
                                      );
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 50),
                                      child: Text("SAVE",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'Schyler',
                                          letterSpacing: 3.0,
                                          fontWeight: FontWeight.w600
                                        )
                                      )
                                    ),
                                  ),
                                )
                              ],
                            )
                            : Container(
                              child: Center(child: CircularProgressIndicator(),)
                            )
                          ]
                        ),
                      )
                    )
            ],
          ),
        ),
      ),
    );
  }

  List<Item> _getCity() =>
    cityList2.keys.map((key) => Item(id: key, value: cityList2[key])).toList();

  List<Item> _getProvince() => provinceList.keys
    .map((key) => Item(id: key, value: provinceList[key]))
    .toList();

  void _cityPicker() async {
    final Item item = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>SearchableListPicker(title: "City", items: _getCity())));
    if (item != null)
      setState(() {
        _city = item;
      });
  }

  void _provincePicker() async {
    final Item item = await Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => SearchableListPicker(title: "Province", items: _getProvince())));
    if (item != null)
      setState(() {
        _province = item;
        getCityList2(_province?.id);
      });
  }

  String _validateNumber(String value) =>
    FormValidators.isMobileNumber(value, "Enter a valid mobile number");

  void _onSubmit(String fname, String mname, String lname, DateTime birthday,
    String uname,String carddigits, int maritalStatus,
    int title, int gender, String zipCode, String address, String email) {

    String mobileNumber = _mobileNumberController.text.isNotEmpty
      ? "+63${_mobileNumberController.text}"
      : "";
    int cities = _city?.id;
    int provinces = _province?.id;

    if(mobileNumber == ""){
      mobileNumber = mobilenumber;
    }

    if(provinces == null){
      provinces = province;
    }

    if(provinces == province){
      if(cities == null) {
        cities = city;
      }
    }else if(cities == null || cityList2[_city?.id] == null){
      _showErrorSnackBox("Please check your city");
      return;
    }

    final User user = User(
      address: address,
      zipCode: zipCode,
      email: email,
      gender: gender,
      title: title,
      maritalStatus: maritalStatus,
      cardDigits: carddigits,
      firstname: fname,
      middleName: mname,
      lastName: lname,
      birthDate: birthday,
      mobile: mobileNumber,
      username: uname,
      city: cities,
      province: provinces,
    );

    if (mobileNumber.length < 13 &&  mobileNumber.length > 3) {
      _showErrorSnackBox("Fill up all the required fields");
      return;
    }

    if (_formKey.currentState.validate()) {
      savedUser(mobileNumber.toString(), provinceList[provinces], cityList2[cities]);
      _cardBloc.updateAccount(_authBloc.session.value.accountId,user);
    }
  }

  void onStateChanged(ScreenState state) {
    switch (state.state) {
      case States.DONE:
        Navigator.pop(context);
        loading = true;
        break;
      case States.ERROR:
        _showErrorSnackBox(state.error.toString());
        loading = true;
        break;
      case States.IDLE:
        loading = true;
        break;
      case States.WAITING:
        loading = false;
        break;
    }
  }

  void _showErrorSnackBox(String errorMessage) {
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.GROUNDED,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      backgroundColor: AppColors.violet1,
      icon: Icon(
        Icons.error,
        color:Colors.redAccent,
      ),
      messageText: Text(
        "$errorMessage",
        style: TextStyle(
          fontSize: 15.0, 
          color: Colors.white,
          fontFamily: "Schyler"),
      ),
      duration:  Duration(seconds: 3),              
    )..show(context);
  }

  Future getCityList2(int provinceId) async {
    String url = API_ENDPOINT+"/api/v1/dropdown/cities/"+provinceId.toString();
    Map<String, String> headers = {"Content-type": "application/json",
      "apikey":"b9SMpvj5HohfWRp4JF2UnvDtSRdLGnFUOgMeehjyr61d6yE0OR8uR4M862EMg3ovjHyejAOWhstIDaLaKYjg4w=="};
    final response = await get(url, headers: headers);
    var responses = Cities.fromJson(jsonDecode(response.body));
    cityList2.clear();
    for(int x=0; x<responses.cities.length; x++){
      setState(() {
        cityList2[responses.cities[x].cityId] = responses.cities[x].cityName;
      });
    }
  }

  savedUser(String mobile, String province, String city) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('mobile', mobile);
    await prefs.setString('province', province);
    await prefs.setString('city', city);
  }
}