import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:plainsandprintsloyalty/app_colors.dart';
import 'package:plainsandprintsloyalty/model/request/member_request.dart';
import 'package:plainsandprintsloyalty/routes.dart';
import 'package:plainsandprintsloyalty/widgets/selector_field.dart';

import 'arguments/registration_arguments.dart';

class ExistingMemberPage extends StatefulWidget {
  @override
  _ExistingMemberPageState createState() => _ExistingMemberPageState();
}

class _ExistingMemberPageState extends State<ExistingMemberPage> {

  FocusNode unused = new FocusNode();
  TextEditingController cusid = new TextEditingController();
  bool visible = true;
  DateTime _birthday;
  Color cursor = Colors.black;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: 
        visible ? Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * .2,
                  ),
                  Text("Enter your Customer ID",
                    style: TextStyle(
                      fontFamily: 'Schyler',
                      color: AppColors.violet1,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Center(
                    child: Container(
                      width: 230,
                      child: TextField(
                        controller: cusid,
                        maxLength: 6,
                        keyboardType: TextInputType.phone,
                        cursorColor: cursor,
                        style: TextStyle(
                          color: AppColors.violet1,
                          fontFamily: 'Schyler',
                          fontSize: 25.0,
                          letterSpacing: 23.5
                        ),
                        decoration: new InputDecoration(
                          counterText: '', border: InputBorder.none
                        ),
                        onChanged: (value) {
                          setState(() {
                            next();  
                          });
                          if (!_isNumber(value)) {
                            cusid.text = cusid.text.substring(0, cusid.text.length - 1);
                            cusid.clear();
                          }
                          checkCount();
                        },
                      )
                    )
                  ),
                  Center(
                    child: Container(
                      width: 240,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            color: AppColors.violet1,
                            height: 2.0,
                            width: 25.0,
                          ),
                          Container(
                            color: AppColors.violet1,
                            height: 2.0,
                            width: 25.0,
                          ),
                          Container(
                            color: AppColors.violet1,
                            height: 2.0,
                            width: 25.0,
                          ),
                          Container(
                            color: AppColors.violet1,
                            height: 2.0,
                            width: 25.0,
                          ),
                          Container(
                            color: AppColors.violet1,
                            height: 2.0,
                            width: 25.0,
                          ),
                          Container(
                            color: AppColors.violet1,
                            height: 2.0,
                            width: 25.0,
                          ),
                          Container(
                            color: AppColors.violet1,
                            height: 2.0,
                            width: 0.0,
                          )
                        ],
                      ),
                    )
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * .1,),
                  next(),      
                ],
              ),
            ),
            Center (
              child :Container(
                width: 150,
                height: 150,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logo.png"),
                    fit: BoxFit.contain,
                  ),
                ),
              )
            ),
          ]
        )
        : Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height * .2,),
                  Text("Select your Birthday",
                    style: TextStyle(
                      fontFamily: 'Schyler',
                      color: AppColors.violet1,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.center
                  ),
                  SizedBox(height: 30.0,),
                  Center(
                    child: Container(
                      width: 250,
                      height: 40.0,
                      child: SelectorField(
                        child: Text(
                          "${_birthday == null ? "Birthday" : DateFormat('yyyy-MM-dd').format(_birthday)}",
                          style: TextStyle(
                            fontSize: 17.0,
                            fontFamily: 'Schyler',
                            color: _birthday == null
                              ? Theme.of(context).hintColor
                              : Colors.black
                          ),
                        ),
                        onPressed:() {
                          _showDateDialog(DateTime.parse('2000-06-15'));
                        }
                      ),
                    )
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * .1),
                  next2(),
                ],
              ),
            ),
            Center (
              child :Container(
                width: 150,
                height: 150,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logo.png"),
                    fit: BoxFit.contain,
                  ),
                ),
              )
            )
          ]
        )
    );
  }

  void _changed(bool visibility, String field) {
    setState(() {
      if (field == "show"){
        visible = visibility;
      }
    });
  }

  Widget next() {
    if (cusid.text.length < 6) {
      cursor = Colors.black;
      return Container(
        height: 30.0,
        width: 120.0,
        child: Center(
          child: Text("Next",
            style: TextStyle(
              fontFamily: 'Schyler',
              color: Colors.black12,
              fontSize: 15.0,
              letterSpacing: 2.0
            ),
          )
        )
      );
    } else {
      cursor = Colors.white;
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 100),
        height: 30.0,
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(5.0),
          color: AppColors.violet1,
          child: MaterialButton(
            child: Text("Next",
              style: TextStyle(
                fontFamily: 'Schyler',
                color: Colors.white,
                fontSize: 15.0,
                letterSpacing: 2.0
              ),
            ),
            onPressed: () => _changed(false, "show"),
          )
        )
      );
    }
  }

  Widget next2() {
    if (_birthday.toString().length < 8) {
      return Container(
        height: 30.0,
        width: 120.0,
        child: Center(
          child: Text("Next",
            style: TextStyle(
              fontFamily: 'Schyler',
              color: Colors.black12,
              fontSize: 15.0,
              letterSpacing: 2.0
            ),
          )
        )
      );
    } else {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 100),
        height: 30.0,
        child: Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(5.0),
          color: AppColors.violet1,
          child: MaterialButton(
            child: Text("Next",
              style: TextStyle(
                fontFamily: 'Schyler',
                color: Colors.white,
                fontSize: 15.0,
                letterSpacing: 2.0
              ),
            ),
            onPressed: () => _navigateToRegistration(true, cusid.text, _birthday.toString())
          ),
        )
      );
    }
  }

  checkCount(){
    if(cusid.text.length == 6){
      FocusScope.of(context).requestFocus(FocusNode());
    }
  }

  void _showDateDialog(DateTime initDateTime) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.all(10),
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
              side: BorderSide(color: Colors.white)
          ),
          content: Container(
            height: 280,
            child: DatePickerWidget(
              minDateTime: DateTime.parse(DateTime.now().subtract(Duration(days: 36500)).toString()),
              maxDateTime: DateTime.parse(DateTime.now().subtract(Duration(days: 6570)).toString()),
              initialDateTime: initDateTime,
              dateFormat: "MMM dd,yyyy",
              pickerTheme: DateTimePickerTheme(
                backgroundColor: Colors.white,
                cancelTextStyle: TextStyle(color: Colors.red),
                confirmTextStyle: TextStyle(color: AppColors.violet1),
                itemTextStyle: TextStyle(color: AppColors.violet1, fontSize: 16.0),
                pickerHeight: 250.0,
                titleHeight: 25.0,
                itemHeight: 30.0,
              ),
              onConfirm: (dateTime, selectedIndex) {
                setState(() {
                  _birthday = dateTime;
                });
              },
            ),
          ),
        );
      },
    );
  }
  
  bool _isNumber(String value) {
    if (value == null) {
      return true;
    }
    final n = num.tryParse(value);
    return n != null;
  }

  void _navigateToRegistration(bool isExisting, String cusid, String _birthday) {
  final MemberRequest request = MemberRequest(
    carddigits : cusid,
    birthday : _birthday);
    RegistrationArguments args = RegistrationArguments(isExisting,cusid, request);
    Navigator.of(context).pushReplacementNamed(Routes.REGISTRATION, arguments: args);
  }

}
