import 'package:plainsandprintsloyalty/model/empty_checkable.dart';
import 'package:plainsandprintsloyalty/model/transaction.dart';

class History with EmptyCheckable {
  final int totalTransactions;
  final List<Transaction> transactions;

  History({
    this.totalTransactions, 
    this.transactions
  });

  factory History.fromMap(Map<String, dynamic> map) {
    final int totalTransactions = map['total_transaction'] ?? 0;
    final List<Transaction> transactions = map['transaction'] != null
      ? List.from(map['transaction'])
        .map((transaction) => Transaction.fromMap(transaction))
        .toList()
      : [];

    return History(
      totalTransactions: totalTransactions,
      transactions: transactions
    );
  }

  @override
  bool operator == (Object other) =>
    identical(this, other) ||
      other is History &&
        runtimeType == other.runtimeType &&
        totalTransactions == other.totalTransactions &&
        transactions == other.transactions;

  @override
  int get hashCode =>
      totalTransactions.hashCode ^
      transactions.hashCode;

  @override
  bool isEmpty() => transactions == null || transactions.isEmpty;
}