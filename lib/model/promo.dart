class Promo {
  final int promoId;
  final String promoName;
  final String promoDescription;
  final String promoCategory;
  final String promoImage;

  Promo({
    this.promoId,
    this.promoName,
    this.promoDescription,
    this.promoCategory,
    this.promoImage
  });

  factory Promo.fromMap(Map<String, dynamic> map) {
    final int promoId = map['promo_id'] ?? -1;
    final String promoName = map['promo_name'] ?? "";
    final String promoDescription = map['promo_desc'] ?? "";
    final String promoCategory = map['promo_categ'] ?? "";
    final String promoImage = map['promo_img'] ?? "";

    return Promo(
      promoId: promoId,
      promoName: promoName,
      promoDescription: promoDescription,
      promoCategory: promoCategory,
      promoImage: promoImage
    );
  }
}
