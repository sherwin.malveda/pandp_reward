class Point {
  final String bucketName;
  final double totalAvailable;
  final double totalQueue;
  final double totalEarned;
  final double totalRedeemed;
  final double totalExpired;

  Point({
    this.bucketName,
    this.totalAvailable,
    this.totalQueue,
    this.totalEarned,
    this.totalRedeemed,
    this.totalExpired
  });

  factory Point.fromMap(Map<String, dynamic> map) {
    final String bucketName = map['bucket_name'] ?? "";
    final double totalAvailable = map['total_available'] ?? 0.0;
    final double totalQueue = map['total_queue'] ?? 0.0;
    final double totalEarned = map['total_earned'] ?? 0.0;
    final double totalRedeemed = map['total_redeemed'] ?? 0.0;
    final double totalExpired = map['total_expired'] ?? 0.0;

    return Point(
      bucketName: bucketName,
      totalAvailable: totalAvailable,
      totalQueue: totalQueue,
      totalEarned: totalEarned,
      totalRedeemed: totalRedeemed,
      totalExpired: totalExpired
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Point &&
        runtimeType == other.runtimeType &&
        bucketName == other.bucketName &&
        totalAvailable == other.totalAvailable &&
        totalQueue == other.totalQueue &&
        totalEarned == other.totalEarned &&
        totalRedeemed == other.totalRedeemed &&
        totalExpired == other.totalExpired;

  @override
  int get hashCode =>
    bucketName.hashCode ^
    totalAvailable.hashCode ^
    totalQueue.hashCode ^
    totalEarned.hashCode ^
    totalRedeemed.hashCode ^
    totalExpired.hashCode;
}
