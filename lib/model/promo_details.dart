import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

class PromoDetails with EmptyCheckable{
  final int promoId;
  final String promoName;
  final String promoDescription;
  final String promoCategory;
  final String promoImage;

  PromoDetails({
    this.promoId,
    this.promoName,
    this.promoDescription,
    this.promoCategory,
    this.promoImage
  });

  factory PromoDetails.fromMap(Map<String, dynamic> map) {
    final int promoId = map['promo_id'] ?? -1;
    final String promoName = map['promo_name'] ?? "";
    final String promoDescription = map['promo_desc'] ?? "";
    final String promoCategory = map['promo_categ'] ?? "";
    final String promoImage = map['promo_img'] ?? "";

    return PromoDetails(
      promoId: promoId,
      promoName: promoName,
      promoDescription: promoDescription,
      promoCategory: promoCategory,
      promoImage: promoImage
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is PromoDetails &&
        runtimeType == other.runtimeType &&
        promoId == other.promoId &&
        promoName == other.promoName &&
        promoDescription == other.promoDescription &&
        promoCategory == other.promoCategory &&
        promoImage == other.promoImage ;

  @override
  int get hashCode =>
    promoId.hashCode ^
    promoName.hashCode ^
    promoDescription.hashCode ^
    promoCategory.hashCode ^
    promoImage.hashCode;

  @override
  bool isEmpty() => false;
}
