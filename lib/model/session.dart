import 'package:plainsandprintsloyalty/dao/mao_dao.dart';
import 'package:plainsandprintsloyalty/dao/map_persistable.dart';
import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

class Session with MapPersistable<Session>, EmptyCheckable {
  static const String KEY_ACCOUNT_ID = 'account_id';
  static const String KEY_TOKEN = 'access_token';
  static const String KEY_TOKEN_TYPE = 'token_type';
  static const String KEY_EXPIRY = 'expiry';
  static const String KEY_REFRESH_TOKEN = 'refresh_token';
  static const String KEY_GRANT_TYPE = 'grant_type';
  static const String KEY_CARD_PAN = 'card_pan';
  static const String KEY_VALIDITY = 'validity';

  final int accountId;
  final String token;
  final String tokenType;
  final DateTime expiry;
  final String refreshToken;
  final String grantType;
  final DateTime validity;
  final String cardPan;

  Session({
    this.accountId,
    this.token,
    this.tokenType,
    this.expiry,
    this.refreshToken,
    this.grantType,
    this.validity,
    this.cardPan
  });

  factory Session.fromMap(Map<String, dynamic> map) {
    final int accountId = map[KEY_ACCOUNT_ID] ?? -1;
    final String token = map[KEY_TOKEN] ?? "";
    final String tokenType = map[KEY_TOKEN_TYPE] ?? "";
    final DateTime expiry = DateTime.tryParse(map[KEY_EXPIRY] ?? "") ?? DateTime.now();
    final String refreshToken = map[KEY_REFRESH_TOKEN] ?? "";
    final String grantType = map[KEY_GRANT_TYPE] ?? "";
    final DateTime validity = DateTime.tryParse(map[KEY_VALIDITY] ?? "") ?? DateTime.now();
    final String cardPan = map[KEY_CARD_PAN] ?? "";
    
    return Session(
      accountId: accountId,
      token: token,
      refreshToken: refreshToken,
      grantType: grantType,
      validity: validity,
      tokenType: tokenType,
      expiry: expiry,
      cardPan: cardPan
    );
  }

  factory Session.fromDao(MapDao dao) {
    final int accountId = dao.getInt(KEY_ACCOUNT_ID);
    final String token = dao.getString(KEY_TOKEN);
    final String tokenType = dao.getString(KEY_TOKEN_TYPE);
    final String expiry = dao.getString(KEY_EXPIRY);
    final String refreshToken = dao.getString(KEY_REFRESH_TOKEN);
    final String grantType = dao.getString(KEY_GRANT_TYPE);
    final String validity = dao.getString(KEY_VALIDITY);
    final String cardPan = dao.getString(KEY_CARD_PAN);

    return Session(
      accountId: accountId,
      token: token,
      tokenType: tokenType,
      expiry: DateTime.tryParse(expiry ?? ""),
      refreshToken: refreshToken,
      grantType: grantType,
      validity: DateTime.tryParse(validity ?? ""),
      cardPan: cardPan
    );
  }

  @override
  void saveMap(MapDao dao) async {
    dao.setInt(KEY_ACCOUNT_ID, accountId);
    dao.setString(KEY_TOKEN, token);
    dao.setString(KEY_TOKEN_TYPE, tokenType);
    dao.setString(KEY_EXPIRY, expiry.toString());
    dao.setString(KEY_REFRESH_TOKEN, refreshToken);
    dao.setString(KEY_GRANT_TYPE, grantType);
    dao.setString(KEY_VALIDITY, validity.toString());
    dao.setString(KEY_CARD_PAN, cardPan);
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Session &&
        runtimeType == other.runtimeType &&
        accountId == other.accountId &&
        token == other.token &&
        tokenType == other.tokenType &&
        expiry == other.expiry &&
        refreshToken == other.refreshToken &&
        grantType == other.grantType &&
        validity == other.validity &&
        cardPan == other.cardPan;

  @override
  int get hashCode =>
    accountId.hashCode ^
    token.hashCode ^
    tokenType.hashCode ^
    expiry.hashCode ^
    refreshToken.hashCode ^
    grantType.hashCode ^
    validity.hashCode ^
    cardPan.hashCode;

  @override
  bool isEmpty() => false;
}
