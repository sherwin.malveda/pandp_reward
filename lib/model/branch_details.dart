import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

class BranchDetails with EmptyCheckable {
  final String branchName;
  final String branchCode;
  final String branchAddress;
  final String longitude;
  final String latitude;
  final String email;
  final String landlineNumber;
  final String mobileNumber;

  BranchDetails({
    this.branchName,
    this.branchCode,
    this.branchAddress,
    this.longitude,
    this.latitude,
    this.email,
    this.landlineNumber,
    this.mobileNumber
  });

  factory BranchDetails.fromMap(Map<String, dynamic> map) {
    final String branchName = map['branch_name'] ?? "";
    final String branchCode = map['branch_code'] ?? "";
    final String branchAddress = map['branch_address'] ?? "";
    final String longitude = map['longitude'] ?? "";
    final String latitude = map['latitude'] ?? "";
    final String email = map['email'] ?? "";
    final String landlineNumber = map['landline_no'] ?? "";
    final String mobileNumber = map['mobile_no'] ?? "";

    return BranchDetails(
      branchName: branchName,
      branchCode: branchCode,
      branchAddress: branchAddress,
      longitude: longitude,
      latitude: latitude,
      email: email,
      landlineNumber: landlineNumber,
      mobileNumber: mobileNumber
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is BranchDetails &&
        runtimeType == other.runtimeType &&
        branchName == other.branchName &&
        branchCode == other.branchCode &&
        branchAddress == other.branchAddress &&
        longitude == other.longitude &&
        latitude == other.latitude &&
        email == other.email &&
        landlineNumber == other.landlineNumber &&
        mobileNumber == other.mobileNumber;

  @override
  int get hashCode =>
    branchName.hashCode ^
    branchCode.hashCode ^
    branchAddress.hashCode ^
    longitude.hashCode ^
    latitude.hashCode ^
    email.hashCode ^
    landlineNumber.hashCode ^
    mobileNumber.hashCode;

  @override
  bool isEmpty() => false;
}
