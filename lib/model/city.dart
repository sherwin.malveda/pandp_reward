import 'package:plainsandprintsloyalty/model/cities.dart';

class Cities {
  final List<City> cities;

  Cities({
    this.cities
  });

  factory Cities.fromJson(List<dynamic> parsedJson) {

    List<City> cities = new List<City>();
    cities = parsedJson.map((i)=>City.fromMap(i)).toList();

    return Cities(
        cities: cities
    );
  }
}
