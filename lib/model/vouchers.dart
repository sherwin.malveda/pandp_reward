import 'package:plainsandprintsloyalty/model/empty_checkable.dart';
import 'package:plainsandprintsloyalty/model/voucher.dart';

class Vouchers with EmptyCheckable {
  final int totalVoucher;
  final List<Voucher> vouchers;

  Vouchers({
    this.totalVoucher,
    this.vouchers
  });

  factory Vouchers.fromMap(Map<String, dynamic> map) {
    final int totalVoucher = map['total_voucher'] ?? 0;
    final List<Voucher> vouchers = map['voucher'] != null
      ? List.from(map['voucher'])
        .map((transaction) => Voucher.fromMap(transaction))
        .toList()
      : [];

    return Vouchers(
      totalVoucher: totalVoucher,
      vouchers: vouchers
    );
  }

  @override
  bool isEmpty() => vouchers == null || vouchers.isEmpty;
}