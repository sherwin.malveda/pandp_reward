class OfflineBranch {
  final String branchId;
  final String branchName;
  final String branchCode;
  final String branchAddress;

  OfflineBranch(
    this.branchId,
    this.branchName,
    this.branchCode,
    this.branchAddress
  );
}
