class VoucherOffline{
  final String voucherCode;
  final String description;
  final String status;
  final String expiration;

  VoucherOffline(
    this.voucherCode, 
    this.description, 
    this.status, 
    this.expiration
  );
}