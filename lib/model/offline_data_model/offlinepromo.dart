class OfflinePromo {
  final String promoId;
  final String promoName;
  final String promoDescription;
  final String promoImage;

  OfflinePromo(
    this.promoId,
    this.promoName,
    this.promoDescription,
    this.promoImage);
}
