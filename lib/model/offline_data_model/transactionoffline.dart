class TransactionOffline {
  final String branch;
  final String invoiceNumber;
  final String transactionType;
  final String storeLocation;
  final double endBalance;
  final double earnedPoints;
  final double transactionAmount;
  final double redeemedPoints;
  final int transactionStatus;
  final DateTime transactionDate;

  TransactionOffline(
    this.branch,
    this.invoiceNumber,
    this.transactionType,
    this.storeLocation,
    this.endBalance,
    this.earnedPoints,
    this.transactionAmount,
    this.redeemedPoints,
    this.transactionStatus,
    this.transactionDate);
}
