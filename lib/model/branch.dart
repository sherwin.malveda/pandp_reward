class Branch {
  final int branchId;
  final String branchName;
  final String branchCode;
  final String branchAddress;

  Branch({
    this.branchId,
    this.branchName,
    this.branchCode,
    this.branchAddress
  });

  factory Branch.fromMap(Map<String, dynamic> map) {
    final int branchId = map['branch_id'] ?? -1;
    final String branchName = map['branch_name'] ?? "";
    final String branchCode = map['branch_code'] ?? "";
    final String branchAddress = map['branch_address'] ?? "";

    return Branch(
      branchId: branchId,
      branchName: branchName,
      branchCode: branchCode,
      branchAddress: branchAddress
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Branch &&
        runtimeType == other.runtimeType &&
        branchId == other.branchId &&
        branchName == other.branchName &&
        branchCode == other.branchCode &&
        branchAddress == other.branchAddress;

  @override
  int get hashCode =>
    branchId.hashCode ^
    branchName.hashCode ^
    branchCode.hashCode ^
    branchAddress.hashCode;
}
