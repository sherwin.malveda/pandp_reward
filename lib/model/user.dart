import 'package:intl/intl.dart';
import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

class User with EmptyCheckable {
  final String firstname;
  final String middleName;
  final String lastName;
  final DateTime birthDate;
  final String email;
  final String username;
  final String password;
  final String mobile;
  final String address;
  final String zipCode;
  final String cardDigits;
  final int title;
  final int gender;
  final int maritalStatus;
  final int province;
  final int city;
  final int cardFormat;
  final bool subscription;
  final String cardType;

  User({
    this.firstname,
    this.middleName = "",
    this.lastName,
    this.birthDate,
    this.title = 0,
    this.gender = 0,
    this.maritalStatus = 0,
    this.email,
    this.username,
    this.password,
    this.mobile,
    this.address = "",
    this.city,
    this.province,
    this.zipCode,
    this.cardDigits = "",
    this.cardFormat = 1,
    this.subscription = false,
    this.cardType = ""
  });

  Map<String, dynamic> toMap() => {
    "first_name": firstname,
    "middle_name": middleName,
    "last_name": lastName,
    "birth_date": DateFormat("yyyy-MM-dd").format(birthDate),
    "title": title,
    "gender": gender,
    "marital_status": maritalStatus,
    "email": email,
    "username": username,
    "password": password,
    "mobile_no": mobile,
    "address_line": address,
    "city": city,
    "province": province,
    "country": 1,
    "zip_code": zipCode,
    "card_digits": cardDigits,
    "cardFormat": 1,
    "email_notif": subscription,
    "card_category": cardType,
  };

  factory User.fromMap(Map<String, dynamic> map) {
    final String firstName = map['first_name'] ?? "";
    final String middleName = map['middle_name'] ?? "";
    final String lastName = map['last_name'] ?? "";
    final DateTime birthDate = DateTime.tryParse(map['birth_date'] ?? "");
    final String email = map['email'] ?? "";
    final int title = map['title'] ?? 0;
    final String username = map['username'] ?? "";
    final String password = map['password'] ?? "";
    final String mobile = map['mobile_no'] ?? "";
    final String address = map['address'] ?? "";
    final String zipCode = map['zip_code'] ?? "";
    final String cardDigits = map['card_digits'] ?? "";
    final int gender = map['gender'] ?? 0;
    final int maritalStatus = map['marital_status'] ?? 0;
    final int province = map['province'] ?? -1;
    final int city = map['city'] ?? -1;
    final int cardFormat = map['card_format'] ?? -1;
    final bool subscription = map['email_notif'] ?? false;
    final String cardType = map['card_category'] ?? "";

    return User(
      firstname: firstName,
      middleName: middleName,
      lastName: lastName,
      birthDate: birthDate,
      title: title,
      email: email,
      username: username,
      password: password,
      mobile: mobile,
      address: address,
      zipCode: zipCode,
      cardDigits: cardDigits,
      gender: gender,
      maritalStatus: maritalStatus,
      province: province,
      city: city,
      cardFormat: cardFormat, 
      subscription: subscription,
      cardType: cardType,
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is User &&
        runtimeType == other.runtimeType &&
        firstname == other.firstname &&
        middleName == other.middleName &&
        lastName == other.lastName &&
        birthDate == other.birthDate &&
        email == other.email &&
        username == other.username &&
        password == other.password &&
        mobile == other.mobile &&
        address == other.address &&
        zipCode == other.zipCode &&
        cardDigits == other.cardDigits &&
        title == other.title &&
        gender == other.gender &&
        maritalStatus == other.maritalStatus &&
        province == other.province &&
        city == other.city &&
        cardFormat == other.cardFormat &&
        subscription == other.subscription &&
        cardType == other.cardType;

  @override
  int get hashCode =>
    firstname.hashCode ^
    middleName.hashCode ^
    lastName.hashCode ^
    birthDate.hashCode ^
    email.hashCode ^
    username.hashCode ^
    password.hashCode ^
    mobile.hashCode ^
    address.hashCode ^
    zipCode.hashCode ^
    cardDigits.hashCode ^
    title.hashCode ^
    gender.hashCode ^
    maritalStatus.hashCode ^
    province.hashCode ^
    city.hashCode ^
    cardFormat.hashCode ^
    subscription.hashCode ^
    cardType.hashCode;

  bool hasNoNullsForRequiredField() {
    return ((firstname != null && firstname.isNotEmpty) &&
      (lastName != null && lastName.isNotEmpty) &&
      (email != null && email.isNotEmpty) &&
      (username != null && username.isNotEmpty) &&
      (password != null && password.isNotEmpty) &&
      (zipCode != null && zipCode.isNotEmpty) &&
      (mobile != null && mobile.isNotEmpty) &&
      birthDate != null &&
      province != null &&
      city != null
    );
  }

  @override
  bool isEmpty() => false;
}