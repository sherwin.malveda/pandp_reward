class Transaction {
  final String transactionId;
  final String branch;
  final String invoiceNumber;
  final String cardNumber;
  final String transactionType;
  final String storeLocation;
  final double endBalance;
  final double earnedPoints;
  final double transactionAmount;
  final double redeemedPoints;
  final int transactionStatus;
  final DateTime transactionDate;

  Transaction({
    this.transactionId,
    this.branch,
    this.invoiceNumber,
    this.cardNumber,
    this.transactionType,
    this.earnedPoints,
    this.transactionAmount,
    this.redeemedPoints,
    this.transactionStatus,
    this.storeLocation,
    this.endBalance,
    this.transactionDate});

  factory Transaction.fromMap(Map<String, dynamic> map) {
    final String transactionId = map['txn_id'] ?? "";
    final String branch = map['branch'] ?? "";
    final String invoiceNumber = map['invoice_no'] ?? "";
    final String cardNumber = map['card_no'] ?? "";
    final String storeLocation = map['store_location'] ?? "";
    final double endBalance = map['end_balance'] ?? 0.0;
    final double earnedPoints = map['earned_points'] ?? 0.0;
    final String transactionType = map['txn_type'] ?? "";
    final double redeemedPoints = map['redeemed_points'] ?? 0.0;
    final double transactionAmount = map['txn_amount'] ?? 0.0;
    final int transactionStatus = map['txn_status'] ?? -1;
    final DateTime transactionDate = DateTime.tryParse(map['txn_date'] ?? "");

    return Transaction(
      transactionId: transactionId,
      branch: branch,
      storeLocation: storeLocation,
      endBalance: endBalance,
      invoiceNumber: invoiceNumber,
      cardNumber: cardNumber,
      earnedPoints: earnedPoints,
      transactionType: transactionType,
      redeemedPoints: redeemedPoints,
      transactionAmount: transactionAmount,
      transactionStatus: transactionStatus,
      transactionDate: transactionDate
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Transaction &&
        runtimeType == other.runtimeType &&
        transactionId == other.transactionId &&
        branch == other.branch &&
        invoiceNumber == other.invoiceNumber &&
        cardNumber == other.cardNumber &&
        transactionType == other.transactionType &&
        earnedPoints == other.earnedPoints &&
        transactionAmount == other.transactionAmount &&
        redeemedPoints == other.redeemedPoints &&
        transactionStatus == other.transactionStatus &&
        transactionDate == other.transactionDate;

  @override
  int get hashCode =>
      transactionId.hashCode ^
      branch.hashCode ^
      invoiceNumber.hashCode ^
      cardNumber.hashCode ^
      transactionType.hashCode ^
      earnedPoints.hashCode ^
      transactionAmount.hashCode ^
      redeemedPoints.hashCode ^
      transactionStatus.hashCode ^
      transactionDate.hashCode;



}
