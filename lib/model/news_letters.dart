import 'package:collection/collection.dart';
import 'package:plainsandprintsloyalty/model/empty_checkable.dart';
import 'package:plainsandprintsloyalty/model/news_letter.dart';

class NewsLetters with EmptyCheckable {
  final int totalNewsLetters;
  final List<NewsLetter> newsLetters;

  NewsLetters({
    this.totalNewsLetters,
    this.newsLetters
  });

  factory NewsLetters.fromMap(Map<String, dynamic> map) {
    final int totalNewsLetters = map['total_newsletter'] ?? 0;
    final List<NewsLetter> newsLetters = map['newsletter'] != null
      ? List.from(map['newsletter'])
        .map((newsletter) => NewsLetter.fromMap(newsletter))
        .toList()
      : [];

    return NewsLetters(
      totalNewsLetters: totalNewsLetters,
      newsLetters: newsLetters
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is NewsLetters &&
        runtimeType == other.runtimeType &&
        totalNewsLetters == other.totalNewsLetters &&
        ListEquality().equals(newsLetters, other.newsLetters);

  @override
  int get hashCode => 
    totalNewsLetters.hashCode ^ 
    newsLetters.hashCode;

  @override
  bool isEmpty() => newsLetters == null || newsLetters.isEmpty;
}
