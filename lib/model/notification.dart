class NotificationModel {
  final int messageId;
  final String messageTitle;
  final String messageBody;
  final String messageContent;
  final String messageImage;
  final String messageIcon;

  NotificationModel({
    this.messageId,
    this.messageTitle,
    this.messageBody,
    this.messageContent,
    this.messageImage,
    this.messageIcon
  });

  factory NotificationModel.fromMap(Map<String, dynamic> map) {
    final int messageId = map['message_id'] ?? -1;
    final String messageTitle = map['message_title'] ?? "";
    final String messageBody = map['message_body'] ?? "";
    final String messageContent = map['message_content'] ?? "";
    final String messageImage = map['message_image'] ?? "";
    final String messageIcon = map['message_icon'] ?? "";

    return NotificationModel(
        messageId: messageId,
        messageTitle: messageTitle,
        messageBody: messageBody,
        messageContent: messageContent,
        messageImage: messageImage,
        messageIcon: messageIcon
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is NotificationModel &&
              runtimeType == other.runtimeType &&
              messageId == other.messageId &&
              messageTitle == other.messageTitle &&
              messageBody == other.messageBody &&
              messageContent == other.messageContent&&
              messageImage == other.messageImage&&
              messageIcon == other.messageIcon;

  @override
  int get hashCode =>
      messageId.hashCode ^
      messageTitle.hashCode ^
      messageBody.hashCode ^
      messageContent.hashCode^
      messageImage.hashCode^
      messageIcon.hashCode;
}
