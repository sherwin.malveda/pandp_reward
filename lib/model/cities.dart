class City {
  final int cityId;
  final String cityName;

  City({
    this.cityId,
    this.cityName
  });

  factory City.fromMap(Map<String, dynamic> map) {
    final int cityId = map['city_id'] ?? -1;
    final String cityName = map['city_name'] ?? "";

    return City(
        cityId: cityId,
      cityName: cityName
    );
  }
}
