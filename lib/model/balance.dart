import 'package:collection/collection.dart';
import 'package:plainsandprintsloyalty/dao/file_dao.dart';
import 'package:plainsandprintsloyalty/dao/file_persistable.dart';
import 'package:plainsandprintsloyalty/model/empty_checkable.dart';
import 'package:plainsandprintsloyalty/model/point.dart';

class Balance with FilePersistable<Balance>, EmptyCheckable {
  final double amount;
  final List<Point> points;

  Balance({
    this.amount,
    this.points
  });

  factory Balance.fromMap(Map<String, dynamic> map) {
    final double amount = map['load_amount'] ?? 0;
    final List<Point> points = map['points'] != null
      ? List.from(map['points']).map((point) => Point.fromMap(point)).toList()
      : [];

    return Balance(
      amount: amount,
      points: points
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Balance &&
        runtimeType == other.runtimeType &&
        amount == other.amount &&
        ListEquality().equals(points, other.points);

  @override
  int get hashCode => 
    amount.hashCode ^ 
    points.hashCode;

  @override
  void saveFile(FileDao dao) {}

  @override
  bool isEmpty() => points == null || points.isEmpty;
}
