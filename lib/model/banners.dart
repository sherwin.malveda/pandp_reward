import 'package:collection/collection.dart';
import 'package:plainsandprintsloyalty/model/Banner.dart';
import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

class Banners with EmptyCheckable {
  final List<Banner> banners;

  Banners({
    this.banners
  });

  factory Banners.fromMap(Map<String, dynamic> map) {
    final List<Banner> banners = map['banner'] != null
      ? List.from(map['banner'])
        .map((banner) => Banner.fromMap(banner))
        .toList()
      : [];

    return Banners(
      banners: banners
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Banners &&
        runtimeType == other.runtimeType &&
        ListEquality().equals(banners, other.banners);

  @override
  int get hashCode => 
    banners.hashCode;

  @override
  bool isEmpty() => banners == null || banners.isEmpty;
}
