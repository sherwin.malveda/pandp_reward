class FAQs {
 
  final String title;
  final List<FAQs> children;

  FAQs(
    this.title, 
    [this.children = const <FAQs>[]]
  );

}