import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

class Voucher with EmptyCheckable {
  final String voucherCode;
  final String description;
  final String status;
  final String expiration;

  Voucher({
    this.voucherCode, 
    this.description, 
    this.status, 
    this.expiration
  });

  factory Voucher.fromMap(Map<String, dynamic> map) {
    final String voucherCode = map['voucher_code'] ?? "";
    final String description = map['voucher_description'] ?? "";
    final String status = map['voucher_status'] ?? "";
    final String expiration = map['voucher_expiration'] ?? "";

    return Voucher(
      voucherCode: voucherCode,
      description: description,
      status: status,
      expiration: expiration
    );
  }

  @override
  bool isEmpty() => false;
}