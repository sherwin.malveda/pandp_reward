import 'package:plainsandprintsloyalty/model/branch.dart';
import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

class Branches with EmptyCheckable {
  final int totalBranches;
  final List<Branch> branches;

  Branches({
    this.totalBranches,
    this.branches
  });

  factory Branches.fromMap(Map<String, dynamic> map) {
    final int totalBranches = map['total_branch'] ?? 0;
    final List<Branch> branches = map['branch'] != null
      ? List.from(map['branch'])
        .map((branch) => Branch.fromMap(branch))
        .toList()
      : [];

    return Branches(
      totalBranches: totalBranches,
      branches: branches
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Branches &&
        runtimeType == other.runtimeType &&
        totalBranches == other.totalBranches &&
        branches == other.branches;

  @override
  int get hashCode => 
    totalBranches.hashCode ^ 
    branches.hashCode;

  @override
  bool isEmpty() => branches == null || branches.isEmpty;
}