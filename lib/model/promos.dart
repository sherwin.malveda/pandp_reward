import 'package:collection/collection.dart';
import 'package:plainsandprintsloyalty/model/empty_checkable.dart';
import 'package:plainsandprintsloyalty/model/promo.dart';

class Promos with EmptyCheckable {
  final int totalPromotions;
  final List<Promo> promotions;

  Promos({
    this.totalPromotions,
    this.promotions
  });

  factory Promos.fromMap(Map<String, dynamic> map) {
    final int totalPromotions = map['total_promotion'] ?? 0;
    final List<Promo> promotions = map['promotion'] != null
    ? List.from(map['promotion'])
      .map((promo) => Promo.fromMap(promo))
      .toList()
    : [];

    return Promos(
      totalPromotions: totalPromotions,
      promotions: promotions
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is Promos &&
        runtimeType == other.runtimeType &&
        totalPromotions == other.totalPromotions &&
        ListEquality().equals(promotions, other.promotions);

  @override
  int get hashCode =>
    totalPromotions.hashCode ^
    promotions.hashCode;

  @override
  bool isEmpty() => promotions == null || promotions.isEmpty;
}
