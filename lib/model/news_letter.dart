import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

class NewsLetter extends EmptyCheckable{
  final int id;
  final String title;
  final String content;
  final String imagePath;

  NewsLetter({
    this.id,
    this.title,
    this.content,
    this.imagePath
  });

  factory NewsLetter.fromMap(Map<String, dynamic> map) {
    final int id = map['newsletter_id'] ?? 0;
    final String title = map['newsletter_title'] ?? "";
    final String content = map['newsletter_content'] ?? "";
    final String imagePath = map['newsletter_img'] ?? "";

    return NewsLetter(
      id: id,
      title: title,
      content: content,
      imagePath: imagePath
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is NewsLetter &&
        runtimeType == other.runtimeType &&
        id == other.id &&
        title == other.title &&
        content == other.content &&
        imagePath == other.imagePath;

  @override
  int get hashCode =>
    id.hashCode ^ 
    title.hashCode ^ 
    content.hashCode ^ 
    imagePath.hashCode;

  @override
  bool isEmpty() => false;
}