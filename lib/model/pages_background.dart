import 'package:plainsandprintsloyalty/model/empty_checkable.dart';

class PageBackground with EmptyCheckable {
  final String pageId;
  final String pageName;
  final String pageImage;

  PageBackground({
    this.pageId,
    this.pageName,
    this.pageImage
  });

  factory PageBackground.fromMap(Map<String, dynamic> map) {
    final String pageId = map['page_id '] ?? "";
    final String pageName = map['page_name '] ?? "";
    final String pageImage = map['background_url'] ?? "";

    return PageBackground(
      pageId: pageId,
      pageName: pageName,
      pageImage: pageImage
    );
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is PageBackground &&
        runtimeType == other.runtimeType &&
        pageId == other.pageId &&
        pageName == other.pageName &&
        pageImage == other.pageImage;

  @override
  int get hashCode =>
    pageId.hashCode ^ 
    pageName.hashCode ^ 
    pageImage.hashCode;

  @override
  bool isEmpty() => false;
}
