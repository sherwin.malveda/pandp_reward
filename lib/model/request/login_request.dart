class LoginRequest {
  final String username;
  final String password;
  final String fcmtoken;

  LoginRequest({
    this.username,
    this.password,
    this.fcmtoken
  });

  toMap() => {
    "username": username, 
    "password": password, 
    "messaging_token": fcmtoken
  };

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is LoginRequest &&
        runtimeType == other.runtimeType &&
        username == other.username &&
        password == other.password  &&
        fcmtoken == other.fcmtoken;

  @override
  int get hashCode =>
    username.hashCode ^
    password.hashCode ^
    fcmtoken.hashCode;  
}
