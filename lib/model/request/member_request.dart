import 'package:meta/meta.dart';

class MemberRequest {
  final String carddigits;
  final String birthday;

  MemberRequest({
    @required this.carddigits,
    @required this.birthday
  });

  Map<String, dynamic> toMap() => {
    "card_digits": carddigits,
    "birth_date": birthday
  };

  factory MemberRequest.fromMap(Map<String, dynamic> map) {
    final String carddigits = map['card_digits'] ?? "";
    final String birthday = map['birth_date '] ?? "";

    return MemberRequest(carddigits: carddigits, birthday: birthday);
  }

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is MemberRequest &&
        runtimeType == other.runtimeType &&
        carddigits == other.carddigits &&
        birthday == other.birthday;

  @override
  int get hashCode => 
    carddigits.hashCode ^ 
    birthday.hashCode;
}