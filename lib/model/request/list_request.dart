class ListRequest {
  final int accountId;
  final int rows;
  final int page;

  ListRequest({
    this.accountId, 
    this.rows, 
    this.page
  });

  toMap() => {
    "row_count": rows, 
    "page_no": page
  };

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is ListRequest &&
        runtimeType == other.runtimeType &&
        rows == other.rows &&
        page == other.page;

  @override
  int get hashCode =>
    rows.hashCode ^
    page.hashCode;
}
