import 'package:meta/meta.dart';

class ResetPasswordRequest {
  final String code;
  final String newPassword;
  final String confirmPassword;

  ResetPasswordRequest({
    @required this.code,
    @required this.newPassword,
    @required this.confirmPassword
  });

  Map<String, dynamic> toMap() => {
    "reset_code": code,
    "new_password": newPassword,
    "confirm_password": confirmPassword
  };

  @override
  bool operator ==(Object other) =>
    identical(this, other) ||
      other is ResetPasswordRequest &&
        runtimeType == other.runtimeType &&
        code == other.code &&
        newPassword == other.newPassword &&
        confirmPassword == other.confirmPassword;

  @override
  int get hashCode =>
    code.hashCode ^ 
    newPassword.hashCode ^ 
    confirmPassword.hashCode;
}
