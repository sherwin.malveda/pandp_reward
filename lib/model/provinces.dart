import 'package:plainsandprintsloyalty/model/province.dart';

class Provinces {
  final List<Province> provinces;

  Provinces({
    this.provinces
  });

  factory Provinces.fromJson(List<dynamic> parsedJson) {

    List<Province> provinces = new List<Province>();
    provinces = parsedJson.map((i)=>Province.fromMap(i)).toList();

    return Provinces(
      provinces: provinces
    );
  }
}
