class Province {
  final int provinceId;
  final String provinceName;

  Province({
    this.provinceId,
    this.provinceName
  });

  factory Province.fromMap(Map<String, dynamic> map) {
    final int provinceId = map['province_id'] ?? -1;
    final String provinceName = map['province_name'] ?? "";

    return Province(
      provinceId: provinceId,
      provinceName: provinceName
    );
  }
}
