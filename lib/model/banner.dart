class Banner {
  final int bannerId;
  final String imageUrl;
  final int type;
  final String redirectUrl;

  Banner({
    this.bannerId,
    this.imageUrl,
    this.type,
    this.redirectUrl
  });

  factory Banner.fromMap(Map<String, dynamic> map) {
    final int bannerId = map['banner_id'] ?? -1;
    final String imageUrl = map['image_url'] ?? "";
    final int type = map['type'] ?? "";
    final String redirectUrl = map['redirect_url'] ?? "";

    return Banner(
      bannerId: bannerId,
      imageUrl: imageUrl,
      type: type,
      redirectUrl: redirectUrl
    );
  }
}
