import 'package:plainsandprintsloyalty/model/faqs.dart';
    
enum States { IDLE, WAITING, DONE, ERROR }

// Api Endpoint
const String API_ENDPOINT = "https://plainsandprints.me:8093";
//const String API_ENDPOINT = "http://58.69.149.193:8083";
//const String API_ENDPOINT = "http://172.16.0.8:8083";
//const String API_ENDPOINT = "http://180.232.98.122:3479";
//const String API_ENDPOINT = "http://10.36.28.227:1187";

//pagination
const int MAX_ROW_LIST = 30;
const int INFINITE_ROW_LIST = 999999;
const int MAX_RESEND_TIMER = 30;

//registrations
const int GENDER_NONE = 0;
const int GENDER_MALE = 1;
const int GENDER_FEMALE = 2;

const int TITLE_NONE = 0;
const int TITLE_MR = 1;
const int TITLE_MRS = 2;

const int MARITAL_STATUS_NONE = 0;
const int MARITAL_STATUS_SINGLE = 1;
const int MARITAL_STATUS_MARRIED = 2;
const int MARITAL_STATUS_SEPARATED = 3;
const int MARITAL_STATUS_WIDOWED = 4;

const int FORGOT_PASSWORD_BACKGROUND = 8;
const int LOGIN_BACKGROUND = 7;
const int EXISTING_MEMBER_BACKGROUND = 4;
const int GETTING_STARTED_PAGE = 2;
const int SPLASH_SCREEN_PAGE = 1;
const int LANDING_PAGE = 3;
const int DASHBOARD_PAGE = 9;

Map<int, String> provinceList = {};
Map<int, String> cityList = {};

// faqs
final List<FAQs> faqsData = <FAQs>[
  FAQs(
    '1.	How can I create an account?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t •	For EXISTING members, customer must fill out the Customer ID, "+
          "email address, mobile number and city address to complete registration or "+
          "through FB registered information.\n"
      ),
      FAQs(
          "\t\t\t\t\t•	For NEW members, customer must fill out Full Name, Birthdate, "+
          "(MM-DD-YYYY), Email, Mobile Number, City Address or through FB registered "+
          "information.\n"
      ),
    ],
  ),
  FAQs(
    '2.	What if I forgot my password?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t•	If you have an existing account, select “Forgot Password?” on "+
          "the Login page and enter the e-mail address you used to register then select "+
          "“RESET”\n"
      ),
      FAQs(
          "\t\t\t\t\t•	A step by step procedure will be sent to your e-mail in order to "+
          "reset your account password\n"
      ),
    ],
  ),
  FAQs(
    '3.	Can I sign up without a smart phone?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t•	All customers must download the Plains & Prints app on their smart "+
          "phone and are required to register online to be able to join the Plains & Prints "+
          "e-loyalty program.\n"
      ),
    ],
  ),
  FAQs(
    '4.	How do I know if my account is successfully activated?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t•	To activate account, customer must verify his/her account via the "+
          "registered e-mail on the Plains & Prints app."
      ),
    ],
  ),
  FAQs(
    '5.	Can I still redeem my points in store without the app?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t•	Customers cannot redeem points without the Plains & Prints app. "+
          "Customer must convert his/her points to voucher codes on the app that can be "+
          "used during transaction. Voucher code is valid within 48 hours. Unused vouchers "+
          "will be reverted back to the account.\n"
      ),
    ],
  ),
  FAQs(
    '6.	Can I still earn points using the Plains & Prints partnership card?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t•	All members must present their Plains & Prints e-loyalty card via "+
          "the app to earn points. Plains & Prints partnership cards will no longer be valid "+
          "for earning or redeeming of points.\n"
      ),
    ],
  ),
  FAQs(
    '7.	How can I use my existing Plains & Prints partnership card points?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t•	All EXISTING members must register on the Plains & Prints app. Once "+
          "registration is completed, member’s existing partnership card points will "+
          "automatically be added to the account’s REDEEM POINTS page.\n"
      ),
    ],
  ),
  FAQs(
    '8.	Where can I use the Plains & Prints app?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t•	The Plains & Prints e-loyalty program via the mobile app is valid in "+
          "all Plains & Prints and RAF boutiques nationwide and online at www.plainsandprints.com. \n"
        ),
      FAQs(
          "\t\t\t\t\t•	Plains & Prints mobile app is qualified to issue or transact points any "+
          "time during Store Operations.\n"
      ),
    ],
  ),
  FAQs(
    '9.	How can I earn or redeem points online at www.plainsandprints.com?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t•	For online www.plainsandprints.com, customer must log in or sign up on the "+
          "website using registered e-mail address on the mobile app before proceeding with his/her "+
          "online transaction.\n"
      ),
      FAQs(
          "\t\t\t\t\t•	Customers must make sure they are registered on the Plains & Prints mobile app "+
          "in order to earn and redeem points online at www.plainsandprints.com.\n"
      ),
    ],
  ),
  FAQs(
    '10.	How much is the earning of points per purchase transaction?',
    <FAQs>[
      FAQs(
          "\t\t\t\t\t•	For every purchase of any regular priced Plains & Prints and/or RAF merchandise, "+
          "no minimum purchase required, cash, credit card or through purchased Gift Certificates, customer "+
          "shall earn points depending on his/her e-loyalty member type:\n"+
          "\t\t\t\t\t\t\t\t\t\ti.	Regular member – 3% of total transaction\n"+
          "\t\t\t\t\t\t\t\t\t\tii.	Prestige member – 10% of total transaction \n"+
          "\t\t\t\t\t\t\t\t\t\tiii.	Elite member – 20% of total transaction\n"
      ),
      FAQs(
          "\t\t\t\t\t•	System will round up earning amount if there is a decimal of .5 & above and drop if "+
          "decimal is .4 below."
      ),
    ],
  ),
];
