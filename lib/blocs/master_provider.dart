import 'package:flutter/material.dart';
import 'package:plainsandprintsloyalty/api/session_invalidation_notifier.dart';
import 'package:plainsandprintsloyalty/blocs/auth_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/card_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/page_bloc.dart';
import 'package:plainsandprintsloyalty/blocs/store_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MasterProvider extends StatefulWidget {
  final AuthBloc authBloc;
  final CardBloc cardBloc;
  final StoreBloc storeBloc;
  final PageBloc pageBloc;
  final SessionInvalidationNotifier sessionInvalidationNotifier;
  final Widget child;

  const MasterProvider({
    Key key,
    @required this.authBloc,
    @required this.cardBloc,
    @required this.storeBloc,
    @required this.pageBloc,
    @required this.sessionInvalidationNotifier,
    @required this.child,
  }) : super(key: key);

  @override
  _MasterProviderState createState() => _MasterProviderState();

  static AuthBloc auth(BuildContext context) =>
    (context.inheritFromWidgetOfExactType(_BlocProvider) as _BlocProvider).authBloc;

  static CardBloc card(BuildContext context) =>
    (context.inheritFromWidgetOfExactType(_BlocProvider) as _BlocProvider).cardBloc;

  static StoreBloc store(BuildContext context) =>
    (context.inheritFromWidgetOfExactType(_BlocProvider) as _BlocProvider).storeBloc;

  static PageBloc page(BuildContext context) =>
    (context.inheritFromWidgetOfExactType(_BlocProvider) as _BlocProvider).pageBloc;

  static SessionInvalidationNotifier sessionInvalidator(BuildContext context) =>
    (context.inheritFromWidgetOfExactType(_BlocProvider) as _BlocProvider).sessionInvalidationNotifier;

  static void clearData(BuildContext context) async {
    final AuthBloc auth = MasterProvider.auth(context);
    final CardBloc card = MasterProvider.card(context);
    final StoreBloc store = MasterProvider.store(context);
    final PageBloc page = MasterProvider.page(context);
    
    auth.clearData();
    card.clearData();
    store.clearData();
    page.clearData();
 
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
}

class _MasterProviderState extends State<MasterProvider> {
  @override
  Widget build(BuildContext context) {
    return _BlocProvider(
      authBloc: widget.authBloc,
      cardBloc: widget.cardBloc,
      storeBloc: widget.storeBloc,
      pageBloc: widget.pageBloc,
      sessionInvalidationNotifier: widget.sessionInvalidationNotifier,
      child: widget.child,
    );
  }
}

class _BlocProvider extends InheritedWidget {
  final AuthBloc authBloc;
  final CardBloc cardBloc;
  final StoreBloc storeBloc;
  final PageBloc pageBloc;
  final SessionInvalidationNotifier sessionInvalidationNotifier;

  const _BlocProvider({
    Key key,
    @required this.authBloc,
    @required this.cardBloc,
    @required this.storeBloc,
    @required this.pageBloc,
    @required this.sessionInvalidationNotifier,
    @required Widget child,
  })  : assert(child != null),
        super(key: key, child: child);

  static _BlocProvider of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(_BlocProvider) as _BlocProvider;
  }

  @override
  bool updateShouldNotify(_BlocProvider old) {
    return old.authBloc != authBloc;
  }
}
