import 'package:meta/meta.dart';
import 'package:plainsandprintsloyalty/blocs/bloc.dart';
import 'package:plainsandprintsloyalty/model/banners.dart';
import 'package:plainsandprintsloyalty/model/branch_details.dart';
import 'package:plainsandprintsloyalty/model/branches.dart';
import 'package:plainsandprintsloyalty/model/news_letter.dart';
import 'package:plainsandprintsloyalty/model/news_letters.dart';
import 'package:plainsandprintsloyalty/model/notifications.dart';
import 'package:plainsandprintsloyalty/model/promo_details.dart';
import 'package:plainsandprintsloyalty/model/promos.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/store_repository.dart';
import 'package:rxdart/rxdart.dart';

class StoreBloc implements Bloc {
  final StoreRepository storeRepository;

  StoreBloc({@required this.storeRepository});

  BehaviorSubject<Promos> get promos => _promos;
  
  PublishSubject<PromoDetails> get promoDetails => _promoDetails;

  BehaviorSubject<NewsLetters> get newsLetters => _newsletters;

  PublishSubject<NewsLetter> get newsLetter => _newsletter;

  BehaviorSubject<Branches> get branches => _branches;

  PublishSubject<BranchDetails> get branchDetails => _branchDetails;

  BehaviorSubject<Banners> get banners => _banners;

  BehaviorSubject<Banners> get banner => _banner;

  BehaviorSubject<Banners> get landing => _landing;

  BehaviorSubject<Notifications> get notification => _notif;

  void getPromos(ListRequest request) async {
    try {
      final Promos promos = await storeRepository.getPromos(request);
      _promos.add(promos);
    } catch (error) {
      _promos.addError(error);
    }
  }

  void getPromoDetails(int promoId) async {
    try {
      final PromoDetails promo = await storeRepository.getPromo(promoId);
      _promoDetails.add(promo);
    } catch (error) {
      _promoDetails.addError(error);
    }
  }

  void getNewsLetter(ListRequest request) async {
    try {
      final NewsLetters newsLetters = await storeRepository.newsLetters(request);
      _newsletters.add(newsLetters);
    } catch (error) {
      _newsletters.addError(error);
    }
  }

  void getNewsLetterDetails(int newsletterId) async {
    try {
      final NewsLetter newsletter = await storeRepository.getNewsletter(newsletterId);
      _newsletter.add(newsletter);
    } catch (error) {
      _newsletter.addError(error);
    }
  }

  void getBranchDetails(int branchId) async {
    try {
      final BranchDetails branch = await storeRepository.getBranch(branchId);
      _branchDetails.add(branch);
    } catch (error) {
      _branchDetails.addError(error);
    }
  }

  Future<void> getBranches(ListRequest request) async {
    try {
      final Branches newBranches = await storeRepository.getBranches(request);
      _branches.add(newBranches);
    } catch (error) {
      if (_branches.value == null) _branches.addError(error);
    }
  }

  void getBanners(ListRequest request) async {
    try {
      final Banners banners = await storeRepository.getBanners(request);
      _banners.add(banners);
    } catch (error) {
      _banners.addError(error);
    }
  }

  void getBanner(ListRequest request) async {
    try {
      final Banners banner = await storeRepository.getBanners(request);
      _banner.add(banner);
    } catch (error) {
      _banner.addError(error);
    }
  }

  void getBanner2(ListRequest request) async {
    try {
      final Banners landing = await storeRepository.getBanners(request);
      _landing.add(landing);
    } catch (error) {
      _landing.addError(error);
    }
  }

  void getNotification(ListRequest request) async {
    try {
      final Notifications notification = await storeRepository.getNotifications(request);
      _notif.add(notification);
    } catch (error) {
      _notif.addError(error);
    }
  }

  void close() {
    _promos.close();
    _promoDetails.close();
    _branches.close();
    _branchDetails.close();
    _banners.close();
    _banner.close();
    _landing.close();
    _notif.close();
    _newsletters.close();
  }

  @override
  void clearData() {
    _promos.add(null);
    _branches.add(null);
    _banners.add(null);
    _banner.add(null);
    _landing.add(null);
    _notif.add(null);
    _newsletters.add(null);
  }

  final BehaviorSubject<Promos> _promos = BehaviorSubject();
  final BehaviorSubject<NewsLetters> _newsletters = BehaviorSubject();
  final PublishSubject<NewsLetter> _newsletter = PublishSubject();
  final BehaviorSubject<Branches> _branches = BehaviorSubject();
  final PublishSubject<BranchDetails> _branchDetails = PublishSubject();
  final PublishSubject<PromoDetails> _promoDetails = PublishSubject();
  final BehaviorSubject<Banners> _banners = BehaviorSubject();
  final BehaviorSubject<Banners> _banner = BehaviorSubject();
  final BehaviorSubject<Banners> _landing = BehaviorSubject();
  final BehaviorSubject<Notifications> _notif = BehaviorSubject();
}