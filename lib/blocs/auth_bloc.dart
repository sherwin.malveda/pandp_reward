import 'package:meta/meta.dart';
import 'package:plainsandprintsloyalty/blocs/bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/model/request/activation_request.dart';
import 'package:plainsandprintsloyalty/model/request/change_password_request.dart';
import 'package:plainsandprintsloyalty/model/request/forgot_password_password.dart';
import 'package:plainsandprintsloyalty/model/request/login_request.dart';
import 'package:plainsandprintsloyalty/model/request/member_request.dart';
import 'package:plainsandprintsloyalty/model/request/reset_password_request.dart';
import 'package:plainsandprintsloyalty/model/request/verification_request.dart';
import 'package:plainsandprintsloyalty/model/session.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/auth_repository.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc implements Bloc {
  final AuthRepository authRepository;

  PublishSubject<ScreenState> get loginState => _loginState;

  PublishSubject<ScreenState> get registerState => _registerState;

  PublishSubject<ScreenState> get verificationState => _verificationState;

  PublishSubject<ScreenState> get activationState => _activationState;

  PublishSubject<ScreenState> get changePasswordState => _changePasswordState;

  PublishSubject<ScreenState> get forgotPasswordState => _forgotPasswordState;

  PublishSubject<ScreenState> get resetPasswordState => _resetPasswordState;

  PublishSubject<User> get existingMember => _existingMember;

  BehaviorSubject<User> get user => _user;

  PublishSubject<VerificationRequest> get registrationCredentials => _registrationCredentials;
  
  BehaviorSubject<Session> get session => _session;

  AuthBloc({@required this.authRepository}) {
    this.authRepository.getCachedSession().then((session) {
      if (session.accountId != null && session.accountId > -1)
        _session.add(session);
    });
  }

  void existingmember(MemberRequest request) async {
    try {
      final User newuser = await authRepository.existingMember(request);
      _existingMember.add(newuser);
      _user.add(null);
    } catch (error) {
      _existingMember.addError(error);
    }
  }

  void login(LoginRequest request) async {
    try {
      _loginState.add(ScreenState(States.WAITING));
      final Session session = await authRepository.login(request);
      _session.add(session);
      _loginState.add(ScreenState(States.DONE));
    } catch (error) {
      _session.addError(error);
      _loginState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void register(User user) async {
    try {
      _registerState.add(ScreenState(States.WAITING));
      final VerificationRequest credentials =
      await authRepository.register(user);
      _registrationCredentials.add(credentials);
      _registerState.add(ScreenState(States.DONE));
    } catch (error) {
      _registerState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void forgotPassword(ForgotPasswordRequest request) async {
    try {
      _forgotPasswordState.add(ScreenState(States.WAITING));
      await authRepository.forgotPassword(request);
      _forgotPasswordState.add(ScreenState(States.DONE));
    } catch (error) {
      _forgotPasswordState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void changePassword(ChangePasswordRequest request) async {
    try {
      _changePasswordState.add(ScreenState(States.WAITING));
      await authRepository.changePassword(request);
      _changePasswordState.add(ScreenState(States.DONE));
    } catch (error) {
      _changePasswordState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void resetPassword(ResetPasswordRequest request) async {
    try {
      _resetPasswordState.add(ScreenState(States.WAITING));
      await authRepository.resetPassword(request);
      _resetPasswordState.add(ScreenState(States.DONE));
    } catch (error) {
      _resetPasswordState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void verifyAccount(VerificationRequest request) async {
    try {
      _verificationState.add(ScreenState(States.WAITING));
      await authRepository.verifyAccount(request);
      _verificationState.add(ScreenState(States.DONE));
    } catch (error) {
      _verificationState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void activateAccount(ActivationRequest request) async {
    try {
      _activationState.add(ScreenState(States.WAITING));
      await authRepository.activateAccount(request);
      _activationState.add(ScreenState(States.DONE));
    } catch (error) {
      _activationState.add(ScreenState(States.ERROR, error: error));
    }
  }


  close() {
    _session.close();
    _loginState.close();
    _registerState.close();
    _changePasswordState.close();
    _forgotPasswordState.close();
    _resetPasswordState.close();
    _verificationState.close();
    _activationState.close();
    _registrationCredentials.close();
    _existingMember.close();
    _user.close();
  }

  final BehaviorSubject<Session> _session = BehaviorSubject();
  final PublishSubject<ScreenState> _loginState = PublishSubject();
  final PublishSubject<VerificationRequest> _registrationCredentials = PublishSubject();
  final PublishSubject<ScreenState> _registerState = PublishSubject();
  final PublishSubject<ScreenState> _verificationState = PublishSubject();
  final PublishSubject<ScreenState> _activationState = PublishSubject();
  final PublishSubject<ScreenState> _changePasswordState = PublishSubject();
  final PublishSubject<ScreenState> _resetPasswordState = PublishSubject();
  final PublishSubject<ScreenState> _forgotPasswordState = PublishSubject();
  final BehaviorSubject<User> _user = BehaviorSubject();
  final PublishSubject<User> _existingMember = PublishSubject();

  @override
  void clearData() {
    _session.add(null);
  }

  Future<bool> hasSession() async {
    final Session session = await authRepository.getCachedSession();
    return session.accountId != null && session.accountId > -1;
  }
}
