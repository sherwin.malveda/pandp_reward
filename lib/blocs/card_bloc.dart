import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:plainsandprintsloyalty/blocs/bloc.dart';
import 'package:plainsandprintsloyalty/constants.dart';
import 'package:plainsandprintsloyalty/model/balance.dart';
import 'package:plainsandprintsloyalty/model/history.dart';
import 'package:plainsandprintsloyalty/model/request/balance_request.dart';
import 'package:plainsandprintsloyalty/model/request/cancel_voucher_request.dart';
import 'package:plainsandprintsloyalty/model/request/convert_points_request.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/model/voucher.dart';
import 'package:plainsandprintsloyalty/model/vouchers.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/card_repository.dart';
import 'package:plainsandprintsloyalty/screens/screen_state.dart';
import 'package:rxdart/rxdart.dart';

class CardBloc implements Bloc {
  final CardRepository cardRepository;

  CardBloc({@required this.cardRepository});

  PublishSubject<ScreenState> get updateState => _updateState;

  PublishSubject<ScreenState> get cancelVoucherState => _cancelVoucherState;

  PublishSubject<Voucher> get newVoucher => _newVoucher;

  BehaviorSubject<Balance> get balance => _balance;

  BehaviorSubject<User> get currentUser => _currentUser;

  BehaviorSubject<History> get history => _history;

  BehaviorSubject<Vouchers> get vouchers => _vouchers;

  void getAccountDetails(int accountId) async {
    try {
      final User user = await cardRepository.getAccountDetails(accountId);
      _currentUser.add(user);
    } catch (error) {
      _currentUser.addError(error);
    }
  }

  void getBalance(BalanceRequest request) async {
    try {
      final Balance balance = await cardRepository.getBalance(request);
      _balance.add(balance);
    } catch (error) {
      _balance.addError(error);
    }
  }

  Future<void> getHistory(ListRequest request) async {
    try {
      final History newHistory = await cardRepository.getHistory(request);
      _history.add(newHistory);
    } catch (error) {
      _history.addError(error);
    }

  }

  Future<void> getVouchers(ListRequest request) async {
    try {
      final Vouchers newVouchers = await cardRepository.getVouchers(request);
        _vouchers.add(newVouchers);
    } catch (error) {
      if (_vouchers.value == null) _vouchers.addError(error);
    }
  }

  void updateAccount(int accountId, User user) async {
    try {
      _updateState.add(ScreenState(States.WAITING));
      await cardRepository.updateAccount(accountId, user);
      _updateState.add(ScreenState(States.DONE));
      getAccountDetails(accountId);
    } catch (error) {
      _updateState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void cancelVoucher(CancelVoucherRequest request) async {
    try {
      _cancelVoucherState.add(ScreenState(States.WAITING));
      await cardRepository.cancelVoucher(request);
      _cancelVoucherState.add(ScreenState(States.DONE));
      _vouchers.add(null);
      final ListRequest vouchersRequest = ListRequest(
        accountId: request.accountId, rows: MAX_ROW_LIST, page: 1
      );
      final BalanceRequest balanceRequest = BalanceRequest(
        accountId: request.accountId, cardPan: request.voucherCode
      );
      getVouchers(vouchersRequest);
      getBalance(balanceRequest);
    } catch (error) {
      _cancelVoucherState.add(ScreenState(States.ERROR, error: error));
    }
  }

  void convertPoints(ConvertPointsRequest request) async {
    try {
      final Voucher newVoucher = await cardRepository.convertPoints(request);
      _newVoucher.add(newVoucher);
      _vouchers.add(null);
      final ListRequest vouchersRequest = ListRequest(
        accountId: request.accountId, rows: MAX_ROW_LIST, page: 1
      );
      final BalanceRequest balanceRequest = BalanceRequest(
        accountId: request.accountId, cardPan: request.cardPan
      );
      getVouchers(vouchersRequest);
      getBalance(balanceRequest);
    } catch (error) {
      _newVoucher.addError(error);
    }
  }

  void close() {
    _currentUser.close();
    _balance.close();
    _history.close();
    _updateState.close();
    _newVoucher.close();
    _cancelVoucherState.close();
  }

  @override
  void clearData() {
    _currentUser.add(null);
    _balance.add(null);
    _history.add(null);
    _vouchers.add(null);
  }
  final BehaviorSubject<User> _currentUser = BehaviorSubject();
  final BehaviorSubject<Balance> _balance = BehaviorSubject();
  final BehaviorSubject<History> _history = BehaviorSubject();
  final BehaviorSubject<Vouchers> _vouchers = BehaviorSubject();
  final PublishSubject<ScreenState> _updateState = PublishSubject();
  final PublishSubject<ScreenState> _cancelVoucherState = PublishSubject();
  final PublishSubject<Voucher> _newVoucher = PublishSubject();


}