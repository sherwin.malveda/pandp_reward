import 'package:meta/meta.dart';
import 'package:plainsandprintsloyalty/blocs/bloc.dart';
import 'package:plainsandprintsloyalty/model/pages_background.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/page_repository.dart';

class PageBloc implements Bloc {
  final PageRepository pageRepository;

  PageBloc({@required this.pageRepository});

  PageBackground page;
  var termsandcondition, contactus, aboutus;
  
  Future<PageBackground> getPageBackground(int pageId) async {
    try {
      page = await pageRepository.getPagesBackground(pageId);
    } catch (error) {}
    return page;
  }

    
  Future getTermsandCondition(int pageId) async {
    try {
      termsandcondition = await pageRepository.getTermsandCondition(pageId);
    } catch (error) {}
    return termsandcondition;
  }

  Future getAboutUs(int pageId) async {
    try {
      aboutus = await pageRepository.getTermsandCondition(pageId);
    } catch (error) {}
    return aboutus;
  }

  Future getContactUs(int pageId) async {
    try {
      contactus = await pageRepository.getTermsandCondition(pageId);
    } catch (error) {}
    return contactus;
  }

  @override
  void clearData() {}
}
