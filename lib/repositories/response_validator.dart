import 'package:dio/dio.dart';
import 'package:plainsandprintsloyalty/api/session_invalidation_notifier.dart';
import 'package:plainsandprintsloyalty/errors/not_activated_error.dart';
import 'package:plainsandprintsloyalty/errors/standard_error.dart';
import 'package:plainsandprintsloyalty/model/request/verification_request.dart';

class ResponseValidator {
  final SessionInvalidationNotifier sessionInvalidationNotifier;

  ResponseValidator(this.sessionInvalidationNotifier);

  void checkError(Response response) {
    if (response == null) throw StandardError(message: "Something went wrong");
    switch (response.statusCode) {
      case 200:
        break;
      case 401:
        sessionInvalidationNotifier.notifyListeners();
        break;
      case 313:
        throw NotActivatedError(request: VerificationRequest.fromMap(response.data));
        break;
      default:
        dynamic error;
        String errorMessage;
        try {
          error = response.data['error'] ?? {};
          errorMessage = error["message"] ?? "Something went wrong";
        } catch (error) {
          errorMessage = "Something went wrong";
        }
        throw StandardError(message: errorMessage);
        break;
    }
  }
}