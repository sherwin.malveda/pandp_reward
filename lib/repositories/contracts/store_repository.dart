import 'package:plainsandprintsloyalty/model/banners.dart';
import 'package:plainsandprintsloyalty/model/branch_details.dart';
import 'package:plainsandprintsloyalty/model/branches.dart';
import 'package:plainsandprintsloyalty/model/news_letter.dart';
import 'package:plainsandprintsloyalty/model/news_letters.dart';
import 'package:plainsandprintsloyalty/model/notifications.dart';
import 'package:plainsandprintsloyalty/model/promo_details.dart';
import 'package:plainsandprintsloyalty/model/promos.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';

abstract class StoreRepository {

  Future<Promos> getPromos(ListRequest request);

  Future<PromoDetails> getPromo(int promoId);

  Future<NewsLetters> newsLetters(ListRequest request);

  Future<NewsLetter> getNewsletter(int newsletterId);

  Future<Branches> getBranches(ListRequest request);

  Future<BranchDetails> getBranch(int branchId);

  Future<Banners> getBanners(ListRequest request);

  Future<Notifications> getNotifications(ListRequest request);
}
