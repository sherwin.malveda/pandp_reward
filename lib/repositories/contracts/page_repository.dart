import 'package:plainsandprintsloyalty/model/pages_background.dart';

abstract class PageRepository {
  Future<PageBackground> getPagesBackground(int pageId);
  
  Future getTermsandCondition(int pageId);

  Future getAboutUs(int pageId);

  Future getContactUs(int pageId);
}
