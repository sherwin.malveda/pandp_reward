import 'package:plainsandprintsloyalty/model/balance.dart';
import 'package:plainsandprintsloyalty/model/history.dart';
import 'package:plainsandprintsloyalty/model/request/balance_request.dart';
import 'package:plainsandprintsloyalty/model/request/cancel_voucher_request.dart';
import 'package:plainsandprintsloyalty/model/request/convert_points_request.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/model/voucher.dart';
import 'package:plainsandprintsloyalty/model/vouchers.dart';

abstract class CardRepository {
  Future<User> getAccountDetails(int accountId);

  Future<void> updateAccount(int accountId, User user);

  Future<void> cancelVoucher(CancelVoucherRequest request);

  Future<Balance> getBalance(BalanceRequest request);

  Future<History> getHistory(ListRequest request);

  Future<Vouchers> getVouchers(ListRequest request);

  Future<Voucher> convertPoints(ConvertPointsRequest request);
}
