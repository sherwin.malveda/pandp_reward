import 'package:plainsandprintsloyalty/model/request/activation_request.dart';
import 'package:plainsandprintsloyalty/model/request/change_password_request.dart';
import 'package:plainsandprintsloyalty/model/request/forgot_password_password.dart';
import 'package:plainsandprintsloyalty/model/request/login_request.dart';
import 'package:plainsandprintsloyalty/model/request/member_request.dart';
import 'package:plainsandprintsloyalty/model/request/reset_password_request.dart';
import 'package:plainsandprintsloyalty/model/request/verification_request.dart';
import 'package:plainsandprintsloyalty/model/session.dart';
import 'package:plainsandprintsloyalty/model/user.dart';

abstract class AuthRepository {
  
  Future<User> existingMember(MemberRequest request);

  Future<Session> login(LoginRequest request);

  Future<void> forgotPassword(ForgotPasswordRequest request);

  Future<VerificationRequest> register(User user);

  Future<void> changePassword(ChangePasswordRequest request);

  Future<void> resetPassword(ResetPasswordRequest request);

  Future<void> activateAccount(ActivationRequest request);

  Future<void> verifyAccount(VerificationRequest request);
  
  Future<Session> getCachedSession();
}
