import 'package:dio/dio.dart';
import 'package:plainsandprintsloyalty/model/pages_background.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/page_repository.dart';

class MockedPageRepository implements PageRepository {
  @override
  Future<PageBackground> getPagesBackground(int pageId) async {
    final Map<String, dynamic> responseMap = {
      "page_id": "3",
      "page_name": "Background",
      "background_img":
        "https://www.plainsandprints.com/upload/stadvancedbanner/collectionbannertimeless.jpg"
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    return PageBackground.fromMap(response.data);
  }

  @override
  Future getTermsandCondition(int pageId) {
    return null;
  }

  @override
  Future getAboutUs(int pageId) {
    return null;
  }

  @override
  Future getContactUs(int pageId) {
    return null;
  }
}