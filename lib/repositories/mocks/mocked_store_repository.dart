import 'package:dio/dio.dart';
import 'package:plainsandprintsloyalty/model/banners.dart';
import 'package:plainsandprintsloyalty/model/branch_details.dart';
import 'package:plainsandprintsloyalty/model/branches.dart';
import 'package:plainsandprintsloyalty/model/news_letter.dart';
import 'package:plainsandprintsloyalty/model/news_letters.dart';
import 'package:plainsandprintsloyalty/model/notifications.dart';
import 'package:plainsandprintsloyalty/model/promo_details.dart';
import 'package:plainsandprintsloyalty/model/promos.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/store_repository.dart';

class MockedStoreRepository implements StoreRepository {
  @override
  Future<BranchDetails> getBranch(int branchId) async {
    final Map<String, dynamic> responseMap = {
      "branch_name": "Paseo - Makati Branch",
      "branch_code": "MB01",
      "branch_address": "Paseo de Roxas, Makati City, Philippines",
      "longitude": "-122.085749655962",
      "latitude": "37.42796133580664",
      "email": "johndoe@email.com",
      "landline_no": "02-109-1212",
      "mobile_no": "+639954408843"
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    return BranchDetails.fromMap(response.data);
  }

  @override
  Future<Branches> getBranches(ListRequest request) async {
    final Map<String, dynamic> responseMap = {
      "total_branch": 50,
      "branch": [
        {
          "branch_id": 1,
          "branch_name": "Paseo - Makati Branch",
          "branch_code": "MB01",
          "branch_address": "Paseo de Roxas, Makati City, Philippines"
        }
      ]
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    return Branches.fromMap(response.data);
  }

  Future<PromoDetails> getPromo(int promoId) async {
    final Map<String, dynamic> responseMap = {
      "promo_id": 1,
      "promo_name": "Summer Special 2019",
      "promo_desc": "Celebrate 2019's summer heat with 20% discount”",
      "promo_categ": "Summer Special Promo",
      "promo_img": "https://www.sample.com/files/img_background.jpg"
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    return PromoDetails.fromMap(response.data);
  }

  @override
  Future<Promos> getPromos(ListRequest request) async {
    final Map<String, dynamic> responseMap = {
      "total_transaction": 50,
      "promotion": [
        {
          "promo_name": "Summer Special 2019",
          "promo_desc": "Celebrate 2019's summer heat with 20% discount",
          "promo_categ": "Summer Special Promo",
          "promo_img":
              "https://www.plainsandprints.com/upload/stadvancedbanner/collectionbannertimeless.jpg"
        },
        {
          "promo_name": "Summer Special 2019",
          "promo_desc": "Celebrate 2019's summer heat with 20% discount",
          "promo_categ": "Summer Special Promo",
          "promo_img":
          "https://www.plainsandprints.com/upload/stadvancedbanner/collectionbannertimeless.jpg"
        },
        {
          "promo_name": "Summer Special 2019",
          "promo_desc": "Celebrate 2019's summer heat with 20% discount",
          "promo_categ": "Summer Special Promo",
          "promo_img":
          "https://www.plainsandprints.com/upload/stadvancedbanner/collectionbannertimeless.jpg"
        }
      ]
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    return Promos.fromMap(response.data);
  }

  @override
  Future<Banners> getBanners(ListRequest request) async {
    final Map<String, dynamic> responseMap = {
      "banner": [
        {
          "banner_id": 1,
          "image_url ": "https://www.sample.com/files/img1.jpg",
          "type": 1,
          "redirect_url": "https://www.anotherpage.com"
        },
        {
          "banner_id": 2,
          "image_url ": "https://www.sample.com/files/img2.jpg",
          "type": 1,
          "redirect_url": "https://www.anotherpage.com/2"
        }
      ]
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    return Banners.fromMap(response.data);
  }

  @override
  Future<NewsLetters> newsLetters(ListRequest request) {
    return null;
  }

  @override
  Future<NewsLetter> getNewsletter(int newsletterId) {
    return null;
  }

  @override
  Future<Notifications> getNotifications(ListRequest request) {
    return null;
  }
}