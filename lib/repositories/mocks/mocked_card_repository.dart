import 'package:dio/dio.dart';
import 'package:plainsandprintsloyalty/model/balance.dart';
import 'package:plainsandprintsloyalty/model/history.dart';
import 'package:plainsandprintsloyalty/model/request/balance_request.dart';
import 'package:plainsandprintsloyalty/model/request/cancel_voucher_request.dart';
import 'package:plainsandprintsloyalty/model/request/convert_points_request.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/model/voucher.dart';
import 'package:plainsandprintsloyalty/model/vouchers.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/card_repository.dart';

class MockedCardRepository extends CardRepository {
  @override
  Future<User> getAccountDetails(int accountId) async {
    final Map<String, dynamic> responseMap = {
      "first_name": "Juan",
      "middle_name": "Santos",
      "last_name": "Dela Cruz",
      "title": 1,
      "birth_date": "1995-01-02",
      "gender": 1,
      "marital_status": 1,
      "email": "jdelacruz@email.com",
      "username": "jcruz",
      "password": "1234567890",
      "mobile_no": "+639954225564",
      "address_line": "123 Icecream Street",
      "city": 841,
      "province": 71,
      "country": 1,
      "zip_code": "1229",
      "card_pan": "1234568901234"
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    return User.fromMap(response.data);
  }

  @override
  Future<Balance> getBalance(BalanceRequest request) async {
    final Map<String, dynamic> responseMap = {
      "load_amount": 200.00,
      "points": [
        {
          "bucket_name": "PTS",
          "total_available": 12.00,
          "total_queue": 0.00,
          "total_earned": 20.00,
          "total_redeemed": 5.00,
          "total_expired": 3.00
        }
      ]
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    return Balance.fromMap(response.data);
  }

  @override
  Future<History> getHistory(ListRequest request) async {
    final Map<String, dynamic> responseMap = {
      "total_transaction": 50,
      "transaction": [
        {
          "txn_id": "57837291729",
          "branch": "GHL Philippines - Makati",
          "invoice_no": "123457",
          "card_no": "12340000010",
          "txn_amount": 200.00,
          "earned_points": 1.25,
          "redeemed_points": 0.00,
          "txn_type": "Purchase",
          "txn_status": 2,
          "txn_date": "2019-02-20 10:00:00.000"
        },
        {
          "txn_id": "57837291729",
          "branch": "GHL Philippines - Makati",
          "invoice_no": "123457",
          "card_no": "12340000010",
          "txn_amount": 200.00,
          "earned_points": 1.25,
          "redeemed_points": 0.00,
          "txn_type": "Purchase",
          "txn_status": 2,
          "txn_date": "2019-02-20 10:00:00.000"
        }
      ]
    };
    final Response response = Response(statusCode: 200, data: responseMap);
    await Future.delayed(Duration(seconds: 1));
    return History.fromMap(response.data);
  }

  @override
  Future<void> updateAccount(int accountId, User user) async {
    final Response response = Response(statusCode: 200);
    await Future.delayed(Duration(seconds: 1));
    return response;
  }

  @override
  Future<Vouchers> getVouchers(ListRequest request) async {
    final Map<String, dynamic> responseMap = {
      "total_voucher": 4,
      "voucher": [
        {
          "voucher_code": "Q58V6QMW",
          "voucher_description": "Quantity: 200",
          "voucher_status": "1",
          "voucher_expiration": "2019-05-31T23:59:59+00:00"
        },
        {
          "voucher_code": "JBKY0RBT",
          "voucher_description": "Quantity: 200",
          "voucher_status": "1",
          "voucher_expiration": "2019-05-31T23:59:59+00:00"
        },
        {
          "voucher_code": "V3E8QCHS",
          "voucher_description": "Quantity: 200",
          "voucher_status": "1",
          "voucher_expiration": "2019-05-31T23:59:59+00:00"
        },
        {
          "voucher_code": "BD8VI0FS",
          "voucher_description": "Quantity: 100",
          "voucher_status": "1",
          "voucher_expiration": "2019-05-31T23:59:59+00:00"
        }
      ]
    };
    return Vouchers.fromMap(responseMap);
  }

  @override
  Future<Voucher> convertPoints(ConvertPointsRequest request) async {
    final Map<String, dynamic> responseMap = {
      "voucher_code": "UM8W3SYS",
      "voucher_description": "Quantity: 200",
      "voucher_status": "1",
      "voucher_expiration": "2019-05-31T23:59:59+00:00"
    };
    return Voucher.fromMap(responseMap);
  }

  @override
  Future<void> cancelVoucher(CancelVoucherRequest request) async {
    final Response response = Response(statusCode: 200);
    await Future.delayed(Duration(seconds: 1));
    return response;
  }
}