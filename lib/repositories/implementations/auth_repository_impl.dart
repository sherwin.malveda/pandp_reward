import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:plainsandprintsloyalty/api/api.dart';
import 'package:plainsandprintsloyalty/api/api_uris.dart';
import 'package:plainsandprintsloyalty/dao/mao_dao.dart';
import 'package:plainsandprintsloyalty/model/request/activation_request.dart';
import 'package:plainsandprintsloyalty/model/request/change_password_request.dart';
import 'package:plainsandprintsloyalty/model/request/forgot_password_password.dart';
import 'package:plainsandprintsloyalty/model/request/login_request.dart';
import 'package:plainsandprintsloyalty/model/request/member_request.dart';
import 'package:plainsandprintsloyalty/model/request/reset_password_request.dart';
import 'package:plainsandprintsloyalty/model/request/verification_request.dart';
import 'package:plainsandprintsloyalty/model/session.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  final Api api;
  final MapDao mapDao;

  AuthRepositoryImpl({@required this.api, @required this.mapDao});

  @override
  Future<User> existingMember(MemberRequest request) async {
    final Response response = await api.post(
      ApiUris.EXISTING,
      request.toMap(),
      false
    );
    return User.fromMap(response.data);
  }

  @override
  Future<Session> login(LoginRequest request) async {
    final Response response = await api.post(ApiUris.LOGIN, request.toMap(), false);
    return Session.fromMap(response.data)..saveMap(mapDao);
  }

  @override
  Future<void> forgotPassword(ForgotPasswordRequest request) async {
    await api.post(ApiUris.FORGOT_PASSWORD, request.toMap(), false);
  }

  @override
  Future<VerificationRequest> register(User user) async {
    final Response response = await api.post(
      ApiUris.REGISTRATION, user.toMap(), false
    );
    return VerificationRequest.fromMap(response.data);
  }

  @override
  Future<void> changePassword(ChangePasswordRequest changePassRequest) async {
    await api.post(
      ApiUris.replaceParam(ApiUris.CHANGE_PASSWORD, "accountId", changePassRequest.accountId),
      changePassRequest.toMap(),
      true
    );
  }

  @override
  Future<void> activateAccount(ActivationRequest request) async {
    await api.post(ApiUris.ACTIVATE, request.toMap(), false);
  }

  @override
  Future<void> verifyAccount(VerificationRequest request) async {
    await api.post(ApiUris.VERIFY, request.toMap(), false);
  }

  @override
  Future<Session> getCachedSession() async {
    return Session.fromDao(MapDao.withPrefs(await mapDao.init()));
  }

  @override
  Future<void> resetPassword(ResetPasswordRequest request) async {
    await api.post(ApiUris.RESET_PASSWORD, request.toMap(), false);
  }

}
