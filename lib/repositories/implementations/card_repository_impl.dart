import 'package:dio/dio.dart';
import 'package:plainsandprintsloyalty/api/api.dart';
import 'package:plainsandprintsloyalty/api/api_uris.dart';
import 'package:plainsandprintsloyalty/model/balance.dart';
import 'package:plainsandprintsloyalty/model/history.dart';
import 'package:plainsandprintsloyalty/model/request/balance_request.dart';
import 'package:plainsandprintsloyalty/model/request/cancel_voucher_request.dart';
import 'package:plainsandprintsloyalty/model/request/convert_points_request.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/model/user.dart';
import 'package:plainsandprintsloyalty/model/voucher.dart';
import 'package:plainsandprintsloyalty/model/vouchers.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/card_repository.dart';

class CardRepositoryImpl implements CardRepository {
  final Api api;

  CardRepositoryImpl({this.api});

  Future<User> getAccountDetails(int accountId) async {
    final Response response = await api.post(ApiUris.replaceParam(ApiUris.ACCOUNT_INFO, "accountId", accountId),
      {},
      true
    );
    return User.fromMap(response.data);
  }

  Future<void> updateAccount(int accountId, User user) async {
    await api.post(ApiUris.replaceParam(ApiUris.ACCOUNT_EDIT, "accountId", accountId),
      user.toMap(),
      true
    );
  }

  Future<Balance> getBalance(BalanceRequest request) async {
    final Response response = await api.post(
      ApiUris.replaceParam(ApiUris.BALANCE_INQUIRY, "accountId", request.accountId),
      request.toMap(),
      true
    );
    return Balance.fromMap(response.data);
  }

  Future<History> getHistory(ListRequest request) async {
    final Response response = await api.post(
      ApiUris.replaceParam(ApiUris.TRANSACTION_HISTORY, "accountId", request.accountId),
      request.toMap(),
      true
    );
    return History.fromMap(response.data);
  }

  Future<Vouchers> getVouchers(ListRequest request) async {
    final Response response = await api.post(
      ApiUris.replaceParam(ApiUris.VOUCHER_LIST, "accountId", request.accountId),
      request.toMap(),
      true
    );
    return Vouchers.fromMap(response.data);
  }

  @override
  Future<Voucher> convertPoints(ConvertPointsRequest request) async {
    final Response response = await api.post(
      ApiUris.replaceParam(ApiUris.CONVERT_POINTS, "accountId", request.accountId),
      request.toMap(),
      true
    );
    return Voucher.fromMap(response.data);
  }

  @override
  Future<void> cancelVoucher(CancelVoucherRequest request) async {
    await api.post(
      ApiUris.replaceParam(ApiUris.CANCEL_VOUCHER, "accountId", request.accountId),
      request.toMap(),
      true
    );
  }
}
