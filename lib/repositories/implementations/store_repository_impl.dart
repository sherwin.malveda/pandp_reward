import 'package:dio/dio.dart';
import 'package:plainsandprintsloyalty/api/api.dart';
import 'package:plainsandprintsloyalty/api/api_uris.dart';
import 'package:plainsandprintsloyalty/model/banners.dart';
import 'package:plainsandprintsloyalty/model/branch_details.dart';
import 'package:plainsandprintsloyalty/model/branches.dart';
import 'package:plainsandprintsloyalty/model/news_letter.dart';
import 'package:plainsandprintsloyalty/model/news_letters.dart';
import 'package:plainsandprintsloyalty/model/notifications.dart';
import 'package:plainsandprintsloyalty/model/promo_details.dart';
import 'package:plainsandprintsloyalty/model/promos.dart';
import 'package:plainsandprintsloyalty/model/request/list_request.dart';
import 'package:plainsandprintsloyalty/repositories/contracts/store_repository.dart';

class StoreRepositoryImpl implements StoreRepository {
  final Api api;

  StoreRepositoryImpl({this.api});

  Future<Promos> getPromos(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.attachPaginationToUri(ApiUris.PROMOTIONS, request), 
      false
    );
    return Promos.fromMap(response.data);
  }

  Future<PromoDetails> getPromo(int promoId) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.PROMO_DETAILS, "promoId", promoId), 
      true
    );
    return PromoDetails.fromMap(response.data);
  }

  Future<Branches> getBranches(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.attachPaginationToUri(ApiUris.BRANCHES, request), 
      false
    );
    return Branches.fromMap(response.data);
  }

  @override
  Future<NewsLetters> newsLetters(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.attachPaginationToUri(ApiUris.NEWS_LETTER, request), 
      false
    );
    return NewsLetters.fromMap(response.data);
  }

    Future<NewsLetter> getNewsletter(int newsletterId) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.NEWS_LETTER_DETAILS, "newsletter_id", newsletterId), 
      true
    );
    return NewsLetter.fromMap(response.data);
  }

  Future<BranchDetails> getBranch(int branchId) async {
    final Response response = await api.get(
      ApiUris.replaceParam(ApiUris.BRANCH_INFO, "branchId", branchId),
      true
    );
    return BranchDetails.fromMap(response.data);
  }

  Future<Banners> getBanners(ListRequest request) async {
    final Response response = await api.get(
      ApiUris.attachPaginationToUri(ApiUris.BANNER_LIST, request),
      false
    );
    return Banners.fromMap(response.data);
  }

  Future<Notifications> getNotifications(ListRequest request) async {
    final Response response = await api.get(
        ApiUris.attachPaginationToUri2(ApiUris.NOTIFICATION_LIST, request),
        false
    );
    return Notifications.fromMap(response.data);
  }
}