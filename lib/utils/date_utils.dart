class DateUtils {
  static String getGreetings(DateTime time) {
    if (time.hour >= 0 && time.hour < 12) {
      return "Good Morning";
    } else if (time.hour > 11 && time.hour < 18) {
      return "Good Afternoon";
    } else if (time.hour > 7 && time.hour < 24) {
      return "Good Evening";
    }
    return "Hello";
  }
}
