
import 'package:password_strength/password_strength.dart';

class FormValidators {
  
  static String isIdentical(String a, String b, String errorMessage) {
    if ((a == null || b == null) || (a.isEmpty || b.isEmpty)) return null;
    if (a != b) {
      return errorMessage;
    }
    return null;
  }

  static String isPassword(String a, String errorMessage) {
    if (a == null || a.isEmpty) return null;
    if (a.length <= 3) return errorMessage;
    if (estimatePasswordStrength(a) < 0.2) {
      return errorMessage;
    } else if (estimatePasswordStrength(a) < 0.4) {
      return errorMessage;
    }
    return null;
  }

  static String passwordStrength(String value){
    if (value == null || value.isEmpty) return "";
    if (estimatePasswordStrength(value) < 0.2) {
      return "\t\t\t Very Weak \t\t\t";
    } else if (estimatePasswordStrength(value) < 0.4) {
      return "\t\t\t Weak \t\t\t";
    } else if (estimatePasswordStrength(value) < 0.6) {
      return "\t\t\t Good \t\t\t";
    } else if (estimatePasswordStrength(value) < 0.8) {
      return "\t\t\t Strong \t\t\t";
    } else {
      return "\t\t\t Very Strong \t\t\t";
    }
  }

  static isAlphanumeric(String value, String errorMessage) {
    if (value == null || value.isEmpty) return null;
    if (value.length < 3 || value.length >16) return errorMessage;
  }

  static String isLettersOnly(String value, String errorMessage) {
    if (value == null || value.isEmpty) return null;
    return RegExp(r"^(?=.{1,50}$)[A-Za-z]+(?:['_.\s][A-Za-z]+)*$")
        .hasMatch(value)
        ? null
        : errorMessage;
  }

  static String isMobileNumber(String value, String errorMessage) {
    if (value == null || value.isEmpty) return null;
    return RegExp(r"\d{10}$").hasMatch(value)
        ? null
        : errorMessage;
  }

  static String isNotEmpty(String value, String errorMessage) {
    if (value == null || value.isEmpty) return errorMessage;

    return null;
  }

  static String isValidEmail(String value, String errorMessage) {
    if (value == null || value.isEmpty) return null;
    return RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(value)
        ? null
        : errorMessage;
  }
}
